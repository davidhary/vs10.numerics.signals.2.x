''' <summary>
''' Contains methods that compute advanced functions with real arguments.
''' </summary>
Partial Public NotInheritable Class AdvancedMath

    ' members are defined in other files

    ' internal utility trig functions that are accurate for large arguments

    ''' <summary>
    ''' Computes the Sine for for large arguments
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <returns>System.Double.</returns>
    Friend Shared Function Sin(ByVal x As Double, ByVal y As Double) As Double
        Return Math.Sin(Reduce(x, y))
    End Function

    ''' <summary>
    ''' Computes the Cosine for for large arguments
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <returns>System.Double.</returns>
    Friend Shared Function Cos(ByVal x As Double, ByVal y As Double) As Double
        Return Math.Cos(Reduce(x, y))
    End Function

    Public Const TwoPI As Double = 2.0 * Math.PI

    ''' <summary>
    ''' Reduces an argument to its corresponding argument between -2 Pi &lt; x &lt; 2 Pi
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <returns>System.Double.</returns>
    Friend Shared Function Reduce(ByVal x As Double, ByVal y As Double) As Double

        Dim t As Double = x + TwoPI * y
        If (Math.Abs(t) < 64.0) OrElse (Math.Abs(t) > dmax) Then
            ' if the argument is small we don't need the high accuracy reduction
            ' if the argument is too big, we can't do the high accuracy reduction because it would overflow a decimal variable
            Return t
        Else
            ' otherwise, convert to decimal, subtract a multiple of 2 Pi, and return

            ' reduce x by factors of 2 Pi
            Dim dx As Decimal = Convert.ToDecimal(x)
            Dim dn As Decimal = Decimal.Truncate(dx / dPI2)
            dx = dx - dn * dPI2

            ' reduce y by factors of 1
            Dim dy As Decimal = Convert.ToDecimal(y)
            Dim dm As Decimal = Decimal.Truncate(dy / 1D)
            dy = dy - dm * 1D

            ' form the argument
            Dim dt As Decimal = dx + dy * dPI2
            Return Convert.ToDouble(dt)

        End If
    End Function

    Private Shared ReadOnly dPI2 As Decimal = 2D * 3.1415926535897932384626433833D

    Private Shared ReadOnly dmax As Double = Convert.ToDouble(Decimal.MaxValue)

End Class
