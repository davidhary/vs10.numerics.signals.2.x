﻿Partial Public NotInheritable Class AdvancedMath

    Private Sub New()
    End Sub

#If False Then

    ' Mathematical constants

    ''' <summary>
    ''' The golden ratio.
    ''' </summary>
    ''' <remarks><para>The golden ratio &#x3C6; = 1.1618...</para></remarks>
    ''' <seealso href="http://en.wikipedia.org/wiki/Golden_ratio"/>
    ''' <seealso href="http://mathworld.wolfram.com/GoldenRatio.html" />
    Public Shared ReadOnly GoldenRatio As Double = (1.0 + Math.Sqrt(5.0)) / 2.0

    ''' <summary>
    ''' The Euler constant.
    ''' </summary>
    ''' <remarks><para>The Euler constant &#x3B3; = 0.5772...</para></remarks>
    ''' <seealso href="http://en.wikipedia.org/wiki/Euler_gamma"/>
    ''' <seealso href="http://mathworld.wolfram.com/Euler-MascheroniConstant.html" />
    Public Const EulerGamma As Double = 0.57721566490153287

    ''' <summary>
    ''' Catalan's constant.
    ''' </summary>
    ''' <remarks><para>Catalan's constant 0.9159...</para></remarks>
    ''' <seealso href="http://en.wikipedia.org/wiki/Catalan_constant"/>
    Public Const Catalan As Double = 0.915965594177219

#End If


End Class
