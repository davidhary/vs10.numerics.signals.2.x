﻿Imports System.Numerics
''' <summary>
''' Provides simple functions of complex arguments. 
''' </summary>
Public NotInheritable Class ComplexMath

    Private Sub New()
    End Sub

    ' static pure imaginary

    ''' <summary>
    ''' Gets the unit imaginary number I.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="I")>
    Public Shared ReadOnly Property I() As Complex
        Get
            Return New Complex(0.0, 1.0)
        End Get
    End Property

    ''' <summary>
    ''' Computes the absolute value of a complex number.
    ''' </summary>
    ''' <param name="value">The argument.</param>
    ''' <returns>The value of |z|.</returns>
    ''' <remarks>
    ''' <para>The absolute value of a complex number is the distance of the number from the origin
    ''' in the complex plane. This is a compatible generalization of the definition of the absolute
    ''' value of a real number.</para>
    ''' </remarks>
    ''' <seealso cref="System.Math.Abs"/>
    Public Shared Function Abs(ByVal value As Complex) As Double
        Return ComplexMath.Hypotenuse(value.Real, value.Imaginary)
    End Function

    ''' <summary>
    ''' Computes the length of a right triangle's hypotenuse.
    ''' </summary>
    ''' <param name="value">The argument.</param>
    ''' <returns>The length of the hypotenuse, sqrt(real<sup>2</sup> + imaginary<sup>2</sup>) = |value|.</returns>
    ''' <remarks>
    ''' <para>The length is computed accurately, even in cases where
    ''' x<sup>2</sup> or y<sup>2</sup> would overflow.</para>
    ''' </remarks>
    Public Shared Function Hypotenuse(ByVal value As Complex) As Double
        Return ComplexMath.Hypotenuse(value.Real, value.Imaginary)
    End Function

    ''' <summary>
    ''' Computes the length of a right triangle's hypotenuse.
    ''' </summary>
    ''' <param name="value">The length of one side.</param>
    ''' <param name="orthogonal">The length of orthogonal side.</param>
    ''' <returns>The length of the hypotenuse, sqrt(x<sup>2</sup> + y<sup>2</sup>).</returns>
    ''' <remarks>
    ''' <para>The length is computed accurately, even in cases where
    ''' x<sup>2</sup> or y<sup>2</sup> would overflow.</para>
    ''' </remarks>
    Public Shared Function Hypotenuse(ByVal value As Double, ByVal orthogonal As Double) As Double
        If (value = 0.0) AndAlso (orthogonal = 0.0) Then
            Return 0.0
        Else
            Dim ax As Double = Math.Abs(value)
            Dim ay As Double = Math.Abs(orthogonal)
            If ax > ay Then
                Dim r As Double = orthogonal / value
                Return ax * Math.Sqrt(1.0 + r * r)
            Else
                Dim r As Double = value / orthogonal
                Return ay * Math.Sqrt(1.0 + r * r)
            End If
        End If
    End Function

    ''' <summary>
    ''' Computes the phase of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of arg(z).</returns>
    ''' <remarks>
    ''' <para>The phase of a complex number is the angle between the line joining it to the origin and the real axis of the complex plane.</para>
    ''' <para>The phase of complex numbers in the upper complex plane lies between 0 and &#x3C0;. The phase of complex numbers
    ''' in the lower complex plane lies between 0 and -&#x3C0;. The phase of a real number is zero.</para>
    ''' </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Arg(ByVal z As Complex) As Double
        ' returns 0 to PI in the upper complex plane (Im>=0),
        ' 0 to -PI in the lower complex plane (Im<0)
        Return Math.Atan2(z.Imaginary, z.Real)
    End Function

    ''' <summary>
    ''' Computes e raised to the power of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of e<sup>z</sup>.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Exp(ByVal z As Complex) As Complex
        Dim m As Double = Math.Exp(z.Real)
        Return New Complex(m * AdvancedMath.Cos(z.Imaginary, 0.0), m * AdvancedMath.Sin(z.Imaginary, 0.0))
    End Function

    ''' <summary>
    ''' Computes the natural logarithm of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of ln(z).</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Log(ByVal z As Complex) As Complex
        Return New Complex(Math.Log(Abs(z)), Arg(z))
    End Function

    ''' <summary>
    ''' Computes the square root of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The square root of the argument.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Sqrt(ByVal z As Complex) As Complex
        If z.Imaginary = 0 Then
            Return Math.Sqrt(z.Real)
        Else
            Return Pow(z, 0.5)
        End If
    End Function

    ''' <summary>
    ''' Computes the sine of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of sin(z).</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Sin(ByVal z As Complex) As Complex
        Dim p As Double = Math.Exp(z.Imaginary)
        Dim q As Double = 1 / p
        Dim sineH As Double = (p - q) / 2.0
        Dim cosineH As Double = (p + q) / 2.0
        Return New Complex(Math.Sin(z.Real) * cosineH, Math.Cos(z.Real) * sineH)
    End Function

    ''' <summary>
    ''' Computes the hyperbolic sine of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of sinh(z).</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Sinh(ByVal z As Complex) As Complex
        ' sinh(z) = -i sin(iz)
        Dim sineH As Complex = Sin(New Complex(-z.Imaginary, z.Real))
        Return New Complex(sineH.Imaginary, -sineH.Real)
    End Function

    ''' <summary>
    ''' Computes the cosine of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of cos(z).</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Cos(ByVal z As Complex) As Complex
        Dim p As Double = Math.Exp(z.Imaginary)
        Dim q As Double = 1 / p
        Dim sinwH As Double = (p - q) / 2.0
        Dim cosineH As Double = (p + q) / 2.0
        Return New Complex(Math.Cos(z.Real) * cosineH, -Math.Sin(z.Real) * sinwH)
    End Function

    ''' <summary>
    ''' Computes the hyperbolic cosine of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of cosh(z).</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Cosh(ByVal z As Complex) As Complex
        ' cosh(z) = cos(iz)
        Return Cos(New Complex(-z.Imaginary, z.Real))
    End Function

    ''' <summary>
    ''' Computes the tangent of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of tan(z).</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Tan(ByVal z As Complex) As Complex
        ' tan z = [sin(2x) + I sinh(2y)]/[cos(2x) + I cosh(2y)]
        Dim x2 As Double = 2.0 * z.Real
        Dim y2 As Double = 2.0 * z.Imaginary
        Dim p As Double = Math.Exp(y2)
        Dim q As Double = 1 / p
        Dim cosineH As Double = (p + q) / 2.0
        If Math.Abs(z.Imaginary) < 4.0 Then
            Dim sineH As Double = (p - q) / 2.0
            Dim D As Double = Math.Cos(x2) + cosineH
            Return New Complex(Math.Sin(x2) / D, sineH / D)
        Else
            ' when Im(z) gets too large, sinh and cosh individually blow up
            ' but ratio is still ~1, so rearrange to use tanh instead
            Dim F As Double = (1.0 + Math.Cos(x2) / cosineH)
            Return New Complex(Math.Sin(x2) / cosineH / F, Math.Tanh(y2) / F)
        End If
    End Function

    ''' <summary>
    ''' Computes the hyperbolic tangent of a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The value of tanh(z).</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Tanh(ByVal z As Complex) As Complex
        Return Sinh(z) / Cosh(z)
    End Function

    ''' <summary>
    ''' Raises a complex number to an arbitrary real power.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <param name="p">The power.</param>
    ''' <returns>The value of z<sup>p</sup>.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="p")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Pow(ByVal z As Complex, ByVal p As Double) As Complex
        Dim m As Double = Math.Pow(Abs(z), p)
        Dim t As Double = Arg(z) * p
        Return New Complex(m * Math.Cos(t), m * Math.Sin(t))
    End Function

    ''' <summary>
    ''' Raises a real number to an arbitrary complex power.
    ''' </summary>
    ''' <param name="x">The real base, which must be non-negative.</param>
    ''' <param name="z">The complex exponent.</param>
    ''' <returns>The value of x<sup>z</sup>.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Pow(ByVal x As Double, ByVal z As Complex) As Complex
        If x < 0.0 Then
            Throw New ArgumentOutOfRangeException("x")
        End If
        If z = 0.0 Then
            Return 1.0
        End If
        If x = 0.0 Then
            Return 0.0
        End If
        Dim m As Double = Math.Pow(x, z.Real)
        Dim t As Double = Math.Log(x) * z.Imaginary
        Return New Complex(m * Math.Cos(t), m * Math.Sin(t))
    End Function

    ''' <summary>
    ''' Raises a complex number to an integer power.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <param name="n">The power.</param>
    ''' <returns>The value of z<sup>n</sup>.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="n")>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    Public Shared Function Pow(ByVal z As Complex, ByVal n As Integer) As Complex

        ' this is a straight-up copy of MoreMath.Pow with x -> z, double -> Complex

        If n < 0 Then
            Return 1.0 / Pow(z, -n)
        End If

        Select Case n
            Case 0
                ' we follow convention that 0^0 = 1
                Return 1.0
            Case 1
                Return z
            Case 2
                ' 1 multiply
                Return z * z
            Case 3
                ' 2 multiplies
                Return z * z * z
            Case 4
                ' 2 multiplies
                Dim z2 As Complex = z * z
                Return z2 * z2
            Case 5
                ' 3 multiplies
                Dim z2 As Complex = z * z
                Return z2 * z2 * z
            Case 6
                ' 3 multiplies
                Dim z2 As Complex = z * z
                Return z2 * z2 * z2
            Case 7
                ' 4 multiplies
                Dim z3 As Complex = z * z * z
                Return z3 * z3 * z
            Case 8
                ' 3 multiplies
                Dim z2 As Complex = z * z
                Dim z4 As Complex = z2 * z2
                Return z4 * z4
            Case 9
                ' 4 multiplies
                Dim z3 As Complex = z * z * z
                Return z3 * z3 * z3
            Case 10
                ' 4 multiplies
                Dim z2 As Complex = z * z
                Dim z4 As Complex = z2 * z2
                Return z4 * z4 * z2
            Case Else
                Return ComplexMath.Pow(z, CDbl(n))
        End Select

    End Function

End Class

