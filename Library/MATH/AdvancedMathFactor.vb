Imports System.Collections

Partial Public NotInheritable Class AdvancedMath

    Private Sub New()
    End Sub


    Friend Shared Function GCD(ByVal u As Integer, ByVal v As Integer) As Integer
        Do While v <> 0
            Dim t As Integer = u Mod v
            u = v
            v = t
        Loop
        Return u
    End Function

    ''' <summary>
    ''' Computes a power of an integer in modular arithmetic.
    ''' </summary>
    ''' <param name="b">The base, which must be positive.</param>
    ''' <param name="e">The exponent, which must be positive.</param>
    ''' <param name="m">The modulus, which must be positive.</param>
    ''' <returns>The value of b<sup>e</sup> mod m.</returns>
    ''' <remarks>
    ''' <para>Modular exponentiation is used in many number-theory applications, including
    ''' primality testing, prime factorization, and cryptography.</para>
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"><paramref name="b"/>,  <paramref name="e"/>, or <paramref name="m"/> is not positive.</exception>
    ''' <seealso href="http://en.wikipedia.org/wiki/Modular_exponentiation"/>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="b")>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="e")>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="m")>
    Public Shared Function PowMod(ByVal b As Integer, ByVal e As Integer, ByVal m As Integer) As Integer
        If b < 0 Then
            Throw New ArgumentOutOfRangeException("b")
        End If
        If e < 1 Then
            Throw New ArgumentOutOfRangeException("e")
        End If
        If m < 1 Then
            Throw New ArgumentOutOfRangeException("m")
        End If

        ' use long internally
        ' since the "worst" we ever do before modding is to square, and since a long should
        ' hold twice as many digits as an int, this algorithm should not overflow
        Dim bb As Long = Convert.ToInt64(b)
        Dim mm As Long = Convert.ToInt64(m)
        Dim rr As Long = 1

        Do While e > 0
            If (e And 1) = 1 Then
                'TO_DO: There is no VB equivalent to 'checked' in this context:
                'ORIGINAL LINE: rr = checked((rr * bb) Mod mm);
                rr = (rr * bb) Mod mm
            End If
            e = e >> 1
            'TO_DO: There is no VB equivalent to 'checked' in this context:
            'ORIGINAL LINE: bb = checked((bb * bb) Mod mm);
            bb = (bb * bb) Mod mm
        Loop

        Return Convert.ToInt32(rr)

    End Function

    ''' <summary>
    ''' Factors the specified n.
    ''' </summary>
    ''' <param name="n">The n.</param>
    ''' <returns>Generic.List{Factor}.</returns>
    ''' <remarks>
    ''' Prime factorization. Leave this internal for now, until it is cleaned up.
    ''' As currently implemented, it does not actually guarantee full prime factorization! Pollard's rho
    ''' method can yield non-prime factors, and this appears to occur for about 0.25% of all integers under 1,000,000.
    ''' For example, "factors" of 1681 = 41 * 41, 6751 = 43 * 157, and 9167 = 89 * 103 are claimed.
    ''' These composite "factors" are, however, still co-prime to the other factors, so the almost-factorization
    ''' will still work for reduction of Fourier transforms, which is how we are currently using it.
    ''' </remarks>
    ''' <exception cref="System.ArgumentOutOfRangeException">n</exception>
    Friend Shared Function Factor(ByVal n As Integer) As Generic.List(Of Factor)
        If n < 1 Then
            Throw New ArgumentOutOfRangeException("n")
        End If

        Dim factors As New Generic.List(Of Factor)()

        If n > 1 Then
            FactorByTrialDivision(factors, n)
        End If

        If n > 1 Then
            FactorByPollardsRhoMethod(factors, n, 250)
        End If

        If n > 1 Then
            factors.Add(New Factor(n, 1))
        End If

        Return factors
    End Function

    Friend Shared ReadOnly SmallPrimes() As Integer = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31}

    ' Trial division is the simplest prime factorization method. It consists of attempted to divide by known primes.
    ' It is a good way to eliminate known small prime factors before proceeding on to bigger and more difficult prime factors.

    Private Shared Sub FactorByTrialDivision(ByVal factors As Generic.List(Of Factor), ByRef n As Integer)

        For Each p As Integer In SmallPrimes

            Dim m As Integer = 0
            Do While n Mod p = 0
                n = n \ p
                m += 1
            Loop
            If m > 0 Then
                factors.Add(New Factor(p, m))
            End If

            If n = 1 Then
                Return
            End If

        Next p

    End Sub

    ''' <summary>
    ''' Factors the by Pollards Rho method.
    ''' </summary>
    ''' <param name="factors">The factors.</param>
    ''' <param name="n">The n.</param>
    ''' <param name="maximumIterations">The maximum iterations.</param>
    Private Shared Sub FactorByPollardsRhoMethod(ByVal factors As Generic.List(Of Factor), ByRef n As Integer, ByVal maximumIterations As Integer)

        Dim x As Integer = 5
        Dim y As Integer = 2
        Dim k As Integer = 1
        Dim l As Integer = 1

        For c As Integer = 0 To maximumIterations - 1
            'while (true) {
            Dim g As Integer = AdvancedMath.GCD(Math.Abs(y - x), n)
            If g = n Then
                ' the factor n will repeat itself indefinitely; either n is prime or the method has failed
                Return
            ElseIf g = 1 Then
                k -= 1
                If k = 0 Then
                    y = x
                    l = 2 * l
                    k = l
                End If
                ' take x <- (x^2 + 1) mod n
                x = AdvancedMath.PowMod(x, 2, n) + 1
                If x = n Then
                    x = 0
                End If
            Else
                ' g is a factor of n; in all likelihood, it is prime, although this isn't guaranteed
                ' for our current approximate-factoring purposes, we will assume it is prime
                ' it is at least co-prime to all other recognized factors
                Dim m As Integer = 0
                Do While n Mod g = 0
                    n = n \ g
                    x = x Mod n
                    y = y Mod n
                    m += 1
                Loop
                factors.Add(New Factor(g, m))
            End If
        Next c

    End Sub

End Class

''' <summary>
''' Struct Factor
''' </summary>
Friend Structure Factor

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Factor" /> struct.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="multiplicity">The multiplicity.</param>
    Public Sub New(ByVal value As Integer, ByVal multiplicity As Integer)
        Me._value = value
        Me._multiplicity = multiplicity
    End Sub

    Private _value As Integer
    ''' <summary>
    ''' Gets the value.
    ''' </summary>
    ''' <value>The value.</value>
    Public ReadOnly Property Value() As Integer
        Get
            Return Me._value
        End Get
    End Property

    Dim _multiplicity As Integer
    ''' <summary>
    ''' Gets the multiplicity.
    ''' </summary>
    ''' <value>The multiplicity.</value>
    Public ReadOnly Property Multiplicity() As Integer
        Get
            Return (Me._multiplicity)
        End Get
    End Property

End Structure
