﻿''' <summary>
''' Contains additional basic math operations.
''' </summary>
''' <remarks>
''' <para>The <see cref="System.Math"/> class defines many basic math operations, but a few that are important for optimal numerical
''' practice are missing. They are defined by this class.</para>
''' </remarks>
Public NotInheritable Class MoreMath

    Private Sub New()
    End Sub

    ''' <summary>
    ''' Raises an argument to an integer power.
    ''' </summary>
    ''' <param name="x">The argument.</param>
    ''' <param name="n">The power.</param>
    ''' <returns>The value of x<sup>n</sup>.</returns>
    ''' <remarks>
    ''' <para>Low integer powers can be computed by optimized algorithms much faster than the general
    ''' algorithm for an arbitrary real power employed by <see cref="System.Math.Pow"/>.</para>
    ''' </remarks>
    Public Shared Function Pow(ByVal x As Double, ByVal n As Integer) As Double

        If n < 0 Then
            Return (1.0 / Pow(x, -n))
        End If

        Select Case n
            Case 0
                ' we follow convention that 0^0 = 1
                Return (1.0)
            Case 1
                Return (x)
            Case 2
                ' 1 multiply
                Return (x * x)
            Case 3
                ' 2 multiplies
                Return (x * x * x)
            Case 4
                ' 2 multiplies
                Dim x2 As Double = x * x
                Return (x2 * x2)
            Case 5
                ' 3 multiplies
                Dim x2 As Double = x * x
                Return (x2 * x2 * x)
            Case 6
                ' 3 multiplies
                Dim x2 As Double = x * x
                Return (x2 * x2 * x2)
            Case 7
                ' 4 multiplies
                Dim x3 As Double = x * x * x
                Return (x3 * x3 * x)
            Case 8
                ' 3 multiplies
                Dim x2 As Double = x * x
                Dim x4 As Double = x2 * x2
                Return (x4 * x4)
            Case 9
                ' 4 multiplies
                Dim x3 As Double = x * x * x
                Return (x3 * x3 * x3)
            Case 10
                ' 4 multiplies
                Dim x2 As Double = x * x
                Dim x4 As Double = x2 * x2
                Return (x4 * x4 * x2)
            Case Else
                Return (Math.Pow(x, n))
        End Select


    End Function

    ' an internal method for squaring

    Friend Shared Function Pow2(ByVal x As Double) As Double
        Return (x * x)
    End Function

    ''' <summary>
    ''' Computes the length of a right triangle's hypotenuse.
    ''' </summary>
    ''' <param name="x">The length of one side.</param>
    ''' <param name="y">The length of another side.</param>
    ''' <returns>The length of the hypotenuse, sqrt(x<sup>2</sup> + y<sup>2</sup>).</returns>
    ''' <remarks>
    ''' <para>The length is computed accurately, even in cases where
    ''' x<sup>2</sup> or y<sup>2</sup> would overflow.</para>
    ''' </remarks>
    Public Shared Function Hypot(ByVal x As Double, ByVal y As Double) As Double
        If (x = 0.0) AndAlso (y = 0.0) Then
            Return (0.0)
        Else
            Dim ax As Double = Math.Abs(x)
            Dim ay As Double = Math.Abs(y)
            If ax > ay Then
                Dim r As Double = y / x
                Return (ax * Math.Sqrt(1.0 + r * r))
            Else
                Dim r As Double = x / y
                Return (ay * Math.Sqrt(1.0 + r * r))
            End If
        End If
    End Function

    ''' <summary>
    ''' Computes e<sup>x</sup>-1.
    ''' </summary>
    ''' <param name="x">The argument.</param>
    ''' <returns>The value of e<sup>x</sup>-1.</returns>
    ''' <remarks>
    ''' <para>If x is close to 0, then e<sup>x</sup> is close to 1, and computing e<sup>x</sup>-1 by by subtracting one from
    ''' e<sup>x</sup> as computed by the <see cref="Math.Exp"/> function will be subject to severe loss of significance due to
    ''' cancellation. This method maintains full precision for all values of x by switching to a series expansion for values of
    ''' x near zero.</para>
    ''' </remarks>
    Public Shared Function ExpMinusOne(ByVal x As Double) As Double
        If Math.Abs(x) < 0.125 Then
            Dim dr As Double = x
            Dim r As Double = dr
            For k As Integer = 2 To [Global].SeriesMax - 1
                Dim r_old As Double = r
                dr *= x / k
                r += dr
                If r = r_old Then
                    Return (r)
                End If
            Next k
            Throw New NonconvergenceException()
        Else
            Return (Math.Exp(x) - 1.0)
        End If
    End Function

End Class
