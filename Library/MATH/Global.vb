Friend NotInheritable Class Constants

    Private Sub New()
    End Sub

#If False Then
    ' maximum number of iterations of a series
    Public Const SeriesMax As Integer = 250

    ' double dedicates 10 bits to the magnitude of the the exponent so 2^10 = 1024 is the largest exponent,
    ' and 2^1024 is the largest representable double. 2^512 is its square root, which we compare to to
    ' decide if we are in danger of overflowing
    Public Shared ReadOnly SqrtMax As Double = Math.Pow(2.0, 512)

    ' double dedicates 52 bits to the magnitude of the mantissa, so 2^-52 is the smallest fraction difference
    ' it can detect; in order to avoid any funny effects at the margin, we try for one byte less, 2^-49
    Public Shared ReadOnly Accuracy As Double = Math.Pow(2.0, -49)

    ' pre-calculate some often-used factors

    Public Const TwoPI As Double = 2.0 * Math.PI

    Public Const HalfPI As Double = Math.PI / 2.0

    ' pre-calculate some often-used square roots

    Public Shared ReadOnly SqrtTwo As Double = Math.Sqrt(2.0)

    Public Shared ReadOnly SqrtThree As Double = Math.Sqrt(3.0)

    Public Shared ReadOnly SqrtPI As Double = Math.Sqrt(Math.PI)

    Public Shared ReadOnly SqrtTwoPI As Double = Math.Sqrt(2.0 * Math.PI)

    ' pre-calculate some often-used logs

    Public Shared ReadOnly LogTwo As Double = Math.Log(2.0)

#End If

End Class
