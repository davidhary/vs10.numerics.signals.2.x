﻿Imports System.Numerics
Imports System.Runtime.CompilerServices

Module ComplexExtensions

    ''' <summary>
    ''' Gets the complex conjugate of the complex number.
    ''' </summary>
    <Extension()>
    Public Function Conjugate(ByVal value As Complex) As Complex
        Return (New Complex(value.Real, -value.Imaginary))
    End Function

    ''' <summary>
    ''' Gets the complex value swapping imaginary and real.
    ''' </summary>
    <Extension()>
    Public Function Swap(ByVal value As Complex) As Complex
        Return (New Complex(value.Imaginary, value.Real))
    End Function

    ''' <summary>
    ''' Swaps the imaginary and real values.
    ''' </summary>
    <Extension()>
    Public Sub Swap(ByVal values() As Complex)
        If values IsNot Nothing Then
            For i As Integer = 0 To values.Length - 1
                values(i) = values(i).Swap
            Next
        End If
    End Sub

    ''' <summary>
    ''' Converts the real and imaginary values to complex.
    ''' </summary>
    ''' <param name="reals">The reals.</param>
    ''' <param name="imaginaries">The imaginaries.</param>
    ''' <returns>Complex[][].</returns>
    Public Function ToComplex(ByVal reals() As Double, ByVal imaginaries() As Double) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        Dim x(reals.Length - 1) As Complex
        For i As Integer = 0 To reals.Length - 1
            x(i) = New Complex(reals(i), imaginaries(i))
        Next
        Return x
    End Function

    ''' <summary>
    ''' Converts the complex values to a real and imaginary arrays.
    ''' </summary>
    ''' <param name="values">The values.</param>
    ''' <param name="reals">The reals.</param>
    ''' <param name="imaginaries">The imaginaries.</param>
    <Extension()>
    Public Sub FromComplex(ByVal values() As Complex, ByVal reals() As Double, ByVal imaginaries() As Double)
        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length <> values.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real, imaginary and complex values must have the same size")
        End If
        If values.Length > 0 Then
            For i As Integer = 0 To values.Length - 1
                reals(i) = values(i).Real
                imaginaries(i) = values(i).Imaginary
            Next
        End If
    End Sub

    ''' <summary>
    ''' Converts the real and imaginary values to complex.
    ''' </summary>
    ''' <param name="reals">The reals.</param>
    ''' <param name="imaginaries">The imaginaries.</param>
    ''' <returns>Complex[][].</returns>
    Public Function ToComplex(ByVal reals() As Single, ByVal imaginaries() As Single) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        Dim x(reals.Length - 1) As Complex
        For i As Integer = 0 To reals.Length - 1
            x(i) = New Complex(reals(i), imaginaries(i))
        Next
        Return x
    End Function

    ''' <summary>
    ''' Converts the complex values to a real and imaginary arrays.
    ''' </summary>
    ''' <param name="values">The values.</param>
    ''' <param name="reals">The reals.</param>
    ''' <param name="imaginaries">The imaginaries.</param>
    <Extension()>
    Public Sub FromComplex(ByVal values() As Complex, ByVal reals() As Single, ByVal imaginaries() As Single)
        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length <> values.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real, imaginary and complex values must have the same size")
        End If
        If values.Length > 0 Then
            For i As Integer = 0 To values.Length - 1
                reals(i) = CSng(values(i).Real)
                imaginaries(i) = CSng(values(i).Imaginary)
            Next
        End If
    End Sub

End Module
