
' In Debug mode, math operations (e.g. z1 + z2) are faster if we directly access the backing fields instead of using the
' property accessors. I suspect that in Release mode, this difference is optimized away, but i haven't checked.

''' <summary>Represents a complex number.</summary>
''' <remarks>
''' <para>Version 4.0 of the .NET Framework introduced a Complex structure equivalent to this one. To maintain compatibility
''' with earlier versions of the .NET Framework, Meta.Numerics maintains its own Complex structure.</para>
''' </remarks>
<Serializable()> 
Public Structure Complex
    Implements IEquatable(Of Complex)

    Private _real As Double
    ''' <summary>
    ''' Gets the real part of the complex number.
    ''' </summary>
    Public ReadOnly Property Real() As Double
        Get
            Return Me._real
        End Get
    End Property

    Private _imaginary As Double
    ''' <summary>
    ''' Gets the imaginary part of the complex number.
    ''' </summary>
    Public ReadOnly Property Imaginary() As Double
        Get
            Return Me._imaginary
        End Get
    End Property

    ''' <summary>
    ''' Gets the complex conjugate of the complex number.
    ''' </summary>
    Public ReadOnly Property Conjugate() As Complex
        Get
            Return (New Complex(Me._real, -_imaginary))
        End Get
    End Property

    ''' <summary>
    ''' Initializes a new complex number.
    ''' </summary>
    ''' <param name="re">The real part of the complex number.</param>
    ''' <param name="im">The imaginary part of the complex number.</param>
    Public Sub New(ByVal re As Double, ByVal im As Double)
        Me._real = re
        Me._imaginary = im
    End Sub

    ' conversions

    ''' <summary>
    ''' Converts the complex number to a double-precision real number.
    ''' </summary>
    ''' <param name="z">The complex number to covert.</param>
    ''' <returns>The corresponding double-precision real number.</returns>
    ''' <remarks><para>This explicit cast will fail if the complex number has a non-zero imaginary part.
    ''' If you just want to obtain the real part of a complex number, use the <see cref="Real" /> property.</para></remarks>
    ''' <exception cref="InvalidCastException">z.Im &#x2260; 0</exception>
    Public Shared Narrowing Operator CType(ByVal z As Complex) As Double
        If z.Imaginary <> 0.0 Then
            Throw New InvalidCastException("Complex casts to real must have vanishing imaginary parts.")
        End If
        Return (z.Real)
    End Operator

    ''' <summary>
    ''' Converts a double-precision real number to a complex number.
    ''' </summary>
    ''' <param name="x">The double-precision real number to convert.</param>
    ''' <returns>The corresponding complex number.</returns>
    ''' <remarks><para>The complex number output has a zero imaginary part and real part equal to the input number.</para>
    ''' <para>This is an implicit cast; the compiler will apply it automatically whenever a real number is given in a situation
    ''' where a complex number is required.</para></remarks>
    Public Shared Widening Operator CType(ByVal x As Double) As Complex
        Return (New Complex(x, 0.0))
    End Operator

    ' printing

    ''' <summary>
    ''' Produces a string representation of the complex number.
    ''' </summary>
    ''' <returns>A string representation of the complex number.</returns>
    Public Overrides Function ToString() As String
        Return (String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", Me._real, Me._imaginary))
    End Function

    ''' <summary>
    ''' Produces the representation of the complex number for the Python interactive console.
    ''' </summary>
    ''' <returns>A string representation of the complex number.</returns>
    <CLSCompliant(False)> 
    Public Function __repr__() As String
        Return (ToString())
    End Function

    ' equality operations are right by default

    ' static unary operators

    ''' <summary>
    ''' Negates a complex number.
    ''' </summary>
    ''' <param name="z">The argument.</param>
    ''' <returns>The argument times -1.</returns>
    Public Shared Operator -(ByVal z As Complex) As Complex
        Return (New Complex(-z._real, -z._imaginary))
    End Operator

    ' equality operators

    ''' <summary>
    ''' Tests the equality of two complex numbers.
    ''' </summary>
    ''' <param name="z1">The first complex number.</param>
    ''' <param name="z2">The second complex number.</param>
    ''' <returns>True if the two complex numbers are equal, otherwise false.</returns>
    Public Shared Operator =(ByVal z1 As Complex, ByVal z2 As Complex) As Boolean
        Return ((z1._real = z2._real) AndAlso (z1._imaginary = z2._imaginary))
    End Operator

    ''' <summary>
    ''' Tests the inequality of two complex numbers.
    ''' </summary>
    ''' <param name="z1">The first complex number.</param>
    ''' <param name="z2">The second complex number.</param>
    ''' <returns>False if the two complex numbers are equal, otherwise true.</returns>
    Public Shared Operator <>(ByVal z1 As Complex, ByVal z2 As Complex) As Boolean
        Return ((z1.Real <> z2.Real) OrElse (z1.Imaginary <> z2.Imaginary))
    End Operator

    ''' <summary>
    ''' Determines whether the given object represents the same complex number.
    ''' </summary>
    ''' <param name="obj">The object to compare.</param>
    ''' <returns>True if the object represents the same complex number, otherwise false.</returns>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return ((CType(obj, Complex)) = Me)
    End Function

    ''' <summary>
    ''' Determines whether the given complex number is the same.
    ''' </summary>
    ''' <param name="other">The complex number to compare.</param>
    ''' <returns>True if the complex number is the same, otherwise false.</returns>
    Public Overloads Function Equals(ByVal other As Complex) As Boolean Implements IEquatable(Of Complex).Equals
        Return (Me = other)
    End Function

    ''' <summary>
    ''' Returns a hash code for the complex number.
    ''' </summary>
    ''' <returns>A hash code.</returns>
    Public Overrides Function GetHashCode() As Integer
        Return (Real.GetHashCode() Xor Imaginary.GetHashCode())
    End Function

    ''' <summary>
    ''' Adds two complex numbers.
    ''' </summary>
    ''' <param name="z1">The first complex number.</param>
    ''' <param name="z2">The second complex number.</param>
    ''' <returns>The sum of the complex numbers.</returns>
    Public Shared Operator +(ByVal z1 As Complex, ByVal z2 As Complex) As Complex
        Return (New Complex(z1._real + z2._real, z1._imaginary + z2._imaginary))
    End Operator

    ''' <summary>
    ''' Subtracts the second complex number from the first.
    ''' </summary>
    ''' <param name="z1">The first complex number.</param>
    ''' <param name="z2">The second complex number.</param>
    ''' <returns>The difference of the complex numbers.</returns>
    Public Shared Operator -(ByVal z1 As Complex, ByVal z2 As Complex) As Complex
        Return (New Complex(z1._real - z2._real, z1._imaginary - z2._imaginary))
    End Operator

    ''' <summary>
    ''' Multiplies two complex numbers.
    ''' </summary>
    ''' <param name="z1">The first complex number.</param>
    ''' <param name="z2">The second complex number.</param>
    ''' <returns>The product of the two complex numbers.</returns>
    Public Shared Operator *(ByVal z1 As Complex, ByVal z2 As Complex) As Complex
        Return (New Complex(z1._real * z2._real - z1._imaginary * z2._imaginary, z1._real * z2._imaginary + z1._imaginary * z2._real))
    End Operator

    ''' <summary>
    ''' Divides two complex numbers.
    ''' </summary>
    ''' <param name="z1">The first complex number.</param>
    ''' <param name="z2">The second complex number.</param>
    ''' <returns>The quotient of the two complex numbers.</returns>
    Public Shared Operator /(ByVal z1 As Complex, ByVal z2 As Complex) As Complex
        If Math.Abs(z2.Real) > Math.Abs(z2.Imaginary) Then
            Dim x As Double = z2.Imaginary / z2.Real
            Dim w As Double = z2.Real + x * z2.Imaginary
            Return (New Complex((z1.Real + x * z1.Imaginary) / w, (z1.Imaginary - x * z1.Real) / w))
        Else
            Dim x As Double = z2.Real / z2.Imaginary
            Dim w As Double = x * z2.Real + z2.Imaginary
            Return (New Complex((x * z1.Real + z1.Imaginary) / w, (x * z1.Imaginary - z1.Real) / w))
        End If
    End Operator

    ' mixed double-complex binary operations

    ' these are not strictly necessary, since we have defined an implicit double->Complex casr
    ' but they are presumably faster than doing a cast and then an operation with zero im parts

    '        
    '		public static Complex operator+ (Complex z, double a) {
    '			return( new Complex(z.Re + a, z.Im) );
    '		}
    '
    '		public static Complex operator+ (double a, Complex z) {
    '			return( z + a );
    '		}
    '
    '		public static Complex operator- (Complex z, double a) {
    '			return( new Complex(z.Re -a, z.Im) );
    '		}
    '
    '		public static Complex operator- (double a, Complex z) {
    '			return( new Complex(a - z.Re, -z.Im) );
    '		}
    '        

    ''' <summary>
    ''' Multiplies a complex number by a real number.
    ''' </summary>
    ''' <param name="a">The real number.</param>
    ''' <param name="z">The complex number.</param>
    ''' <returns>The product az.</returns>
    Public Shared Operator *(ByVal a As Double, ByVal z As Complex) As Complex
        Return (New Complex(a * z._real, a * z._imaginary))
    End Operator

    ''' <summary>
    ''' Multiplies a real number by a complex number.
    ''' </summary>
    ''' <param name="z">The complex number.</param>
    ''' <param name="a">The real number.</param>
    ''' <returns>The product za.</returns>
    Public Shared Operator *(ByVal z As Complex, ByVal a As Double) As Complex
        Return (a * z)
    End Operator

    '        
    '		public static Complex operator/ (double a, Complex z) {
    '			if (Math.Abs(z.Re)>Math.Abs(z.Im)) {
    '				double x = z.Im/z.Re;
    '				double w = z.Re + x*z.Im;
    '				return( new Complex(a/w, -a*x/w) );
    '			} else {
    '				double x = z.Re/z.Im;
    '				double w = x*z.Re + z.Im;
    '				return( new Complex(a*x/w, -a/w) );
    '			}
    '		}
    '
    '		public static Complex operator/ (Complex z, double a) {
    '			return( new Complex(z.Re/a, z.Im/a) );
    '		}
    '        

End Structure
