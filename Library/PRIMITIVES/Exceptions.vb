''' <summary>
''' The exception that is thrown when attempting an operation on objects with incompatible dimensions.
''' </summary>
''' <license>
''' (c) 2012 David Wright (http://www.meta-numerics.net).
''' Licensed under the Microsoft Public License (Ms-PL). 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
<Serializable()>
Public Class DimensionMismatchException
    Inherits InvalidOperationException

    ''' <summary>
    ''' Initializes a new dimension mismatch exception.
    ''' </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new dimension mismatch exception with the given exception message.
    ''' </summary>
    ''' <param name="message">The exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initializes a new dimension mismatch exception with the given exception message and inner exception.
    ''' </summary>
    ''' <param name="message">The exception message.</param>
    ''' <param name="innerException">The inner exception.</param>
    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>
    ''' Initializes a new dimension mismatch exception with the given serialization information and streaming context.
    ''' </summary>
    ''' <param name="info">The serialization information.</param>
    ''' <param name="context">The streaming context.</param>
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub

End Class

''' <summary>
''' The exception that is thrown when an algorithm fails to converge.
''' </summary>
<Serializable()>
Public Class NonConvergenceException
    Inherits Exception

    ''' <summary>
    ''' Initializes a new non-convergence exception.
    ''' </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new non-convergence exception with the given exception message.
    ''' </summary>
    ''' <param name="message">The exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initializes a new non-convergence exception with the given exception message and inner exception.
    ''' </summary>
    ''' <param name="message">The exception message.</param>
    ''' <param name="innerException">The inner exception.</param>
    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>
    ''' Initializes a new non-convergence exception with the given serialization information and streaming context.
    ''' </summary>
    ''' <param name="info">The serialization information.</param>
    ''' <param name="context">The streaming context.</param>
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
    End Sub

End Class

''' <summary>Handles DLL errors.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
<Serializable()>
Public Class DllException
    Inherits isr.Numerics.Signals.BaseException

    Private Const _messageFormat As String = "{0}. The DLL returned error number {1}"

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="errorNumber">Specifies the error number from the DLL.</param>
    Public Sub New(ByVal message As String, ByVal errorNumber As Integer)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, Me._messageFormat, message, errorNumber))
        Me._errorNumber = errorNumber
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="errorNumber">Specifies the error number from the DLL.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal errorNumber As Integer,
                   ByVal innerException As System.Exception)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, Me._messageFormat, message, errorNumber), innerException)
        Me._errorNumber = errorNumber
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
                      ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    Private _errorNumber As Integer
    Public ReadOnly Property ErrorNumber() As Integer
        Get
            Return Me._errorNumber
        End Get
    End Property

    ''' <summary>Overrides the GetObjectData method to serialize custom values.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    <System.Security.SecurityCritical()>
    Public Overrides Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo,
                                       ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

End Class

