﻿''' <summary>
''' This structure encapsulates validation outcome and failure reason.
''' </summary>
''' <license>
''' (c) 2006 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Structure ValidationOutcome

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a validation outcome.</summary>
    ''' <param name="validated">True if validated.</param>
    ''' <param name="failureReason">Failure message.</param>
    Public Sub New(ByVal validated As Boolean, ByVal failureReason As String)
        Me._validated = validated
        Me._failureReason = failureReason
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>Returns True if equal.</summary>
    ''' <param name="left">The left hand side item to compare for equality</param>
    ''' <param name="right">The left hand side item to compare for equality</param>
    ''' <returns>Returns <c>True</c> if <paramref name="obj" /> both X and Y values are the same.</returns>
    Public Shared Operator =(ByVal left As ValidationOutcome, ByVal right As ValidationOutcome) As Boolean
        Return ValidationOutcome.Equals(left, right)
    End Operator

    ''' <summary>Returns True if not equal.</summary>
    ''' <param name="left">The left hand side item to compare for equality</param>
    ''' <param name="right">The left hand side item to compare for equality</param>
    ''' <returns>Returns <c>True</c> if <paramref name="obj" /> both X and Y values are the same.</returns>
    Public Shared Operator <>(ByVal left As ValidationOutcome, ByVal right As ValidationOutcome) As Boolean
        Return Not ValidationOutcome.Equals(left, right)
    End Operator

    ''' <summary>Returns True if equal.</summary>
    ''' <param name="left">The left hand side item to compare for equality</param>
    ''' <param name="right">The left hand side item to compare for equality</param>
    ''' <returns>Returns <c>True</c> if <paramref name="obj" /> both FailureReason and Validated  values are the same.</returns>
    Public Overloads Shared Function Equals(ByVal left As ValidationOutcome, ByVal right As ValidationOutcome) As Boolean

        Return left._failureReason.Equals(right._failureReason) 
          AndAlso left._validated.Equals(right._validated)

    End Function

    ''' <summary>Returns True if the value of the <paramref name="obj" /> equals to the
    '''   instance value.</summary>
    ''' <param name="obj">The object to compare for equality with this instance.
    '''   This object should be type <see cref="ValidationOutcome"/>.</param>
    ''' <returns>Returns <c>True</c> if <paramref name="obj" /> is the same value as this
    '''   instance; otherwise, <c>False</c></returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean

        If obj IsNot Nothing AndAlso TypeOf obj Is ValidationOutcome Then
            Return Equals(CType(obj, ValidationOutcome))
        Else
            Return False
        End If

    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="ValidationOutcome">Validation Outcome</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns>A Boolean data type</returns>
    ''' <remarks>Planar Ranges are the same if the have the same 
    ''' <see cref="FailureReason"/> and <see cref="Validated"/> ranges.</remarks>
    Public Overloads Function Equals(ByVal compared As ValidationOutcome) As Boolean
        Return ValidationOutcome.Equals(Me, compared)
    End Function

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._validated.GetHashCode Xor Me._failureReason.GetHashCode
    End Function

#End Region

#Region " PROPERTIES "

    Private _validated As Boolean
    ''' <summary>Gets or sets the condition for outcome was validated.
    ''' </summary>
    Public ReadOnly Property Validated() As Boolean
        Get
            Return Me._validated
        End Get
    End Property

    Private _failureReason As String
    ''' <summary>Gets or sets the failure reason description.
    ''' </summary>
    Public ReadOnly Property FailureReason() As String
        Get
            Return Me._failureReason
        End Get
    End Property

#End Region

End Structure
