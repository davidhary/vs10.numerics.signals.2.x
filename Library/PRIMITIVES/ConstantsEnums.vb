#Region " TYPES "

''' <summary>
''' Enumerates the filter types.
''' </summary>
Public Enum TaperFilterType
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("Low Pass")> LowPass
    <System.ComponentModel.Description("High Pass")> HighPass
    <System.ComponentModel.Description("Band Pass")> BandPass
    <System.ComponentModel.Description("Band Reject")> BandReject
End Enum

''' <summary>
''' Enumerates the taper windows types.
''' </summary>
Public Enum TaperWindowType
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("Bartlett")> Bartlett
    <System.ComponentModel.Description("Blackman")> Blackman
    <System.ComponentModel.Description("Cosine")> Cosine
    <System.ComponentModel.Description("Hamming")> Hamming
    <System.ComponentModel.Description("Hanning")> Hanning
End Enum

''' <summary>
''' Enumerates the FFT Types.
''' </summary>
Public Enum FourierTransformType
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("Dft")> Dft
    <System.ComponentModel.Description("Sliding")> Sliding
    <System.ComponentModel.Description("Mixed Radix")> MixedRadix
End Enum

#End Region

