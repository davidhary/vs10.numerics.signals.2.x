''' <summary>Provides shared services for the library.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public NotInheritable Class Helper

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>A private constructor to prevent the compiler from generating a 
    '''   default constructor.</summary>
    Private Sub New()
    End Sub

#End Region

#Region " MAGNITUDE "

    ''' <summary>Calculates magnitude of the Fourier transform.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <returns>The magnitude array</returns>
    Public Shared Function Magnitudes(ByVal reals() As Double, ByVal imaginaries() As Double) As Double()

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If

        Dim result(imaginaries.Length - 1) As Double

        ' calculate the magnitude
        For i As Integer = 0 To reals.Length - 1

            Dim realValue As Double = reals(i)
            Dim imaginaryValue As Double = imaginaries(i)
            result(i) = Math.Sqrt(realValue * realValue + imaginaryValue * imaginaryValue)

        Next i

        Return result

    End Function

    ''' <summary>Calculates magnitude of the Fourier transform.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <returns>The magnitude array</returns>
    Public Shared Function Magnitudes(ByVal reals() As Single, ByVal imaginaries() As Single) As Single()

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If

        Dim result(imaginaries.Length - 1) As Single

        ' calculate the magnitude
        For i As Integer = 0 To reals.Length - 1

            Dim realValue As Single = reals(i)
            Dim imaginaryValue As Single = imaginaries(i)
            result(i) = CSng(Math.Sqrt(realValue * realValue + imaginaryValue * imaginaryValue))

        Next i

        Return result

    End Function

#End Region

#Region " INTEGER ARRAYS "

    ''' <summary>Returns the index of the first maximum of given array.</summary>
    Public Shared Function IndexFirstMaximum(ByVal values() As Integer) As Integer

        If values Is Nothing OrElse values.Length = 0 Then
            Return -1
        End If

        Dim maxIndex As Integer = values.GetLowerBound(0)
        Dim max As Integer = values(maxIndex)
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            If max < values(i) Then
                max = values(i)
                maxIndex = i
            End If
        Next
        Return maxIndex

    End Function

#End Region

#Region " DOUBLE ARRAYS "

    ''' <summary>Adds a scalar to the array elements.</summary>
    Public Shared Function Add(ByVal values() As Double, ByVal scalar As Double) As Boolean

        If values Is Nothing OrElse values.Length = 0 Then
            Return False
        End If

        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            values(i) += scalar
        Next

        Return True

    End Function

    ''' <summary>Copy values between arrays.</summary>
    ''' <param name="fromValues"></param>
    ''' <param name="toValues"></param>
    Public Shared Function Copy(ByVal fromValues() As Double, ByVal toValues() As Double) As Boolean

        If fromValues Is Nothing Then
            Throw New ArgumentNullException("fromValues")
        End If
        If toValues Is Nothing Then
            Throw New ArgumentNullException("toValues")
        End If

        If fromValues.Length = 0 OrElse toValues.Length = 0 Then
            Return False
        End If

        For i As Integer = Math.Max(fromValues.GetLowerBound(0), fromValues.GetLowerBound(0)) 
            To Math.Min(fromValues.GetUpperBound(0), fromValues.GetUpperBound(0))
            toValues(i) = CSng(fromValues(i))
        Next

        Return True

    End Function


    ''' <summary>Copy values between arrays.</summary>
    ''' <param name="fromValues"></param>
    ''' <param name="toValues"></param>
    Public Shared Function Copy(ByVal fromValues() As Double, ByVal toValues() As Single) As Boolean

        If fromValues Is Nothing Then
            Throw New ArgumentNullException("fromValues")
        End If
        If toValues Is Nothing Then
            Throw New ArgumentNullException("toValues")
        End If

        If fromValues.Length = 0 OrElse toValues.Length = 0 Then
            Return False
        End If

        For i As Integer = Math.Max(fromValues.GetLowerBound(0), fromValues.GetLowerBound(0)) 
            To Math.Min(fromValues.GetUpperBound(0), fromValues.GetUpperBound(0))
            toValues(i) = CSng(fromValues(i))
        Next

        Return True

    End Function

    ''' <summary>Copy values between arrays.</summary>
    ''' <param name="fromValues"></param>
    ''' <param name="toValues"></param>
    Public Shared Function Copy(ByVal fromValues() As Single, ByVal toValues() As Double) As Boolean

        If fromValues Is Nothing Then
            Throw New ArgumentNullException("fromValues")
        End If
        If toValues Is Nothing Then
            Throw New ArgumentNullException("toValues")
        End If

        If fromValues.Length = 0 OrElse toValues.Length = 0 Then
            Return False
        End If

        For i As Integer = Math.Max(fromValues.GetLowerBound(0), fromValues.GetLowerBound(0)) 
            To Math.Min(fromValues.GetUpperBound(0), fromValues.GetUpperBound(0))
            toValues(i) = fromValues(i)
        Next

        Return True

    End Function

    ''' <summary>Returns the index of the first maximum of given array.</summary>
    Public Shared Function IndexFirstMaximum(ByVal values() As Double) As Integer

        If values Is Nothing OrElse values.Length = 0 Then
            Return -1
        End If

        Dim maxIndex As Integer = values.GetLowerBound(0)
        Dim max As Double = values(maxIndex)
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            If max < values(i) Then
                max = values(i)
                maxIndex = i
            End If
        Next
        Return maxIndex

    End Function

    ''' <summary>Calculates the mean of the array elements.</summary>
    Public Shared Function Mean(ByVal values() As Double) As Double

        If values Is Nothing OrElse values.Length = 0 Then
            Return 0
        End If

        Return Sum(values) / values.Length

    End Function

    ''' <summary>Removes the mean from the values array.</summary>
    ''' <returns>True if pass.</returns>
    Public Shared Function RemoveMean(ByVal values() As Double) As Boolean

        If values Is Nothing OrElse values.Length = 0 Then
            Return False
        Else
            Return Helper.Add(values, -Helper.Mean(values))
        End If

    End Function

    ''' <summary>Multiplies the array elements by a scalar.</summary>
    Public Shared Function Scale(ByVal values() As Double, ByVal scalar As Double) As Boolean

        If values Is Nothing OrElse values.Length = 0 Then
            Return False
        End If

        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            values(i) *= scalar
        Next

        Return True

    End Function

    ''' <summary>Swaps values between two array locations.</summary>
    ''' <param name="data">The array containing the data.</param>
    ''' <param name="i">An array index</param>
    ''' <param name="j">An array index</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="i")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="j")>
    Public Shared Sub Swap(ByVal data() As Double, ByVal i As Integer, ByVal j As Integer)

        If Not data Is Nothing Then
            Dim cache As Double = data(i)
            data(i) = data(j)
            data(j) = cache
        End If

    End Sub

    ''' <summary>Calculates the sum of the array elements.</summary>
    Public Shared Function Sum(ByVal values() As Double) As Double

        If values Is Nothing OrElse values.Length = 0 Then
            Return 0
        End If
        Dim cache As Double = 0
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            cache += values(i)
        Next
        Return cache

    End Function

    ''' <summary>Calculates the sum of the array elements squared.</summary>
    Public Shared Function SumSquares(ByVal values() As Double) As Double

        If values Is Nothing OrElse values.Length = 0 Then
            Return 0
        End If
        Dim cache As Double = 0
        Dim value As Double
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            value = values(i)
            cache += value * value
        Next
        Return cache

    End Function

    ''' <summary>Calculates the root mean square (RMS) value of the array elements.</summary>
    Public Shared Function RootMeanSquare(ByVal values() As Double) As Double

        If values Is Nothing OrElse values.Length = 0 Then
            Return 0
        End If

        Return Math.Sqrt(SumSquares(values) / values.Length)

    End Function

#End Region

#Region " SINGLE ARRAYS "

    ''' <summary>Adds a scalar to the array elements.</summary>
    Public Shared Function Add(ByVal values() As Single, ByVal scalar As Single) As Boolean

        If values Is Nothing OrElse values.Length = 0 Then
            Return False
        End If

        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            values(i) += scalar
        Next

        Return True

    End Function

    ''' <summary>Returns the index of the first maximum of given array.</summary>
    Public Shared Function IndexFirstMaximum(ByVal values() As Single) As Integer

        If values Is Nothing OrElse values.Length = 0 Then
            Return -1
        End If

        Dim maxIndex As Integer = values.GetLowerBound(0)
        Dim max As Single = values(maxIndex)
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            If max < values(i) Then
                max = values(i)
                maxIndex = i
            End If
        Next
        Return maxIndex

    End Function

    ''' <summary>Calculates the mean of the array elements.</summary>
    Public Shared Function Mean(ByVal values() As Single) As Single

        If values Is Nothing OrElse values.Length = 0 Then
            Return 0
        End If

        Return Sum(values) / values.Length

    End Function

    ''' <summary>Removes the mean from the values array.</summary>
    ''' <returns>True if pass.</returns>
    Public Shared Function RemoveMean(ByVal values() As Single) As Boolean

        If values Is Nothing OrElse values.Length = 0 Then
            Return False
        Else
            Return Helper.Add(values, -Helper.Mean(values))
        End If

    End Function

    ''' <summary>Multiplies the array elements by a scalar.</summary>
    Public Shared Function Scale(ByVal values() As Single, ByVal scalar As Single) As Boolean

        If values Is Nothing OrElse values.Length = 0 Then
            Return False
        End If

        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            values(i) *= scalar
        Next

        Return True

    End Function

    ''' <summary>Swaps values between two array locations.</summary>
    ''' <param name="data">The array containing the data.</param>
    ''' <param name="i">An array index</param>
    ''' <param name="j">An array index</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="i")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="j")>
    Public Shared Sub Swap(ByVal data() As Single, ByVal i As Integer, ByVal j As Integer)

        If Not data Is Nothing Then
            Dim cache As Single = data(i)
            data(i) = data(j)
            data(j) = cache
        End If

    End Sub

    ''' <summary>Calculates the sum of the array elements.</summary>
    Public Shared Function Sum(ByVal values() As Single) As Single

        If values Is Nothing OrElse values.Length = 0 Then
            Return 0
        End If
        Dim cache As Single = 0
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            cache += values(i)
        Next
        Return cache

    End Function

    ''' <summary>Calculates the sum of the array elements squared.</summary>
    Public Shared Function SumSquares(ByVal values() As Single) As Single

        If values Is Nothing OrElse values.Length = 0 Then
            Return 0
        End If
        Dim cache As Single = 0
        Dim value As Single
        For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
            value = values(i)
            cache += value * value
        Next
        Return cache

    End Function

    ''' <summary>Calculates the root mean square (RMS) value of the array elements.</summary>
    Public Shared Function RootMeanSquare(ByVal values() As Single) As Single

        If values Is Nothing OrElse values.Length = 0 Then
            Return 0
        End If

        Return CSng(Math.Sqrt(SumSquares(values) / values.Length))

    End Function

#End Region

End Class
