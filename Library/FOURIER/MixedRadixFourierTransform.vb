Imports System.Numerics
Imports System.Diagnostics.CodeAnalysis
''' <summary>
''' Calculates the Fast Fourier Transform using the Meta Numerics Library.
''' </summary>
''' <remarks>
''' Includes procedures for calculating the forward and inverse 
''' Fourier transform.
''' </remarks>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class MixedRadixFourierTransform

    Inherits FourierTransform

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(FourierTransformType.MixedRadix)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " COMPLEX "

    ''' <summary>
    ''' Calculates a Mixed-Radix Fourier transform.
    ''' </summary>
    ''' <param name="values">Holds the inputs and returns the outputs.</param>
    ''' <returns>Complex[][].</returns>
    ''' <exception cref="System.ArgumentNullException">values</exception>
    ''' <exception cref="System.ArgumentOutOfRangeException">values;Array must be longer than 1</exception>
    ''' <remarks>Swap reals and imaginaries to computer the inverse
    ''' Fourier transform.</remarks>
    Public Overloads Overrides Function Forward(ByVal values() As Complex) As Complex()

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("values", "Array must be longer than 1")
        End If

        Dim ft As FourierTransformer = New FourierTransformer(values.Length)
        Return ft.Transform(values)

    End Function

#End Region

#Region " DOUBLE "

    ''' <summary>
    ''' Calculates a Mixed-Radix Fourier transform.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <remarks>Swap reals and imaginaries to compute the inverse Fourier transform.
    ''' </remarks>
    Public Overloads Overrides Function Forward(ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean
        Me.Forward(ComplexExtensions.ToComplex(reals, imaginaries)).FromComplex(reals, imaginaries)
        Return True
    End Function

#End Region

#Region " SINGLE "

    ''' <summary>
    ''' Calculates the Mixed-Radix Fourier transform.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <remarks>Swap reals and imaginaries to computer the inverse Fourier transform.
    ''' </remarks>
    Public Overloads Overrides Function Forward(ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean
        Me.Forward(ComplexExtensions.ToComplex(reals, imaginaries)).FromComplex(reals, imaginaries)
        Return True
    End Function

#End Region

End Class

