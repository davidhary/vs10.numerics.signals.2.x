﻿Imports System.Numerics
''' <summary>
''' Class Transformlet
''' </summary>
''' <remarks>
''' Given a length-N DFT, we decompose N = R1 R2 ... Rn into prime factors R. The total DFT can then be expressed as a
''' series of length-R DFTs, where each length-R DFT is repeated N/R times. (Each R need not actually be prime, only
''' co-prime to the other factors.)
''' If a length-N DFT is O(N^2) and N = R1 R2, then a naive implementation would be order N^2 = R1^2 R2^2. But the
''' decomposed work is order (N / R1) R1^2 + (N / R2) R2^2 = R1 R2 (R1 + R2), which is less. We handle large prime
''' factors with the Bluestein algorithm.
''' Each length-R DFT is handled by a transformlet. We have a general transformlet for arbitrary R (the Transformlet class),
''' specialized dedicated transformlets for small values of R (LengthTwoTransformlet, LengthThreeTransformlet, etc.) and
''' a BluesteinTransformlet for larger R.
''' </remarks>
''' <license>
''' (c) 2012 David Wright (http://www.meta-numerics.net).
''' Licensed under the Microsoft Public License (Ms-PL). 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
Friend Class Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Transformlet" /> class.
    ''' </summary>
    ''' <param name="radix">The radix.</param>
    ''' <param name="totalLength">The total length.</param>
    ''' <param name="unitRoots">The Nth complex roots of unity.</param>
    Public Sub New(ByVal radix As Integer, ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New()
        Me._radix = radix
        Me._TotalLength = totalLength
        Me._UnityRoots = unitRoots
        Me._dx = totalLength \ radix
    End Sub

    Private _TotalLength As Integer
    ''' <summary>
    ''' Gets or sets the total length.
    ''' </summary>
    ''' <value>The total length.</value>
    Protected ReadOnly Property TotalLength As Integer
        Get
            Return Me._TotalLength
        End Get
    End Property

    Private _UnityRoots As Complex()
    ''' <summary>
    ''' Gets or sets the the Nth complex roots of unity.
    ''' </summary>
    ''' <value>The unity roots.</value>
    Protected ReadOnly Property UnityRoots As Complex()
        Get
            Return Me._UnityRoots
        End Get
    End Property

    Private _dx As Integer

    Private _radix As Integer
    ''' <summary>
    ''' Gets or sets the radix.
    ''' </summary>
    ''' <value>The radix.</value>
    Public ReadOnly Property Radix() As Integer
        Get
            Return Me._radix
        End Get
    End Property

    Private _multiplicity As Integer
    ''' <summary>
    ''' Gets or sets the multiplicity.
    ''' </summary>
    ''' <value>The multiplicity.</value>
    ''' <remarks>
    ''' we don't use the Multiplicity in any Transformlet methods, but it is used to execute the whole plan, and since 
    ''' there is one per transformlet we save ourselves from creating an additional container class by storing it in the Transformlet
    ''' </remarks>
    Public Property Multiplicity() As Integer
        Get
            Return Me._multiplicity
        End Get
        Friend Set(ByVal value As Integer)
            Me._multiplicity = value
        End Set
    End Property

    ''' <summary>
    ''' The FFT.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="ns">The ns.</param>
    ''' <param name="sign">The sign.</param>
    Public Overridable Sub FftPass(ByVal x() As Complex, ByVal y() As Complex, ByVal ns As Integer, ByVal sign As Integer)
        Dim v(Me._radix - 1) As Complex
        Dim dx As Integer = TotalLength \ Me._radix
        For j As Integer = 0 To dx - 1
            ' note: the j-loop could be parallelized, if the v-buffer is not shared between threads
            Dim xi As Integer = j
            Dim ui As Integer = 0
            If sign < 0 Then
                ui = TotalLength
            End If
            Dim du As Integer = (dx \ Ns) * (j Mod Ns)
            If sign < 0 Then
                du = -du
            End If
            ' basically, we need to copy x[j + r * dx] * u[r * du] into v[r]
            ' we do this in a complicated-looking way in order to avoid unnecessary multiplication when u = 1
            ' such a complex multiply requires 6 flops which results in a no-op; we have shown this to be a measurable time-saver
            If False Then
                ' all u-factors are 1, so we can just reference x directly without copying into v
                ' to do this, we need to change FftKernel to accept an offset and stride for x
            Else
                v(0) = x(xi) ' the first u is guaranteed to be 1
                For r As Integer = 1 To Me._radix - 1
                    xi += dx
                    ui += du
                    v(r) = x(xi) * UnityRoots(ui)
                Next r
            End If
            Dim y0 As Integer = Expand(j, Ns, Me._radix)
            FftKernel(v, y, y0, Ns, sign)
        Next j
    End Sub

    ''' <summary>
    ''' The FFT kernel.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="y0">The y0.</param>
    ''' <param name="dy">The dy.</param>
    ''' <param name="sign">The sign.</param>
    ''' <remarks>
    ''' This is an O(R^2) DFT. The only thing we have done to speed it up is special-case the u = 1 case to avoid
    ''' unnecessary complex multiplications. For good performance, override this with a custom kernel for each radix.
    ''' I am a little worried that this call is virtual. It's not in the innermost loop (which is inside it), but it
    ''' is in the next loop out. But the whole transformlet architecture gives a significant performance boost over
    ''' our last architecture, so it's a price I'm willing to pay for now.
    ''' </remarks>
    Public Overridable Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)

        ' x is the source vector, y is the target vector
        ' y0 is the initial y index, dy the stride
        ' sign gives the sign of the Fourier transform in the exponent

        ' yi is the y index we are currently computing; initialize it to y0
        Dim yi As Integer = y0

        ' the first index is the zero frequency component that just adds all the x's
        ' encode this specially so we are not unnecessarily multiplying by complex 1 R times
        y(yi) = 0.0
        For j As Integer = 0 To Me._radix - 1
            y(yi) += x(j)
        Next j

        ' now do the higher index entries
        For i As Integer = 1 To Me._radix - 1
            yi += dy
            y(yi) = x(0)
            Dim ui As Integer = 0
            If sign < 0 Then
                ui = TotalLength
            End If
            Dim du As Integer = Me._dx * i
            If sign < 0 Then
                du = -du
            End If
            For j As Integer = 1 To Me._radix - 1
                ui += du
                If ui >= TotalLength Then
                    ui -= TotalLength
                End If
                If ui < 0 Then
                    ui += TotalLength
                End If
                y(yi) += x(j) * UnityRoots(ui)
            Next j
        Next i

    End Sub

    ''' <summary>
    ''' Expands the specified idx L.
    ''' </summary>
    ''' <param name="idxL">The idx L.</param>
    ''' <param name="n1">The n1.</param>
    ''' <param name="n2">The n2.</param>
    ''' <returns>System.Int32.</returns>
    Protected Shared Function Expand(ByVal idxL As Integer, ByVal n1 As Integer, ByVal n2 As Integer) As Integer
        Return (idxL \ n1) * n1 * n2 + (idxL Mod n1)
    End Function

End Class

''' <summary>
''' Class RadixTwoTransformlet
''' </summary>
Friend Class RadixTwoTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixTwoTransformlet" /> class.
    ''' </summary>
    ''' <param name="totalLength">The total length.</param>
    ''' <param name="unitRoots">The unit roots.</param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(2, totalLength, unitRoots)
    End Sub

    ''' <summary>
    ''' The pass FFT.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="Ns">The ns.</param>
    ''' <param name="sign">The sign.</param>
    Public Overrides Sub FftPass(ByVal x() As Complex, ByVal y() As Complex, ByVal ns As Integer, ByVal sign As Integer)
        Dim dx As Integer = TotalLength \ 2
        For j As Integer = 0 To dx - 1
            Dim du As Integer = (dx \ ns) * (j Mod ns)
            Dim y0 As Integer = Expand(j, ns, 2)
            If sign < 0 Then
                FftKernel(x(j), x(j + dx) * UnityRoots(TotalLength - du), y(y0), y(y0 + ns))
            Else
                FftKernel(x(j), x(j + dx) * UnityRoots(du), y(y0), y(y0 + ns))
            End If
        Next j
    End Sub

    ''' <summary>
    ''' Performs a length-2 FFT.
    ''' </summary>
    ''' <param name="x0">The x0.</param>
    ''' <param name="x1">The x1.</param>
    ''' <param name="y0">The y0.</param>
    ''' <param name="y1">The y1.</param>
    Private Overloads Shared Sub FftKernel(ByVal x0 As Complex, ByVal x1 As Complex, ByRef y0 As Complex, ByRef y1 As Complex)

        Dim a0 As Double = x0.Real
        Dim b0 As Double = x0.Imaginary
        Dim a1 As Double = x1.Real
        Dim b1 As Double = x1.Imaginary
        y0 = New Complex(a0 + a1, b0 + b1)
        y1 = New Complex(a0 - a1, b0 - b1)
        ' for some reason, this looks to be faster than using the complex add and subtract; i don't see why

        ' this kernel has 4 flops, all adds/subs

        ' the naive R=2 kernel has 1 complex multiply and 2 complex adds
        ' a complex multiply requires 6 flops and complex add 2 ops
        ' so the naive kernel has 6 * 1 + 2 * 2 = 6 + 4 = 10 flops
        ' we have saved a factor 2.5

    End Sub

End Class

''' <summary>
''' Class RadixThreeTransformlet
''' </summary>
Friend Class RadixThreeTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixThreeTransformlet" /> class.
    ''' </summary>
    ''' <param name="totalLength">The total length.</param>
    ''' <param name="unitRoots">The unit roots.</param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(3, totalLength, unitRoots)
    End Sub

    ''' <summary>
    ''' The FFT pass.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="ns">The ns.</param>
    ''' <param name="sign">The sign.</param>
    Public Overrides Sub FftPass(ByVal x() As Complex, ByVal y() As Complex, ByVal ns As Integer, ByVal sign As Integer)
        Dim dx As Integer = TotalLength \ 3
        For j As Integer = 0 To dx - 1
            Dim du As Integer = (dx \ ns) * (j Mod ns)
            Dim y0 As Integer = Expand(j, ns, 3)
            If sign < 0 Then
                FftKernel(x(j), x(j + dx) * UnityRoots(TotalLength - du), x(j + 2 * dx) * UnityRoots(TotalLength - 2 * du), y(y0), y(y0 + ns), y(y0 + 2 * ns), -1)
            Else
                FftKernel(x(j), x(j + dx) * UnityRoots(du), x(j + 2 * dx) * UnityRoots(2 * du), y(y0), y(y0 + ns), y(y0 + 2 * ns), 1)
            End If
        Next j
    End Sub

    ''' <summary>
    ''' Performs a length-3 FFT.
    ''' </summary>
    ''' <param name="x0">The x0.</param>
    ''' <param name="x1">The x1.</param>
    ''' <param name="x2">The x2.</param>
    ''' <param name="y0">The y0.</param>
    ''' <param name="y1">The y1.</param>
    ''' <param name="y2">The y2.</param>
    ''' <param name="sign">The sign.</param>
    Private Overloads Shared Sub FftKernel(ByVal x0 As Complex, ByVal x1 As Complex, ByVal x2 As Complex, ByRef y0 As Complex, ByRef y1 As Complex, ByRef y2 As Complex, ByVal sign As Integer)
        Dim a12p As Double = x1.Real + x2.Real
        Dim b12p As Double = x1.Imaginary + x2.Imaginary
        Dim sa As Double = x0.Real + r31.Real * a12p
        Dim sb As Double = x0.Imaginary + r31.Real * b12p
        Dim ta As Double = r31.Imaginary * (x1.Real - x2.Real)
        Dim tb As Double = r31.Imaginary * (x1.Imaginary - x2.Imaginary)
        If sign < 0 Then
            ta = -ta
            tb = -tb
        End If
        y0 = New Complex(x0.Real + a12p, x0.Imaginary + b12p)
        y1 = New Complex(sa - tb, sb + ta)
        y2 = New Complex(sa + tb, sb - ta)

        ' this kernel has 16 flops

        ' the naive kernel for R=3 has 4 complex multiplies and 6 complex adds
        ' a complex multiply requires 6 flops and a complex add 2 flops
        ' so the naive kernel has 4 * 6 + 6 * 2 = 24 + 12 = 36 flops
        ' we have saved a factor 2.25, actually a bit more since we have also removed index loop accounting
    End Sub

    Private Shared ReadOnly r31 As New Complex(-1.0 / 2.0, Math.Sqrt(3.0) / 2.0)

End Class

''' <summary>
''' Class RadixFourTransformlet
''' </summary>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")>
Friend Class RadixFourTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixFourTransformlet" /> class.
    ''' </summary>
    ''' <param name="totalLength">The total length.</param>
    ''' <param name="unitRoots">The unit roots.</param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(4, totalLength, unitRoots)
    End Sub

    ''' <summary>
    ''' Performs a length-4 FFT.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="y0">The y0.</param>
    ''' <param name="dy">The dy.</param>
    ''' <param name="sign">The sign.</param>
    Public Overrides Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)

        Dim a02p As Double = x(0).Real + x(2).Real
        Dim b02p As Double = x(0).Imaginary + x(2).Imaginary
        Dim a02m As Double = x(0).Real - x(2).Real
        Dim b02m As Double = x(0).Imaginary - x(2).Imaginary
        Dim a13p As Double = x(1).Real + x(3).Real
        Dim b13p As Double = x(1).Imaginary + x(3).Imaginary
        Dim a13m As Double = x(1).Real - x(1).Real
        Dim b13m As Double = x(1).Imaginary - x(3).Imaginary

        y(y0) = New Complex(a02p + a13p, b02p + b13p)
        y(y0 + dy) = New Complex(a02m - b13m, b02m + a13m)
        y(y0 + 2 * dy) = New Complex(a02p - a13p, b02p - b13p)
        y(y0 + 3 * dy) = New Complex(a02m + b13m, b02m - a13m)

    End Sub

End Class

''' <summary>
''' Class RadixFiveTransformlet
''' </summary>
Friend Class RadixFiveTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixFiveTransformlet" /> class.
    ''' </summary>
    ''' <param name="totalLength">The total length.</param>
    ''' <param name="unitRoots">The unit roots.</param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(5, totalLength, unitRoots)
    End Sub

    ''' <summary>
    ''' Performs a length-5 FFT..
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="y0">The y0.</param>
    ''' <param name="dy">The dy.</param>
    ''' <param name="sign">The sign.</param>
    Public Overrides Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)
        ' first set of combinations
        Dim a14p As Double = x(1).Real + x(4).Real
        Dim a14m As Double = x(1).Real - x(4).Real
        Dim a23p As Double = x(2).Real + x(3).Real
        Dim a23m As Double = x(2).Real - x(3).Real
        Dim b14p As Double = x(1).Imaginary + x(4).Imaginary
        Dim b14m As Double = x(1).Imaginary - x(4).Imaginary
        Dim b23p As Double = x(2).Imaginary + x(3).Imaginary
        Dim b23m As Double = x(2).Imaginary - x(3).Imaginary
        ' second set of combinations, for v[1] and v[4]
        Dim s14a As Double = x(0).Real + r51.Real * a14p + r52.Real * a23p
        Dim s14b As Double = x(0).Imaginary + r51.Real * b14p + r52.Real * b23p
        Dim t14a As Double = r51.Imaginary * a14m + r52.Imaginary * a23m
        Dim t14b As Double = r51.Imaginary * b14m + r52.Imaginary * b23m
        ' second set of combinations, for v[2] and v[3]
        Dim s23a As Double = x(0).Real + r52.Real * a14p + r51.Real * a23p
        Dim s23b As Double = x(0).Imaginary + r52.Real * b14p + r51.Real * b23p
        Dim t23a As Double = r52.Imaginary * a14m - r51.Imaginary * a23m
        Dim t23b As Double = r52.Imaginary * b14m - r51.Imaginary * b23m
        ' take care of sign
        If sign < 0 Then
            t14a = -t14a
            t14b = -t14b
            t23a = -t23a
            t23b = -t23b
        End If
        ' bring together results
        y(y0) = New Complex(x(0).Real + a14p + a23p, x(0).Imaginary + b14p + b23p)
        y(y0 + dy) = New Complex(s14a - t14b, s14b + t14a)
        y(y0 + 2 * dy) = New Complex(s23a - t23b, s23b + t23a)
        y(y0 + 3 * dy) = New Complex(s23a + t23b, s23b - t23a)
        y(y0 + 4 * dy) = New Complex(s14a + t14b, s14b - t14a)
    End Sub

    Private Shared ReadOnly S5 As Double = Math.Sqrt(5.0)
    Private Shared ReadOnly r51 As New Complex((S5 - 1.0) / 4.0, Math.Sqrt((5.0 + S5) / 8.0))
    Private Shared ReadOnly r52 As New Complex(-(S5 + 1.0) / 4.0, Math.Sqrt((5.0 - S5) / 8.0))

End Class

''' <summary>
''' Class RadixSevenTransformlet
''' </summary>
Friend Class RadixSevenTransformlet
    Inherits Transformlet

    ''' <summary>
    ''' Initializes a new instance of the <see cref="RadixSevenTransformlet" /> class.
    ''' </summary>
    ''' <param name="totalLength">The total length.</param>
    ''' <param name="unitRoots">The unit roots.</param>
    Public Sub New(ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(7, totalLength, unitRoots)
    End Sub

    ''' <summary>
    ''' Performs the length 7 FFT.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="y0">The y0.</param>
    ''' <param name="dy">The dy.</param>
    ''' <param name="sign">The sign.</param>
    Public Overrides Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)
        ' relevant sums and differences
        Dim a16p As Double = x(1).Real + x(6).Real
        Dim a16m As Double = x(1).Real - x(6).Real
        Dim a25p As Double = x(2).Real + x(5).Real
        Dim a25m As Double = x(2).Real - x(5).Real
        Dim a34p As Double = x(3).Real + x(4).Real
        Dim a34m As Double = x(3).Real - x(4).Real
        Dim b16p As Double = x(1).Imaginary + x(6).Imaginary
        Dim b16m As Double = x(1).Imaginary - x(6).Imaginary
        Dim b25p As Double = x(2).Imaginary + x(5).Imaginary
        Dim b25m As Double = x(2).Imaginary - x(5).Imaginary
        Dim b34p As Double = x(3).Imaginary + x(4).Imaginary
        Dim b34m As Double = x(3).Imaginary - x(4).Imaginary
        ' combinations used in y[1] and y[6]
        Dim s16a As Double = x(0).Real + r71.Real * a16p + r72.Real * a25p + r73.Real * a34p
        Dim s16b As Double = x(0).Imaginary + r71.Real * b16p + r72.Real * b25p + r73.Real * b34p
        Dim t16a As Double = r71.Imaginary * a16m + r72.Imaginary * a25m + r73.Imaginary * a34m
        Dim t16b As Double = r71.Imaginary * b16m + r72.Imaginary * b25m + r73.Imaginary * b34m
        ' combinations used in y[2] and y[5]
        Dim s25a As Double = x(0).Real + r71.Real * a34p + r72.Real * a16p + r73.Real * a25p
        Dim s25b As Double = x(0).Imaginary + r71.Real * b34p + r72.Real * b16p + r73.Real * b25p
        Dim t25a As Double = r71.Imaginary * a34m - r72.Imaginary * a16m + r73.Imaginary * a25m
        Dim t25b As Double = r71.Imaginary * b34m - r72.Imaginary * b16m + r73.Imaginary * b25m
        ' combinations used in y[3] and y[4]
        Dim s34a As Double = x(0).Real + r71.Real * a25p + r72.Real * a34p + r73.Real * a16p
        Dim s34b As Double = x(0).Imaginary + r71.Real * b25p + r72.Real * b34p + r73.Real * b16p
        Dim t34a As Double = r71.Imaginary * a25m - r72.Imaginary * a34m - r73.Imaginary * a16m
        Dim t34b As Double = r71.Imaginary * b25m - r72.Imaginary * b34m - r73.Imaginary * b16m
        ' if sign is negative, invert t's
        If sign < 0 Then
            t16a = -t16a
            t16b = -t16b
            t25a = -t25a
            t25b = -t25b
            t34a = -t34a
            t34b = -t34b
        End If
        ' combine to get results
        y(y0) = New Complex(x(0).Real + a16p + a25p + a34p, x(0).Imaginary + b16p + b25p + b34p)
        y(y0 + dy) = New Complex(s16a - t16b, s16b + t16a)
        y(y0 + 2 * dy) = New Complex(s25a + t25b, s25b - t25a)
        y(y0 + 3 * dy) = New Complex(s34a + t34b, s34b - t34a)
        y(y0 + 4 * dy) = New Complex(s34a - t34b, s34b + t34a)
        y(y0 + 5 * dy) = New Complex(s25a - t25b, s25b + t25a)
        y(y0 + 6 * dy) = New Complex(s16a + t16b, s16b - t16a)
    End Sub

    ' seventh roots of unity
    ' a la Gauss, these are not expressible in closed form using rationals and rational roots

    Private Shared ReadOnly r71 As New Complex(0.62348980185873348, 0.7818314824680298)
    Private Shared ReadOnly r72 As New Complex(-0.22252093395631439, 0.97492791218182362)
    Private Shared ReadOnly r73 As New Complex(-0.90096886790241915, 0.43388373911755812)

End Class

''' <summary>
''' Class BluesteinTransformlet
''' </summary>
''' <remarks>
'''  The Bluestein technique works as follows:
'''  Given the length-N FT
'''  \tilde{x}_m = \sum_{n=0}^{N-1} x_n \exp{i \pm 2 \pi m n / N}
''' use m n = \frac{m^2 + n^2 - (m - n)^2}{2} to turn this into
'''   \tilde{x}_m = \exp{i \pm \pi m^2 / N} \sum_{n=0}^{N-1} x_n \exp{i \pm \pi n^2 / N} \exp{i \mp \pi (m - n)^2 / N}
''' The summed expression is a convolution of
'''   a_n = x_n \exp{i \pm \pi n^2 / N}
'''   b_n = \exp{i \mp \pi n^2 / N}
''' A convolution can be done via an FT of any length larger than 2N-1. The 2N is necessary so that a_0 can be multiplied
''' by b_{-N} and a_N can be multiplied by b_0. This thus the sequences to be convolved are
'''   0 0  0       0   0  a_0 a_1 a_2 ... a_n 0 0 0
'''   0 0 b_n ... b_2 b_1 b_0 b_1 b_2 ... b_n 0 0 0
''' Since this is a convolution, it doesn't matter how far out we zero-pad. We pick an M >= 2N-1 that is composed of
''' small prime factors, so we won't need the Bluestein technique to do the convolution itself.
''' </remarks>
Friend Class BluesteinTransformlet
    Inherits Transformlet

    Public Sub New(ByVal radix As Integer, ByVal totalLength As Integer, ByVal unitRoots() As Complex)
        MyBase.New(radix, totalLength, unitRoots)

        ' figure out the right Bluestein length and create a transformer for it
        Me._Nb = SetBluesteinLength(2 * radix - 1)
        Me._ft = New FourierTransformer(Me._Nb)

        ' compute the Bluestein coefficients and compute the FT of the filter based on them
        Me._b = ComputeBluesteinCoefficients(radix)
        Dim c(Me._Nb - 1) As Complex
        c(0) = 1.0
        For i As Integer = 1 To radix - 1
            c(i) = Me._b(i).Conjugate
            c(Me._Nb - i) = c(i)
        Next i
        Me._bt = Me._ft.Transform(c)

    End Sub

    ''' <summary>
    ''' The Length of convolution transform.
    ''' </summary>
    Private _Nb As Integer

    ''' <summary>
    ''' The Fourier transform for convolution transform
    ''' </summary>
    Private _ft As FourierTransformer

    ''' <summary>
    ''' The R Bluestein coefficients
    ''' </summary>
    Private _b() As Complex

    ''' <summary>
    ''' The Nb-length Fourier transform of the symmetric Bluestein coefficient filter.
    ''' </summary>
    Private _bt() As Complex

    ''' <summary>
    ''' Computes the Bluestein coefficients.
    ''' </summary>
    ''' <param name="radix">The radix.</param>
    ''' <returns>Complex[][].</returns>
    ''' <remarks>
    ''' This method computes b_n = \exp{i \pi n^2 / N}. If we do this naively, by computing sin and cos of \pi n^2 / N, then
    ''' the argument can get large, up to N \pi, and the inaccuracy of trig methods for large arguments will hit us
    ''' To avoid this, note that the difference n^2 - (n-1)^2 = 2n-1. So we can add 2n-1 each time and take the result mod 2N
    ''' to keep the argument less than 2 \pi.
    ''' </remarks>
    Private Shared Function ComputeBluesteinCoefficients(ByVal radix As Integer) As Complex()

        Dim b(radix - 1) As Complex
        Dim t As Double = Math.PI / radix
        b(0) = 1.0
        Dim s As Integer = 0
        Dim TwoR As Integer = 2 * radix
        For i As Integer = 1 To radix - 1
            s += (2 * i - 1)
            If s >= TwoR Then
                s -= TwoR
            End If
            Dim ts As Double = t * s
            b(i) = New Complex(Math.Cos(ts), Math.Sin(ts))
        Next i
        Return b
    End Function

    ''' <summary>
    ''' The FFT kernel.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="y0">The y0.</param>
    ''' <param name="dy">The dy.</param>
    ''' <param name="sign">The sign.</param>
    ''' <remarks>This is an O(R^2) DFT. The only thing we have done to speed it up is special-case the u = 1 case to avoid
    ''' unnecessary complex multiplications. For good performance, override this with a custom kernel for each radix.
    ''' I am a little worried that this call is virtual. It's not in the innermost loop (which is inside it), but it
    ''' is in the next loop out. But the whole transformlet architecture gives a significant performance boost over
    ''' our last architecture, so it's a price I'm willing to pay for now.</remarks>
    Public Overrides Sub FftKernel(ByVal x() As Complex, ByVal y() As Complex, ByVal y0 As Integer, ByVal dy As Integer, ByVal sign As Integer)

        ' all we have to do here is convolve (b x) with b-star
        ' to do this convolution, we need to multiply the DFT of (b x) with the DFT of b-star, the IDFT the result back
        ' we have already stored the DFT of b-star

        ' create c = b x and transform it into Fourier space
        Dim c(Me._Nb - 1) As Complex
        If sign > 0 Then
            For i As Integer = 0 To Radix - 1
                c(i) = Me._b(i) * x(i)
            Next i
        Else
            For i As Integer = 0 To Radix - 1
                c(i) = Me._b(i) * x(i).Conjugate
            Next i
        End If
        Dim ct() As Complex = Me._ft.Transform(c)

        ' multiply b-star and c = b x in Fourier space, and inverse transform the product back into configuration space
        For i As Integer = 0 To Me._Nb - 1
            ct(i) = ct(i) * Me._bt(i)
        Next i
        c = Me._ft.InverseTransform(ct)

        ' read off the result
        If sign > 0 Then
            For i As Integer = 0 To Radix - 1
                y(y0 + i * dy) = Me._b(i) * c(i)
            Next i
        Else
            For i As Integer = 0 To Radix - 1
                y(y0 + i * dy) = Me._b(i).Conjugate * c(i).Conjugate
            Next i
        End If

        ' for the sign < 0 case, we have used the fact that the convolution of (b-star x) with b is
        ' just the convolution of (b x-star) with b-star, starred

    End Sub

    ''' <summary>
    ''' Sets the Bluestein length.
    ''' </summary>
    ''' <param name="totalLength">The total length.</param>
    ''' <returns>System.Int32.</returns>
    ''' <remarks>
    ''' This is all about determining a good value to use for the Bluestein length. We choose a length based on powers
    ''' of two and three, since those give very fast Fourier transforms. Our method is heuristic and not optimized.
    ''' </remarks>
    Private Shared Function SetBluesteinLength(ByVal totalLength As Integer) As Integer

        ' try the next power of two
        Dim t As Integer = NextPowerOfBase(totalLength, 2)
        Dim M As Integer = t

        ' see if replacing factors of 4 by 3, which shortens the length, will still be long enough
        Do While M Mod 4 = 0
            t = (M \ 4) * 3
            If t < totalLength Then
                Exit Do
            End If
            If t < M Then
                M = t
            End If
        Loop

        ' try the next power of three
        t = NextPowerOfBase(totalLength, 3)
        If (t > 0) AndAlso (t < M) Then
            M = t
        End If

        Return M

    End Function

    ''' <summary>
    ''' Returns the next the power of base.
    ''' </summary>
    ''' <param name="n">The n.</param>
    ''' <param name="b">The b.</param>
    ''' <returns>System.Int32.</returns>
    Private Shared Function NextPowerOfBase(ByVal n As Integer, ByVal b As Integer) As Integer
        Dim m As Integer = b
        Do While m <= Int32.MaxValue
            If m >= n Then
                Return m
            End If
            m *= b
        Loop
        Return -1
    End Function

End Class

''' <summary>
''' Class FourierAlgorithms
''' </summary>
Friend NotInheritable Class FourierAlgorithms

    ''' <summary>
    ''' Prevents a default instance of the <see cref="FourierAlgorithms" /> class from being created.
    ''' </summary>
    Private Sub New()
    End Sub

    ''' <summary>
    ''' The 2*PI
    ''' </summary>
    Public Const TwoPI As Double = 2.0 * Math.PI

    ''' <summary>
    ''' Computes Computes the Nth roots of unity, which are the factors in a length-N Fourier transform.
    ''' </summary>
    ''' <param name="totalLength">The total length (N).</param>
    ''' <param name="sign">The sign.</param>
    ''' <returns>Complex[][].</returns>
    Public Shared Function ComputeRoots(ByVal totalLength As Integer, ByVal sign As Integer) As Complex()
        Dim u(totalLength) As Complex
        Dim t As Double = sign * TwoPI / totalLength
        u(0) = 1.0
        For r As Integer = 1 To totalLength - 1
            Dim rt As Double = r * t
            u(r) = New Complex(Math.Cos(rt), Math.Sin(rt))
        Next r
        u(totalLength) = 1.0
        Return u
    End Function

End Class
