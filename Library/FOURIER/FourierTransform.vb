Imports System.Numerics
''' <summary>Provides a base class for fast and discrete Fourier transform calculations.</summary>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public MustInherit Class FourierTransform

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="transformType">Specifies the fourier transform type.</param>
    Protected Sub New(ByVal transformType As FourierTransformType)

        ' instantiate the base class
        MyBase.New()

        Me._transformType = transformType

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._sineTable = Nothing
                    Me._cosineTable = Nothing
                    Me._realCache = Nothing
                    Me._imaginaryCache = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " PROPERTIES "

    Private _elementCount As Integer
    ''' <summary>Gets or sets the local size of the time series arrays used for calculating the 
    '''   Fourier transform.  This value is held separately allowing the first call to 
    '''   determine if any cache needs to be recalculated in case of the time series
    '''   length is different from the previous calculation. Setting the time
    '''   series length also sets the 
    '''   <see cref="HalfSpectrumLength">length of the half spectrum</see>.</summary>
    Protected Overridable Property ElementCount() As Integer
        Get
            Return Me._elementCount
        End Get
        Set(ByVal Value As Integer)
            Me._elementCount = Value
            Me._timeSeriesLength = Value
            Me._halfSpectrumLength = CInt(Math.Floor(Me._timeSeriesLength / 2) + 1)
        End Set
    End Property

    Private _halfSpectrumLength As Integer
    ''' <summary>The number of elements in half the spectrum..</summary>
    Public ReadOnly Property HalfSpectrumLength() As Integer
        Get
            Return Me._halfSpectrumLength
        End Get
    End Property

    Private _timeSeriesLength As Integer
    ''' <summary>Returns the number of elements in the time series sample
    '''   that was used to compute the Fourier transform.
    ''' </summary>
    Public ReadOnly Property TimeSeriesLength() As Integer
        Get
            Return Me._timeSeriesLength
        End Get
    End Property

    Private _transformType As FourierTransformType
    ''' <summary>Gets or sets the Fourier Transform type.</summary>
    Public ReadOnly Property TransformType() As FourierTransformType
        Get
            Return Me._transformType
        End Get
    End Property

#End Region

#Region " COMPLEX "

    ''' <summary>
    ''' Calculates the Forward Fourier Transform.
    ''' </summary>
    ''' <param name="values">Holds the inputs and returns the outputs.</param>
    ''' <returns>Complex[][].</returns>
    ''' <remarks>Swap reals and imaginaries to computer the inverse
    ''' Fourier transform.</remarks>
    Public Overridable Function Forward(ByVal values() As Complex) As Complex()
        ' Initialize if new transform.  Move out to the calling methods
        Me.Initialize(values)
        Return values
    End Function

    ''' <summary>Initializes the transform.</summary>
    ''' <param name="values">Holds the inputs and returns the outputs.</param>
    Public Overridable Function Initialize(ByVal values() As Complex) As Boolean

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("values", "Array must be longer than 1")
        End If

        ' set the element count
        Me.ElementCount = values.Length

        Return True

    End Function

    ''' <summary>
    ''' Calculates the Inverse Fourier Transform.
    ''' </summary>
    ''' <param name="values">Holds the inputs and returns the outputs.</param>
    ''' <returns>Complex[][].</returns>
    ''' <exception cref="System.ArgumentNullException">values</exception>
    ''' <exception cref="System.ArgumentOutOfRangeException">values;Array must be longer than 1</exception>
    Public Function Inverse(ByVal values() As Complex) As Complex()

        If values Is Nothing Then
            Throw New System.ArgumentNullException("values")
        End If
        If values.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("values", "Array must be longer than 1")
        End If
        values.Swap()
        Return Me.Forward(values)

    End Function

#End Region

#Region " DOUBLE "

    ''' <summary>Calculates the Forward Fourier Transform.</summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <remarks>Swap reals and imaginaries to computer the inverse 
    '''   Fourier transform.
    ''' TO_DO: Remove initialize and place in the spectrum calculations.
    ''' </remarks>
    Public Overridable Function Forward(ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean
        ' Initialize if new transform.  Move out to the calling methods
        Me.Initialize(reals, imaginaries)
    End Function

    ''' <summary>Initializes the transform.</summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    Public Overridable Function Initialize(ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' set the element count
        Me.ElementCount = reals.Length

    End Function

    ''' <summary>Calculates the Inverse Fourier Transform.</summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    Public Function Inverse(ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        Me.Forward(imaginaries, reals)

    End Function

#End Region

#Region " SINGLE "

    ''' <summary>Calculates the Forward Fourier Transform.</summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <remarks>Swap reals and imaginaries to computer the inverse 
    '''   Fourier transform.
    ''' </remarks>
    Public Overridable Function Forward(ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean
        ' Initialize if new transform.  Move out to the calling methods
        Me.Initialize(reals, imaginaries)
    End Function

    ''' <summary>Initializes the transform.</summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    Public Overridable Function Initialize(ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' set the element count
        Me.ElementCount = reals.Length

    End Function

    ''' <summary>Calculates the Inverse Fourier Transform.</summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    Public Function Inverse(ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        Me.Forward(imaginaries, reals)

    End Function

#End Region

#Region " PROTECTED "

    ''' <summary>Gets or sets the cosine table</summary>
    Private _cosineTable() As Double
    ''' <summary>Returns the cosine table</summary>
    Protected Function CosineTable() As Double()
        Return Me._cosineTable
    End Function

    ''' <summary>Gets or sets the sine table</summary>
    Private _sineTable() As Double
    ''' <summary>Returns the sine table</summary>
    Protected Function SineTable() As Double()
        Return Me._sineTable
    End Function

    Private _realCache() As Double
    ''' <summary>Returns the cache of real values.</summary>
    Protected Function RealCache() As Double()
        Return Me._realCache
    End Function

    ''' <summary>Gets or sets a cache for the imaginary-part of the DFT.</summary>
    Private _imaginaryCache() As Double
    ''' <summary>Returns the cache of imaginary values.</summary>
    Protected Function ImaginaryCache() As Double()
        Return Me._imaginaryCache
    End Function

    Private _dftTablesSize As Integer
    ''' <summary>Builds sine and cosine tables and allocate cache for calculating the
    '''   DFT.  These DFT tables start at Delta and end at 360 degrees and are 
    '''   suitable for calculating the DFT for the Mixed Radix Algorithm.</summary>
    ''' <param name="elementCount">Specifies the size of the sine tables.</param>
    Protected Function BuildShiftedDftTables(ByVal elementCount As Integer) As Boolean

        ' throw an exception if size not right
        If elementCount <= 1 Then
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, "Must be greater than 1")
        End If

        ' exit if sine tables already constructed
        If Me._dftTablesSize = elementCount Then
            Return True
        End If

        ' store the new size
        Me._dftTablesSize = elementCount

        ' allocate the sine and cosine tables 
        ReDim _cosineTable(elementCount - 1)
        ReDim _sineTable(elementCount - 1)
        ReDim _realCache(elementCount - 1)
        ReDim _imaginaryCache(elementCount - 1)

        ' cosine and sine factors
        Dim delta As Double = 2 * Math.PI / Convert.ToDouble(elementCount)
        Dim cosineDelta As Double = Math.Cos(delta)
        Dim sineDelta As Double = Math.Sin(delta)

        ' Sine and cosine temporary values
        Dim sineAlpha As Double
        Dim cosineAlpha As Double

        ' start at last element 
        Dim beta As Integer = elementCount - 1

        ' Set last element of Cosine table to 1.0
        Dim cosineBeta As Double = 1.0#
        Me._cosineTable(beta) = cosineBeta

        ' set last element of Sine table to 0.0
        Dim sineBeta As Double = 0.0#
        Me._sineTable(beta) = sineBeta

        ' Use trigonometric relationships to build the tables
        Dim alpha As Integer = 0
        Do
            cosineAlpha = cosineBeta * cosineDelta + sineBeta * sineDelta
            sineAlpha = cosineBeta * sineDelta - sineBeta * cosineDelta
            cosineBeta = cosineAlpha
            sineBeta = -sineAlpha

            Me._cosineTable(alpha) = cosineAlpha
            Me._sineTable(alpha) = sineAlpha

            beta -= 1
            Me._cosineTable(beta) = cosineBeta
            Me._sineTable(beta) = sineBeta

            alpha += 1

        Loop While alpha < beta

        Return True

    End Function

    ''' <summary>Builds sine and cosine tables and allocate cache for calculating the
    '''   DFT.  These DFT tables start at zero and end at 360 - delta degrees and are 
    '''   suitable for calculating the Discrete Fourier transform and the 
    '''   sliding FFT.</summary>
    ''' <param name="elementCount">Specifies the size of the sine tables.</param>
    Protected Function BuildDftTables(ByVal elementCount As Integer) As Boolean

        ' throw an exception if size not right
        If elementCount <= 1 Then
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, "Must be greater than 1")
        End If

        ' exit if sine tables already constructed
        If Me._dftTablesSize = elementCount Then
            Return True
        End If

        ' store the new size
        Me._dftTablesSize = elementCount

        ' allocate the sine and cosine tables 
        ReDim _cosineTable(elementCount - 1)
        ReDim _sineTable(elementCount - 1)

        ' cosine and sine factors
        Dim delta As Double = 2 * Math.PI / Convert.ToDouble(elementCount)
        Dim cosineDelta As Double = Math.Cos(delta)
        Dim sineDelta As Double = Math.Sin(delta)

#If False Then
    Dim omega As Double
    For i As integer = 0 To elementCount - 1
      omega = delta * i
      Me._cosineTable(i) = System.Math.Cos(omega)
      Me._sineTable(i) = System.Math.Sin(omega)
    Next i
#End If

        ' start at the first element
        Dim alpha As Integer = 0
        Dim beta As Integer = elementCount

        ' Sine and cosine temporary values
        Dim sineAlpha As Double = 0.0R
        Dim cosineAlpha As Double = 1.0R
        Dim cosineBeta As Double
        Dim sineBeta As Double

        ' Set first element of Cosine table to 1.0
        CosineTable(alpha) = cosineAlpha

        ' set first element of Sine table to 0.0
        SineTable(alpha) = sineAlpha

        ' Use trigonometric and inverse relationships to
        ' build the tables
        Do
            ' Get the next angle for the left end size, which
            ' equals the current angle plus the base angle
            cosineBeta = cosineAlpha * cosineDelta - sineAlpha * sineDelta
            sineBeta = cosineAlpha * sineDelta + sineAlpha * cosineDelta

            ' Store the new values
            cosineAlpha = cosineBeta
            sineAlpha = sineBeta

            ' Save the next angle on the left-end
            alpha += 1
            CosineTable(alpha) = cosineAlpha
            SineTable(alpha) = sineAlpha

            ' Save the right-end elements as the mirror image
            ' of the left end elements
            beta -= 1
            CosineTable(beta) = cosineBeta
            SineTable(beta) = -sineBeta

        Loop While alpha < beta

        Return True

    End Function

    ''' <summary>Copies the real- and imaginary-parts to the cache.
    ''' </summary>
    ''' <param name="reals"></param>
    ''' <param name="imaginaries"></param>
    Protected Sub CopyToCache(ByVal reals() As Double, ByVal imaginaries() As Double)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        ' copies arrays to the cache
        ReDim _realCache(reals.Length - 1)
        reals.CopyTo(Me._realCache, 0)
        ReDim _imaginaryCache(imaginaries.Length - 1)
        imaginaries.CopyTo(Me._imaginaryCache, 0)

    End Sub

    ''' <summary>Copies the real- and imaginary-parts to the cache.
    ''' </summary>
    ''' <param name="reals"></param>
    ''' <param name="imaginaries"></param>
    Protected Sub CopyToCache(ByVal reals() As Single, ByVal imaginaries() As Single)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        ' copies arrays to the cache
        ReDim _realCache(reals.Length - 1)
        isr.Numerics.Signals.Helper.Copy(reals, Me._realCache)
        ReDim _imaginaryCache(imaginaries.Length - 1)
        isr.Numerics.Signals.Helper.Copy(imaginaries, Me._imaginaryCache)

    End Sub

#End Region

End Class
