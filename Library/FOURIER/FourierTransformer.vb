﻿Imports System.Numerics
''' <summary>
''' Specifies the normalization convention to be used in a forward Fourier transform.
''' </summary>
''' <remarks>
''' <para>The most common convention in signal processing applications is <see cref="FourierNormalization.None"/>.</para>
''' </remarks>
Public Enum FourierNormalization

    ''' <summary>
    ''' The series is not normalized.
    ''' </summary>
    None

    ''' <summary>
    ''' The series is multiplied by 1/N<sup>1/2</sup>.
    ''' </summary>
    Unitary

    ''' <summary>
    ''' The series is multiplied by 1/N.
    ''' </summary>
    Inverse
End Enum

''' <summary>
''' Specifies the sign convention to be used in the exponent of a forward Fourier transform.
''' </summary>
''' <remarks>
''' <para>The most common convention in signal processing applications is <see cref="FourierSign.Negative"/>.</para>
''' </remarks>
Public Enum FourierSign

    ''' <summary>
    ''' The exponent has positive imaginary values.
    ''' </summary>
    Positive

    ''' <summary>
    ''' The exponent has negative imaginary values.
    ''' </summary>
    Negative
End Enum

''' <summary>
''' An engine for performing Fourier transforms on complex series.
''' </summary>
''' <example>
'''   <para>The following code performs a simple DFT and then inverts it to re-obtain the original data.</para>
'''   <code lang="c#">
''' // Create a Fourier transformer for length-6 series
''' FourierTransformer ft = new FourierTransformer(6);
''' // Create a length-6 series and transform it
''' Complex[] x = new Complex[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0 };
''' Complex[] xt = ft.Transform(x);
''' // Re-use the same transformer to transform a different  series
''' Complex[] y = new Complex[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
''' Complex[] yt = ft.Transform(y);
''' // Transform them back
''' Complex[] xtt = ft.InverseTransform(xt);
''' Complex[] ytt = ft.InverseTransform(yt);
'''   </code>
'''   </example>
'''   <seealso href="http://en.wikipedia.org/wiki/Discrete-time_Fourier_transform" />
'''   <license>
''' (c) 2012 David Wright (http://www.meta-numerics.net).
''' Licensed under the Microsoft Public License (Ms-PL).
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'''   </license>
''' <remarks><para>A Fourier transform decomposes a function into a sum of different frequency components. This is
''' useful for a wide array of applications.</para>
'''   <para>Mathematically, the DFT is an N-dimensional linear transformation
''' with coefficients that are the Nth complex roots of unity.</para>
'''   <img src="../images/Fourier.png" />
'''   <para>An instance of the FourierTransformer class performs DFTs on series of a particular length,
''' given by its <see cref="FourierTransformer.Length" /> property. This specialization allows certain parts of the DFT
''' calculation, which are independent of the transformed series but dependent on the length of the series,
''' to be performed only once and then re-used for all transforms of that length. This saves time and improves
''' performance. If you need to perform DFTs on series with different lengths, simply create a separate instance
''' of the FourierTransform class for each required length.</para>
'''   <para>Many simple DFT implementations require that the series length be a power of two (2, 4, 8, 16, etc.).
''' Meta.Numerics supports DFTs of any length. Our DFT implementation is fast -- order O(N log N) -- for all lengths,
''' including lengths that have large prime factors.</para></remarks>
Public NotInheritable Class FourierTransformer

    ''' <summary>
    ''' Initializes a new instance of the Fourier transformer.
    ''' </summary>
    ''' <param name="size">The series length of the transformer, which must be positive.</param>
    Public Sub New(ByVal size As Integer)
        Me.New(size, FourierSign.Negative, FourierNormalization.None)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the Fourier transformer with the given sign and normalization conventions.
    ''' </summary>
    ''' <param name="size">The series length of the transformer, which must be positive.</param>
    ''' <param name="signConvention">The sign convention of the transformer.</param>
    ''' <param name="normalizationConvention">The normalization convention of the transformer.</param>
    ''' <remarks>
    ''' <para>There are multiple conventions for both the sign of the exponent and the overall normalization of
    ''' Fourier transforms. The default conventions for some widely used software packages are summarized in the following
    ''' table.</para>
    ''' <table>
    '''     <tr><th>Software</th><th>Sign</th><th>Normalization</th></tr>
    '''     <tr><td>Meta.Numerics</td><td><see cref="FourierSign.Negative"/></td><td><see cref="FourierNormalization.None"/></td></tr>
    '''     <tr><td>Matlab</td><td><see cref="FourierSign.Negative"/></td><td><see cref="FourierNormalization.None"/></td></tr>
    '''     <tr><td>Mathematica</td><td><see cref="FourierSign.Positive"/></td><td><see cref="FourierNormalization.Unitary"/></td></tr>
    '''     <tr><td>Numerical Recipes</td><td><see cref="FourierSign.Positive"/></td><td><see cref="FourierNormalization.None"/></td></tr>
    ''' </table>
    ''' </remarks>
    Public Sub New(ByVal size As Integer, ByVal signConvention As FourierSign, ByVal normalizationConvention As FourierNormalization)

        If size < 1 Then
            Throw New ArgumentOutOfRangeException("size")
        End If

        Me._size = size
        Me._signConvention = signConvention
        Me._normalizationConvention = normalizationConvention

        ' pre-compute the Nth complex roots of unity
        Me._roots = FourierAlgorithms.ComputeRoots(size, +1)

        ' decompose the size into prime factors
        Me._factors = AdvancedMath.Factor(size)

        ' store a plan for the transform based on the prime factorization
        Me._plan = New Generic.List(Of Transformlet)()
        For Each factor As Factor In Me._factors

            Dim t As Transformlet
            Select Case factor.Value
                ' use a radix-specialized transformlet when available
                Case 2
                    t = New RadixTwoTransformlet(size, Me._roots)
                Case 3
                    t = New RadixThreeTransformlet(size, Me._roots)
                    ' eventually, we should make an optimized radix-4 transform
                Case 5
                    t = New RadixFiveTransformlet(size, Me._roots)
                Case 7
                    t = New RadixSevenTransformlet(size, Me._roots)
                Case 11, 13
                    ' the base transformlet is R^2, but when R is small, this can still be faster than the Bluestein algorithm
                    ' timing measurements appear to indicate that this is the case for radix 11 and 13
                    ' eventually, we should make optimized Winograd transformlets for these factors
                    t = New Transformlet(factor.Value, size, Me._roots)
                Case Else
                    ' for large factors with no available specialized transformlet, use the Bluestein algorithm
                    t = New BluesteinTransformlet(factor.Value, size, Me._roots)
            End Select

            t.Multiplicity = factor.Multiplicity

            '                
            '                if ((factor.Value == 2) && (factor.Multiplicity % 2 == 0)) {
            '                    t = new RadixFourTransformlet(size, roots);
            '                    t.Multiplicity = factor.Multiplicity / 2;
            '                }
            '                

            Me._plan.Add(t)

        Next factor

    End Sub

    Private _factors As Generic.List(Of Factor)
    Private _plan As Generic.List(Of Transformlet)
    Private _roots() As Complex

    Private _size As Integer
    ''' <summary>
    ''' The series length for which the transformer is specialized.
    ''' </summary>
    Public ReadOnly Property Length() As Integer
        Get
            Return Me._size
        End Get
    End Property

    Private _normalizationConvention As FourierNormalization
    ''' <summary>
    ''' Gets the normalization convention used by the transformer.
    ''' </summary>
    Public ReadOnly Property NormalizationConvention() As FourierNormalization
        Get
            Return Me._normalizationConvention
        End Get
    End Property

    Private _signConvention As FourierSign
    ''' <summary>
    ''' Gets the normalization convention used by the transformer.
    ''' </summary>
    Public ReadOnly Property SignConvention() As FourierSign
        Get
            Return Me._signConvention
        End Get
    End Property

    ''' <summary>
    ''' Gets the sign.
    ''' </summary>
    ''' <returns>System.Int32.</returns>
    Private Function GetSign() As Integer
        If Me._signConvention = FourierSign.Positive Then
            Return +1
        Else
            Return -1
        End If
    End Function

    ''' <summary>
    ''' Normalizes the specified x.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="f">The f.</param>
    Private Shared Sub Normalize(ByVal x() As Complex, ByVal f As Double)
        For i As Integer = 0 To x.Length - 1
            x(i) = New Complex(f * x(i).Real, f * x(i).Imaginary)
        Next i
    End Sub

    ''' <summary>
    ''' Transforms the specified x.
    ''' </summary>
    ''' <param name="x">The x.</param>
    ''' <param name="y">The y.</param>
    ''' <param name="sign">The sign.</param>
    ''' <remarks>
    ''' This is an internal transform method that does not do checking, modifies the input array, and
    ''' requires you to give it a scratch array. x is the input array, which is overwritten by the output
    ''' array, and y is a scratch array of the same length. The transform works by carrying out each
    ''' transformlet in the plan, with input from x and output to y, then switching y and x so that
    ''' the input is in x for the next transformlet.
    ''' </remarks>
    Friend Sub Transform(ByRef x() As Complex, ByRef y() As Complex, ByVal sign As Integer)
        Dim Ns As Integer = 1
        For Each t As Transformlet In Me._plan
            For k As Integer = 0 To t.Multiplicity - 1
                t.FftPass(x, y, Ns, sign)
                ' we avoid element-by-element copying by just switching the arrays referenced by x and y
                ' this is why x and y must be passed in with the ref keyword
                Dim temp() As Complex = x
                x = y
                y = temp
                Ns *= t.Radix
            Next k
        Next t
    End Sub

    ''' <summary>
    ''' Computes the Fourier transform of the given series.
    ''' </summary>
    ''' <param name="values">The series to transform.</param>
    ''' <returns>The discrete Fourier transform of the series.</returns>
    Public Function Transform(ByVal values As Generic.IList(Of Complex)) As Complex()
        If values Is Nothing Then
            Throw New ArgumentNullException("values")
        End If
        If values.Count <> size Then
            Throw New DimensionMismatchException()
        End If

        ' copy the original values into a new array
        Dim x(Me._size - 1) As Complex
        values.CopyTo(x, 0)

        ' normalize the copy appropriately
        If Me._normalizationConvention = FourierNormalization.Unitary Then
            Normalize(x, 1.0 / Math.Sqrt(Me._size))
        ElseIf Me._normalizationConvention = FourierNormalization.Inverse Then
            Normalize(x, 1.0 / Me._size)
        End If

        ' create a scratch array
        Dim y(Me._size - 1) As Complex

        ' do the FFT
        Transform(x, y, GetSign())
        'FourierAlgorithms.Fft(values.Count, factors, ref x, ref y, roots, GetSign());

        Return x

    End Function

    ''' <summary>
    ''' Computes the inverse Fourier transform of the given series.
    ''' </summary>
    ''' <param name="values">The series to invert.</param>
    ''' <returns>The inverse discrete Fourier transform of the series.</returns>
    Public Function InverseTransform(ByVal values As Generic.IList(Of Complex)) As Complex()
        If values Is Nothing Then
            Throw New ArgumentNullException("values")
        End If
        If values.Count <> size Then
            Throw New DimensionMismatchException()
        End If

        ' copy the original values into a new array
        Dim x(Me._size - 1) As Complex
        values.CopyTo(x, 0)

        ' normalize the copy appropriately
        If Me._normalizationConvention = FourierNormalization.None Then
            Normalize(x, 1.0 / Me._size)
        ElseIf Me._normalizationConvention = FourierNormalization.Unitary Then
            Normalize(x, 1.0 / Math.Sqrt(Me._size))
        End If

        ' create a scratch array
        Dim y(Me._size - 1) As Complex

        ' do the FFT
        Transform(x, y, -GetSign())
        'FourierAlgorithms.Fft(values.Count, factors, ref x, ref y, roots, -GetSign());

        Return x

    End Function

End Class
