''' <summary>Calculates the Discrete Fourier Transform.</summary>
''' <remarks>Includes procedures for calculating the forward and inverse DFT.<p>
''' Benchmarks:</p><p>
''' Data    Data  Speed</p><p>
''' Points  Type   (ms)</p><p>
'''   256   Double  less than 2</p><p>
'''   512   Double  4-5</p><p>
'''  1000   Double  9-10</p><p>
'''  1024   Double 10-11</p><p>
'''  2000   Double 19-20</p><p>
'''  2048   Double 26</p>
''' </remarks>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class DiscreteFourierTransform

    Inherits FourierTransform

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New(FourierTransformType.Dft)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Calculates the Discrete Fourier Transform.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <remarks>Computes the DFT directly without using any of the special rotation 
    '''   and permutation algorithms that are part of the Fast Fourier Transform (FFT)
    '''   literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
    ''' </remarks>
    Public Overloads Overrides Function Forward(ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' build the DFT tables
        MyBase.BuildDftTables(reals.Length)

        ' Set element count
        MyBase.ElementCount = reals.Length

        ' save initial values to the cache
        MyBase.CopyToCache(reals, imaginaries)

        Dim realValue As Double
        Dim imaginaryValue As Double
        Dim k As Integer
        For j As Integer = 0 To MyBase.ElementCount - 1
            realValue = MyBase.RealCache(0)
            imaginaryValue = MyBase.ImaginaryCache(0)
            k = 0
            For i As Integer = 1 To MyBase.ElementCount - 1
                k += j
                If k >= MyBase.ElementCount Then
                    k -= MyBase.ElementCount
                End If
                realValue += MyBase.CosineTable(k) * MyBase.RealCache(i) 
                                  + MyBase.SineTable(k) * MyBase.ImaginaryCache(i)
                imaginaryValue += MyBase.CosineTable(k) * MyBase.ImaginaryCache(i) 
                                  - MyBase.SineTable(k) * MyBase.RealCache(i)
            Next i
            reals(j) = realValue
            imaginaries(j) = imaginaryValue
        Next j

    End Function

    ''' <summary>Calculates the Discrete Fourier Transform.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <remarks>Computes the DFT directly without using any of the special rotation 
    '''   and permutation algorithms that are part of the Fast Fourier Transform (FFT)
    '''   literature.  Swap reals and imaginaries to computer the inverse Fourier transform.
    ''' </remarks>
    Public Overloads Overrides Function Forward(ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' build the DFT tables
        MyBase.BuildDftTables(reals.Length)

        ' Set element count
        MyBase.ElementCount = reals.Length

        ' save initial values to the cache
        MyBase.CopyToCache(reals, imaginaries)

        Dim realValue As Double
        Dim imaginaryValue As Double
        Dim k As Integer
        For j As Integer = 0 To MyBase.ElementCount - 1
            realValue = MyBase.RealCache(0)
            imaginaryValue = MyBase.ImaginaryCache(0)
            k = 0
            For i As Integer = 1 To MyBase.ElementCount - 1
                k += j
                If k >= MyBase.ElementCount Then
                    k -= MyBase.ElementCount
                End If
                realValue += MyBase.CosineTable(k) * MyBase.RealCache(i) 
                                  + MyBase.SineTable(k) * MyBase.ImaginaryCache(i)
                imaginaryValue += MyBase.CosineTable(k) * MyBase.ImaginaryCache(i) 
                                  - MyBase.SineTable(k) * MyBase.RealCache(i)
            Next i
            reals(j) = CSng(realValue)
            imaginaries(j) = CSng(imaginaryValue)
        Next j

    End Function

#End Region

End Class
