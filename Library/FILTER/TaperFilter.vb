''' <summary>Provides shared services for applying taper filter windows in the
'''   frequency domain.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public NotInheritable Class TaperFilter

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>A private constructor to prevent the compiler from generating a 
    '''   default constructor.</summary>
    Public Sub New(ByVal filterType As TaperFilterType)

        MyBase.new()
        Me.FilterType = filterType

    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' Validates the filter frequencies.
    ''' </summary>
    ''' <param name="filterType">Type of the filter.</param>
    ''' <param name="samplingRate">The sampling rate.</param>
    ''' <param name="lowFrequency">The low frequency.</param>
    ''' <param name="highFrequency">The high frequency.</param>
    ''' <param name="transitionBand">The transition band.</param>
    ''' <returns>isr.Numerics.Signals.ValidationOutcome.</returns>
    Public Shared Function ValidateFilterFrequencies(ByVal filterType As isr.Numerics.Signals.TaperFilterType,
                                                     ByVal samplingRate As Double,
                                                     ByVal lowFrequency As Double, ByVal highFrequency As Double,
                                                     ByVal transitionBand As Double) As isr.Numerics.Signals.ValidationOutcome

        Dim answer As String = String.Empty
        Dim validated As Boolean = True

        If samplingRate <= 0 Then
            answer = "Sampling rate must be positive."
            Return New isr.Numerics.Signals.ValidationOutcome(False, answer)
        End If

        Select Case filterType

            Case TaperFilterType.HighPass

                If highFrequency - transitionBand / 2 <= 0 Then
                    validated = False
                    answer = "Filter frequency must exceed half the transition band."
                ElseIf highFrequency + transitionBand / 2 > samplingRate / 2 Then
                    validated = False
                    answer = "Filter frequency plus half transition band must be lower than half the sampling rate."
                End If

            Case TaperFilterType.LowPass

                If lowFrequency - transitionBand / 2 <= 0 Then
                    validated = False
                    answer = "Filter frequency must exceed half the transition band."
                ElseIf lowFrequency + transitionBand / 2 > samplingRate / 2 Then
                    validated = False
                    answer = "Filter frequency plus half transition band must be lower than half the sampling rate."
                End If

            Case TaperFilterType.BandPass

                If lowFrequency - transitionBand / 2 <= 0 Then
                    validated = False
                    answer = "Low pass-band frequency must exceed half the transition band."
                ElseIf highFrequency - transitionBand < lowFrequency Then
                    validated = False
                    answer = "High pass-band frequency must exceed the low frequency plus the transition band."
                ElseIf highFrequency + transitionBand / 2 > samplingRate / 2 Then
                    validated = False
                    answer = "High pass-band frequency plus half transition band must be lower than half the sampling rate."
                End If

            Case TaperFilterType.BandReject

                If lowFrequency - transitionBand / 2 <= 0 Then
                    validated = False
                    answer = "Low stop-band frequency must exceed half the transition band."
                ElseIf highFrequency - transitionBand < lowFrequency Then
                    validated = False
                    answer = "High stop-band frequency must exceed the low frequency plus the transition band."
                ElseIf highFrequency + transitionBand / 2 > samplingRate / 2 Then
                    validated = False
                    answer = "High stop-band frequency plus half transition band must be lower than half the sampling rate."
                End If

        End Select

        Return New isr.Numerics.Signals.ValidationOutcome(validated, answer)

    End Function

    ''' <summary>
    ''' Returns the index in the spectrum matching the given frequency.
    ''' </summary>
    ''' <param name="frequency">The frequency.</param>
    ''' <param name="samplingRate">The sampling rate.</param>
    ''' <param name="timeSeriesLength">Length of the time series.</param>
    ''' <returns>System.Int32.</returns>
    ''' <exception cref="System.ArgumentOutOfRangeException">samplingRate;Sampling rate must be positive</exception>
    Private Shared Function FrequencyIndex(ByVal frequency As Double, ByVal samplingRate As Double, ByVal timeSeriesLength As Integer) As Integer

        If samplingRate <= Single.Epsilon Then
            Throw New ArgumentOutOfRangeException("samplingRate", "Sampling rate must be positive")
        End If

        If timeSeriesLength <= 2 Then
            Throw New ArgumentOutOfRangeException("timeSeriesLength", "Time series length must exceed 2")
        End If

        If frequency <= Single.Epsilon Then
            Throw New ArgumentOutOfRangeException("frequency", "Frequency must be positive")
        End If

        Return CInt(frequency * timeSeriesLength / samplingRate)

    End Function

    ''' <summary>Tapers the spectrum using a High Pass taper.</summary>
    ''' <param name="frequency">The high pass frequency index.</param>
    ''' <param name="transitionBankCount">The range of indexes over which the filter is tapered.</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    ''' <returns>True if filtered.</returns>
    Private Shared Function HighPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                          ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If

        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        Return TaperFilter.HighPassTaper(frequency, 0, transitionBankCount, reals, imaginaries)

    End Function

    ''' <summary>Tapers the spectrum using a High Pass taper.</summary>
    ''' <param name="frequency">The high pass frequency index.</param>
    ''' <param name="transitionBankCount">The range of indexes over which the filter is tapered.</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    ''' <returns>True if filtered.</returns>
    Private Shared Function HighPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                          ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If

        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        Return TaperFilter.HighPassTaper(frequency,  transitionBankCount, 0, reals, imaginaries)

    End Function

    ''' <summary>Tapers the spectrum using a High Pass taper.</summary>
    ''' <param name="frequency">The high pass frequency index.</param>
    ''' <param name="transitionBankCount">The range of indexes over which the filter is tapered.</param>
    ''' <param name="fromIndex">The first frequency index form which to apply 
    '''   the high pass taper.</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    ''' <returns>True if filtered.</returns>
    Private Shared Function HighPassTaper(ByVal frequency As Integer,
                                          ByVal transitionBankCount As Integer, ByVal fromIndex As Integer,
                                          ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If

        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        If fromIndex + transitionBankCount \ 2 > frequency Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Frequency must exceed the minimum frequency plus half the transition band.")
        End If

        Dim elementCount As Integer = reals.Length

        If frequency - transitionBankCount \ 2 + transitionBankCount > elementCount \ 2 Then
            Throw New ArgumentOutOfRangeException("frequency", frequency, "Filter frequency plus half transition band must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        If fromIndex = 0 Then
            reals(fromIndex) = 0
            imaginaries(fromIndex) = 0
            fromIndex += 1
        End If

        ' zero out low frequency amplitudes
        frequencyIndex = fromIndex
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < (frequency - transitionBankCount \ 2)

            reals(frequencyIndex) = 0
            imaginaries(frequencyIndex) = 0
            reals(conjugateFrequencyIndex) = 0
            imaginaries(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band band from the data
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5R * (1.0R + Math.Cos(taperPhase))

            ' Apply the taper factor      
            reals(frequencyIndex) *= taperFactor
            imaginaries(frequencyIndex) *= taperFactor
            reals(conjugateFrequencyIndex) *= taperFactor
            imaginaries(conjugateFrequencyIndex) *= taperFactor

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Return True

    End Function

    ''' <summary>Tapers the spectrum using a High Pass taper.</summary>
    ''' <param name="frequency">The high pass frequency index.</param>
    ''' <param name="transitionBankCount">The range of indexes over which the filter is tapered.</param>
    ''' <param name="fromIndex">The first frequency index form which to apply 
    '''   the high pass taper.</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    ''' <returns>True if filtered.</returns>
    Private Shared Function HighPassTaper(ByVal frequency As Integer,
                                          ByVal transitionBankCount As Integer, ByVal fromIndex As Integer,
                                          ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If

        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        If fromIndex + transitionBankCount \ 2 > frequency Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Frequency must exceed the minimum frequency plus half the transition band.")
        End If

        Dim elementCount As Integer = reals.Length

        If frequency - transitionBankCount \ 2 + transitionBankCount > elementCount \ 2 Then
            Throw New ArgumentOutOfRangeException("frequency", frequency, "Filter frequency plus half transition band must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        If fromIndex = 0 Then
            reals(fromIndex) = 0
            imaginaries(fromIndex) = 0
            fromIndex += 1
        End If

        ' zero out low frequency amplitudes
        frequencyIndex = fromIndex
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < (frequency - transitionBankCount \ 2)

            reals(frequencyIndex) = 0
            imaginaries(frequencyIndex) = 0
            reals(conjugateFrequencyIndex) = 0
            imaginaries(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band band from the data
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5R * (1.0R + Math.Cos(taperPhase))

            ' Apply the taper factor      
            reals(frequencyIndex) = CSng(reals(frequencyIndex) * taperFactor)
            imaginaries(frequencyIndex) = CSng(imaginaries(frequencyIndex) * taperFactor)
            reals(conjugateFrequencyIndex) = CSng(reals(conjugateFrequencyIndex) * taperFactor)
            imaginaries(conjugateFrequencyIndex) = CSng(imaginaries(conjugateFrequencyIndex) * taperFactor)

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Return True

    End Function

    ''' <summary>Applies a Low Pass Filter in the frequency domain by tapering
    '''   the signal spectrum.</summary>
    ''' <param name="frequency">The low pass frequency index.</param>
    ''' <param name="transitionBankCount">The range of indexes over which the filter is tapered</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    ''' <returns>True if filtered.</returns>
    Public Shared Function LowPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                        ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        Return TaperFilter.LowPassTaper(frequency, transitionBankCount, reals.Length \ 2, reals, imaginaries)

    End Function

    ''' <summary>Applies a Low Pass Filter in the frequency domain by tapering
    '''   the signal spectrum.</summary>
    ''' <param name="frequency">The low pass frequency index.</param>
    ''' <param name="transitionBankCount">The range of indexes over which the filter is tapered</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    ''' <returns>True if filtered.</returns>
    Public Shared Function LowPassTaper(ByVal frequency As Integer, ByVal transitionBankCount As Integer,
                                        ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        Return TaperFilter.LowPassTaper(frequency, transitionBankCount, reals.Length \ 2, reals, imaginaries)

    End Function

    ''' <summary>Applies a Low Pass Filter in the frequency domain by tapering
    '''   the signal spectrum.</summary>
    ''' <param name="frequency">The low pass frequency index.</param>
    ''' <param name="toIndex">The last frequency to apply the low pass
    '''   filter.  This allows using this method to apply the low past filter 
    '''   in case of a band reject filter where the spectrum can be zeroed only 
    '''   within a specific range.</param>
    ''' <param name="transitionBankCount">The range of indexes over which the filter is tapered</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    ''' <returns>True if filtered.</returns>
    Public Shared Function LowPassTaper(ByVal frequency As Integer,
                                        ByVal transitionBankCount As Integer, ByVal toIndex As Integer,
                                        ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        If frequency - transitionBankCount \ 2 <= 0 Then
            Throw New ArgumentOutOfRangeException("frequency", frequency, "Filter frequency minus half the transition band must exceed zero.")
        End If

        If frequency - transitionBankCount \ 2 + transitionBankCount > toIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band.")
        End If

        Dim elementCount As Integer = reals.Length

        If toIndex > (elementCount \ 2) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Maximum frequency must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band band from the data
        frequencyIndex = frequency - transitionBankCount \ 2
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5R * (1.0R + Math.Cos(taperPhase))

            ' Apply the taper factor      
            reals(frequencyIndex) *= taperFactor
            imaginaries(frequencyIndex) *= taperFactor
            reals(conjugateFrequencyIndex) *= taperFactor
            imaginaries(conjugateFrequencyIndex) *= taperFactor

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Do While frequencyIndex <= toIndex

            reals(frequencyIndex) = 0
            imaginaries(frequencyIndex) = 0
            reals(conjugateFrequencyIndex) = 0
            imaginaries(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Return True

    End Function

    ''' <summary>Applies a Low Pass Filter in the frequency domain by tapering
    '''   the signal spectrum.</summary>
    ''' <param name="frequency">The low pass frequency index.</param>
    ''' <param name="toIndex">The last frequency to apply the low pass
    '''   filter.  This allows using this method to apply the low past filter 
    '''   in case of a band reject filter where the spectrum can be zeroed only 
    '''   within a specific range.</param>
    ''' <param name="transitionBankCount">The range of indexes over which the filter is tapered</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    ''' <returns>True if filtered.</returns>
    Public Shared Function LowPassTaper(ByVal frequency As Integer,
                                        ByVal transitionBankCount As Integer, ByVal toIndex As Integer,
                                        ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        If frequency - transitionBankCount \ 2 <= 0 Then
            Throw New ArgumentOutOfRangeException("frequency", frequency, "Filter frequency minus half the transition band must exceed zero.")
        End If

        If frequency - transitionBankCount \ 2 + transitionBankCount > toIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Maximum frequency must exceed the filter frequency plus half the transition band.")
        End If

        Dim elementCount As Integer = reals.Length

        If toIndex > (elementCount \ 2) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Maximum frequency must be lower than half the spectrum.")
        End If

        Dim frequencyIndex As Integer
        Dim conjugateFrequencyIndex As Integer
        Dim taperFactor As Double

        ' The Cosine taper scale factor
        Dim deltaPhase As Double
        Dim taperPhase As Double

        ' Set the cosine values  
        taperPhase = 0
        deltaPhase = 0.5 * Math.PI / Convert.ToSingle(transitionBankCount)

        ' taper through the frequency transition band band from the data
        frequencyIndex = frequency - transitionBankCount \ 2
        conjugateFrequencyIndex = elementCount - frequencyIndex
        Do While frequencyIndex < frequency - transitionBankCount \ 2 + transitionBankCount

            taperFactor = 0.5R * (1.0R + Math.Cos(taperPhase))

            ' Apply the taper factor      
            reals(frequencyIndex) = CSng(reals(frequencyIndex) * taperFactor)
            imaginaries(frequencyIndex) = CSng(imaginaries(frequencyIndex) * taperFactor)
            reals(conjugateFrequencyIndex) = CSng(reals(conjugateFrequencyIndex) * taperFactor)
            imaginaries(conjugateFrequencyIndex) = CSng(imaginaries(conjugateFrequencyIndex) * taperFactor)

            '  Set the next point
            taperPhase += deltaPhase
            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Do While frequencyIndex < toIndex

            reals(frequencyIndex) = 0
            imaginaries(frequencyIndex) = 0
            reals(conjugateFrequencyIndex) = 0
            imaginaries(conjugateFrequencyIndex) = 0

            frequencyIndex += 1
            conjugateFrequencyIndex -= 1

        Loop

        Return True

    End Function

#End Region

#Region " METHODS "

    ''' <summary>Validates filter settings.
    ''' </summary>
    Public Function ValidateFilterFrequencies() As Boolean

        Dim outcome As isr.Numerics.Signals.ValidationOutcome = TaperFilter.ValidateFilterFrequencies( 
            Me._filterType, Me.SamplingRate, Me.LowFrequency, Me.HighFrequency, Me.TransitionBand)

        If outcome.Validated Then
            Me._statusMessage = String.Empty
            Return True
        Else
            Me._statusMessage = outcome.FailureReason
            Return False
        End If

    End Function

    ''' <summary>Taper the frequency band for filtering the signal in the 
    '''   frequency domain.</summary>
    ''' <param name="samplingRate">The spectrum sampling rate.</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    Public Function Taper(ByVal samplingRate As Double, ByVal reals() As Double, ByVal imaginaries() As Double) As Boolean

        If reals Is Nothing Then
            Throw New ArgumentNullException("reals")
        End If

        If imaginaries Is Nothing Then
            Throw New ArgumentNullException("imaginaries")
        End If

        Me._samplingRate = samplingRate
        Me._timeSeriesLength = reals.Length

        If Not Me.ValidateFilterFrequencies() Then
            Throw New isr.Numerics.Signals.BaseException(Me.StatusMessage)
        End If

        Select Case Me._filterType

            Case TaperFilterType.BandPass

                ' high pass at the low frequency
                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand), reals, imaginaries)

                ' low pass at the high frequency.
                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.HighFrequency), Me.FrequencyIndex(Me.TransitionBand), reals, imaginaries)

            Case TaperFilterType.BandReject

                Dim midFrequency As Double = (Me.LowFrequency + Me.HighFrequency) / 2

                ' low pass at the low frequency but up to the mid band
                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency),
                                         reals, imaginaries)

                ' high pass at the high frequency but only from the mid band
                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.HighFrequency),
                                          Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency),
                                          reals, imaginaries)

            Case TaperFilterType.HighPass

                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.HighFrequency), Me.FrequencyIndex(Me.TransitionBand),
                                          reals, imaginaries)

            Case TaperFilterType.LowPass

                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand),
                                         reals, imaginaries)

            Case TaperFilterType.None

                Return True

        End Select

    End Function

    ''' <summary>Taper the frequency band for filtering the signal in the 
    '''   frequency domain.</summary>
    ''' <param name="samplingRate">The spectrum sampling rate.</param>
    ''' <param name="reals">Holds the real values of the spectrum.</param>
    ''' <param name="imaginaries">Holds the imaginary values of the spectrum.</param>
    Public Function Taper(ByVal samplingRate As Double, ByVal reals() As Single, ByVal imaginaries() As Single) As Boolean

        If reals Is Nothing Then
            Throw New ArgumentNullException("reals")
        End If

        If imaginaries Is Nothing Then
            Throw New ArgumentNullException("imaginaries")
        End If

        Me._samplingRate = samplingRate
        Me._timeSeriesLength = reals.Length

        Select Case Me._filterType

            Case TaperFilterType.BandPass

                ' high pass at the low frequency
                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.LowFrequency),
                                          Me.FrequencyIndex(Me.TransitionBand), reals, imaginaries)

                ' low pass at the high frequency.
                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.HighFrequency),
                                         Me.FrequencyIndex(Me.TransitionBand), reals, imaginaries)

            Case TaperFilterType.BandReject

                Dim midFrequency As Double = (Me.LowFrequency + Me.HighFrequency) / 2

                ' low pass at the low frequency but up to the mid band
                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.LowFrequency),
                                         Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency),
                                         reals, imaginaries)

                ' high pass at the high frequency but only from the mid band
                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.HighFrequency),
                                          Me.FrequencyIndex(Me.TransitionBand), Me.FrequencyIndex(midFrequency),
                                          reals, imaginaries)

            Case TaperFilterType.HighPass

                TaperFilter.HighPassTaper(Me.FrequencyIndex(Me.HighFrequency), Me.FrequencyIndex(Me.TransitionBand),
                                          reals, imaginaries)

            Case TaperFilterType.LowPass

                TaperFilter.LowPassTaper(Me.FrequencyIndex(Me.LowFrequency), Me.FrequencyIndex(Me.TransitionBand),
                                         reals, imaginaries)

            Case TaperFilterType.None

                Return True

        End Select

    End Function

    ''' <summary>
    ''' Returns the frequency index for the specified frequency.
    ''' </summary>
    ''' <param name="frequency">The frequency.</param>
    ''' <returns>System.Int32.</returns>
    Private Function frequencyIndex(ByVal frequency As Double) As Integer
        Return TaperFilter.FrequencyIndex(frequency, Me._samplingRate, Me._timeSeriesLength)
    End Function

#End Region

#Region " PROPERTIES "

    Private _filterType As TaperFilterType
    ''' <summary>Gets or sets the <see cref="isr.Numerics.Signals.TaperFilterType">type</see>
    '''   of filter.</summary>
    Public Property FilterType() As isr.Numerics.Signals.TaperFilterType
        Get
            Return Me._filterType
        End Get
        Set(ByVal Value As TaperFilterType)
            Me._filterType = Value
        End Set
    End Property

    Private _lowFrequency As Double
    ''' <summary>Gets or sets the low frequency of the filter.</summary>
    Public Property LowFrequency() As Double
        Get
            Return Me._lowFrequency
        End Get
        Set(ByVal Value As Double)
            Me._lowFrequency = Value
        End Set
    End Property

    Private _highFrequency As Double
    ''' <summary>Gets or sets the high frequency of the filter.</summary>
    Public Property HighFrequency() As Double
        Get
            Return Me._highFrequency
        End Get
        Set(ByVal Value As Double)
            Me._highFrequency = Value
        End Set
    End Property

    Private _transitionBand As Double
    ''' <summary>Gets or sets the filter transition band.
    ''' </summary>
    Public Property TransitionBand() As Double
        Get
            Return Me._transitionBand
        End Get
        Set(ByVal Value As Double)
            If Value < 2 Then
                Throw New ArgumentOutOfRangeException("Value", Value, "Transition range must exceed 2.")
            Else
                Me._transitionBand = Value
            End If
        End Set
    End Property

    Private _samplingRate As Double
    ''' <summary>Gets or sets the sampling rate for calculating frequency indexes.
    ''' </summary>
    Private ReadOnly Property SamplingRate() As Double
        Get
            Return Me._samplingRate
        End Get
    End Property

    Private _statusMessage As String
    Public ReadOnly Property StatusMessage() As String
        Get
            Return Me._statusMessage
        End Get
    End Property

    Private _timeSeriesLength As Integer
    ''' <summary>Gets or sets the number of elements in the time series sample
    '''   that is processed to compute the Spectrum.
    ''' </summary>
    Public ReadOnly Property TimeSeriesLength() As Integer
        Get
            Return Me._timeSeriesLength
        End Get
    End Property

#End Region

End Class
