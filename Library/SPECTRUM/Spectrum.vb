''' <summary>Calculates the spectrum using the Fourier Transform.  Includes 
'''   pre-processing fro removing mean and applying a taper data window. Includes
'''   post-processing methods for high-pass and low-pass filtering in the frequency
'''   domain.</summary>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class Spectrum

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs This class.</summary>
    ''' <param name="FourierTransform">Holds reference to the Fourier Transform class to use for 
    '''   calculating the spectrum.</param>
    Public Sub New(ByVal fourierTransform As isr.Numerics.Signals.FourierTransform)

        ' instantiate the base class
        MyBase.New()

        ' set reference to the Fourier Transform class
        Me._fourierTransform = fourierTransform

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make This method Overridable (virtual) because a derived 
    '''   class should not be able to override This method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _disposed As Boolean
    ''' <summary>Gets or sets (private) the dispose status sentinel.</summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._disposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; False if This method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                    ' remove the reference to the external objects.
                    Me._fourierTransform = Nothing
                    Me._taperWindow = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " COMMON "

    Private _fourierTransform As isr.Numerics.Signals.FourierTransform
    ''' <summary>Gets reference to the Fourier Transform used for calculating the spectrum.
    ''' </summary>
    Protected ReadOnly Property FourierTransform() As isr.Numerics.Signals.FourierTransform
        Get
            Return Me._fourierTransform
        End Get
    End Property

    ''' <summary>Gets the size of the half spectrum including the DC point.</summary>
    Public ReadOnly Property HalfSpectrumLength() As Integer
        Get
            Return Me._fourierTransform.HalfSpectrumLength
        End Get
    End Property

    Private _isScaleFft As Boolean
    ''' <summary>Gets or Sets the condition as True scale the Spectrum.  It is useful to set
    '''   This to false whenever averaging transforms so that the scaling
    '''   is done at the end.</summary>
    Public Property IsScaleFft() As Boolean
        Get
            Return Me._isScaleFft
        End Get
        Set(ByVal Value As Boolean)
            Me._isScaleFft = Value
        End Set
    End Property

    Private _isRemoveMean As Boolean
    ''' <summary>Gets or Sets the condition as True remove the mean before computing the Spectrum.
    ''' </summary>
    Public Property IsRemoveMean() As Boolean
        Get
            Return Me._isRemoveMean
        End Get
        Set(ByVal Value As Boolean)
            ' Set the remove mean option
            Me._isRemoveMean = Value
        End Set
    End Property

    ''' <summary>Returns the power (average sum of squares of amplitudes) of the 
    '''   taper window.</summary>
    ''' <remarks>Represents the average amount by which the power of the signal was 
    '''   attenuated due to the tapering effect of the taper data window.
    ''' </remarks>
    Public ReadOnly Property TaperWindowPower() As Double
        Get
            If Me._taperWindow Is Nothing Then
                Return 1.0R
            Else
                Return Me._taperWindow.Power()
            End If
        End Get
    End Property

    ''' <summary>Gets or sets the number of elements in the time series sample
    '''   that is processed to compute the Spectrum.  This must be
    '''   set before setting the filter frequencies.  Setting the time
    '''   series length also sets the size of the Fourier transform base class.
    ''' </summary>
    Public ReadOnly Property TimeSeriesLength() As Integer
        Get
            Return Me._fourierTransform.TimeSeriesLength
        End Get
    End Property

    Private _samplingRate As Double
    ''' <summary>Gets or sets the low pass frequency.  Must be set before setting the 
    '''   filter frequencies.
    ''' </summary>
    Public Property SamplingRate() As Double
        Get
            Return Me._samplingRate
        End Get
        Set(ByVal Value As Double)
            Me._samplingRate = Value
        End Set
    End Property

    Private _taperFilter As isr.Numerics.Signals.TaperFilter
    ''' <summary>Gets or sets reference to the <see cref="isr.Numerics.Signals.TaperFilter">taper Filter</see>.</summary>
    Public Property TaperFilter() As isr.Numerics.Signals.TaperFilter
        Get
            Return Me._taperFilter
        End Get
        Set(ByVal Value As isr.Numerics.Signals.TaperFilter)
            Me._taperFilter = Value
        End Set
    End Property

    Private _taperWindow As isr.Numerics.Signals.TaperWindow
    ''' <summary>Gets or sets reference to the <see cref="isr.Numerics.Signals.TaperWindow">taper window</see>
    '''   used for reducing side lobes.</summary>
    Public Property TaperWindow() As isr.Numerics.Signals.TaperWindow
        Get
            Return Me._taperWindow
        End Get
        Set(ByVal Value As isr.Numerics.Signals.TaperWindow)
            Me._taperWindow = Value
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>Applies the filter to the spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    Public Sub ApplyFilter(ByVal reals() As Double, ByVal imaginaries() As Double)

        If Me._taperFilter Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Taper filter not defined")
        End If

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        Me._taperFilter.Taper(Me.SamplingRate, reals, imaginaries)

    End Sub

    ''' <summary>Applies the filter to the spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    Public Sub ApplyFilter(ByVal reals() As Single, ByVal imaginaries() As Single)

        If Me._taperFilter Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Taper filter not defined")
        End If

        If reals Is Nothing Then
            Throw New ArgumentNullException("reals")
        End If

        If imaginaries Is Nothing Then
            Throw New ArgumentNullException("imaginaries")
        End If

        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        Me._taperFilter.Taper(Me.SamplingRate, reals, imaginaries)

    End Sub

    ''' <summary>Calculates the spectrum using the Fourier Transform with pre
    '''   and post processing.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <example>
    ''' </example>
    Public Sub Calculate(ByVal reals() As Single, ByVal imaginaries() As Single)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If

        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If

        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' Check if the remove mean option is set
        If Me.IsRemoveMean Then

            ' remove the Mean
            Helper.RemoveMean(reals)

        End If

        ' Apply the taper window
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Apply(reals)
        End If

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(reals, imaginaries)

        ' Check if the scale option is set
        If Me.IsScaleFft Then

            ' Set the scale factor
            Dim scaleFactor As Single = CSng(1.0R / (System.Math.Sqrt(Me.TaperWindowPower) * reals.Length))

            ' Scale the Spectrum.
            Helper.Scale(reals, scaleFactor)
            Helper.Scale(imaginaries, scaleFactor)

        End If

        If Me._taperFilter IsNot Nothing AndAlso Me._taperFilter.FilterType <> TaperFilterType.None Then

            ' apply the filter as necessary
            Me.ApplyFilter(reals, imaginaries)

        End If

    End Sub

    ''' <summary>Calculates the spectrum using the Fourier Transform with pre
    '''   and post processing.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub Calculate(ByVal reals() As Double, ByVal imaginaries() As Double)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' Check if the remove mean option is set
        If Me.IsRemoveMean Then

            ' remove the Mean
            Helper.RemoveMean(reals)

        End If

        ' Apply the taper window
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Apply(reals)
        End If

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(reals, imaginaries)

        ' Check if the scale option is set
        If Me.IsScaleFft Then

            ' Set the scale factor
            Dim scaleFactor As Double = 1.0R / (System.Math.Sqrt(Me.TaperWindowPower) * reals.Length)

            ' Scale the spectrum.
            Helper.Scale(reals, scaleFactor)
            Helper.Scale(imaginaries, scaleFactor)

        End If

        If Me._taperFilter IsNot Nothing AndAlso Me._taperFilter.FilterType <> TaperFilterType.None Then

            ' apply the filter as necessary
            Me.ApplyFilter(reals, imaginaries)

        End If

    End Sub

    ''' <summary>Filters the real valued signal using low pass and high pass
    '''   taper frequency windows.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub Filter(ByVal reals() As Double)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If

        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' Initialize imaginary array.
        Dim imaginaries() As Double = {}
        ReDim imaginaries(reals.Length - 1)

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(reals, imaginaries)

        ' Set the scale factor
        Dim scaleFactor As Double = 1.0R / reals.Length

        ' Scale the Spectrum.
        Helper.Scale(reals, scaleFactor)
        Helper.Scale(imaginaries, scaleFactor)

        If Me._taperFilter IsNot Nothing AndAlso Me._taperFilter.FilterType <> TaperFilterType.None Then

            ' apply the filter as necessary
            Me.ApplyFilter(reals, imaginaries)

        End If

        ' inverse transform
        Me.FourierTransform.Inverse(reals, imaginaries)

    End Sub

    ''' <summary>Filters the real valued signal using low pass and high pass
    '''   taper frequency windows.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub Filter(ByVal reals() As Single)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If

        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' Initialize imaginary array.
        Dim imaginaries() As Single = {}
        ReDim imaginaries(reals.Length - 1)

        ' Calculate the forward Fourier transform  
        Me.FourierTransform.Forward(reals, imaginaries)

        ' Set the scale factor
        Dim scaleFactor As Single = CSng(1.0R / reals.Length)

        ' Scale the Spectrum.
        Helper.Scale(reals, scaleFactor)
        Helper.Scale(imaginaries, scaleFactor)

        If Me._taperFilter IsNot Nothing AndAlso Me._taperFilter.FilterType <> TaperFilterType.None Then

            ' apply the filter as necessary
            Me.ApplyFilter(reals, imaginaries)

        End If

        ' inverse transform
        Me.FourierTransform.Inverse(reals, imaginaries)

    End Sub

    ''' <summary>Initializes the spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <example>
    ''' </example>
    Public Sub Initialize(ByVal reals() As Double, ByVal imaginaries() As Double)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' Create the taper window
        If Me.TaperWindow IsNot Nothing Then
            Me.TaperWindow.Create(reals.Length)
        End If

        ' initialize the transform.
        Me.FourierTransform.Initialize(reals, imaginaries)

    End Sub

    ''' <summary>Initializes the spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <example>
    ''' </example>
    Public Sub Initialize(ByVal reals() As Single, ByVal imaginaries() As Single)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' Create the taper window
        If Not Me.TaperWindow Is Nothing Then
            Me.TaperWindow.Create(reals.Length)
        End If

        ' initialize the transform.
        Me.FourierTransform.Initialize(reals, imaginaries)

    End Sub

#End Region

End Class

