''' <summary>The base class for the power spectrum.</summary>
''' <license>
''' (c) 1998 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class PowerSpectrum

    Inherits Spectrum

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs This class.</summary>
    ''' <param name="FourierTransform">Holds reference to the Fourier Transform class to use for 
    '''   calculating the spectrum.</param>
    Public Sub New(ByVal fourierTransform As isr.Numerics.Signals.FourierTransform)

        ' instantiate the base class
        MyBase.New(fourierTransform)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if This method releases both managed and unmanaged 
    '''   resources; False if This method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>Appends the spectrum to the existing spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub Append(ByVal reals() As Double, ByVal imaginaries() As Double)

        If Me._scaled Then
            Throw New isr.Numerics.Signals.BaseException("Because densities were already scaled they must be restored before adding move averages to the power spectrum.")
        End If
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        Dim real As Double
        Dim imaginary As Double
        If Me._densities Is Nothing Then
            ReDim _densities(reals.Length - 1)
        End If
        For i As Integer = 0 To Me._densities.Length - 1
            real = reals(i)
            imaginary = imaginaries(i)
            Me._densities(i) += real * real + imaginary * imaginary
        Next

        ' increment the average count
        Me._averageCount += 1

    End Sub

    ''' <summary>Appends the spectrum to the existing spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub Append(ByVal reals() As Single, ByVal imaginaries() As Single)

        If Me._scaled Then
            Throw New isr.Numerics.Signals.BaseException("Because densities were already scaled they must be restored before adding move averages to the power spectrum.")
        End If
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        Dim real As Double
        Dim imaginary As Double
        If Me._densities Is Nothing Then
            ReDim _densities(reals.Length - 1)
        End If
        For i As Integer = 0 To Me._densities.Length - 1
            real = reals(i)
            imaginary = imaginaries(i)
            Me._densities(i) += real * real + imaginary * imaginary
        Next

        ' increment the average count
        Me._averageCount += 1

    End Sub

    Private _averageCount As Integer
    Public ReadOnly Property AverageCount() As Integer
        Get
            Return Me._averageCount
        End Get
    End Property

    ''' <summary>Clears and allocates the power spectrum densities.
    ''' </summary>
    ''' <param name="length">Specifies the length of the spectrum thus allowing for a Int16er
    '''   spectrum than is calculated. Specify 0 to let the program set the maximum
    '''   length.</param>
    Public Sub Clear(ByVal length As Integer)

        Me._scaled = False
        Me._averageCount = 0
        If length > 0 Then
            ReDim _densities(length - 1)
        Else
            Me._densities = Nothing
        End If

    End Sub

    ''' <summary>Copies the density and the number of averages.</summary>
    ''' <param name="averageCount">Specifies the number of averages used to calculate
    '''   the densities.</param>
    ''' <param name="spectrumCount">Specifies the number of points to copy.</param>
    Public Sub CopyToDensities(ByVal values() As Double, ByVal averageCount As Integer, ByVal spectrumCount As Integer)

        If values Is Nothing Then
            Me._densities = Nothing
        Else
            Me._scaled = True
            Me._averageCount = averageCount
            ReDim _densities(spectrumCount - 1)
            Array.Copy(values, Me._densities, spectrumCount)
        End If

    End Sub

    ''' <summary>Copies the density and the number of averages.</summary>
    ''' <param name="averageCount">Specifies the number of averages used to calculate
    '''   the densities.</param>
    ''' <param name="spectrumCount">Specifies the number of points to copy.</param>
    Public Sub CopyToDensities(ByVal values() As Single, ByVal averageCount As Integer, ByVal spectrumCount As Integer)

        If values Is Nothing Then
            Me._densities = Nothing
        Else
            Me._scaled = True
            Me._averageCount = averageCount
            ReDim _densities(spectrumCount - 1)
            Array.Copy(values, Me._densities, spectrumCount)
        End If

    End Sub

    ''' <summary>Returns the indexed density.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
    Public Function Density(ByVal halfSpectrum As Boolean, ByVal index As Integer) As Double

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        If index < Me._densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("index", index, "Index must exceed lower bound")
        End If
        If index > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("index", index, "Index must not exceed spectrum length")
        End If

        If Me._scaled Then

            If halfSpectrum Then

                If index = 0 Then
                    Return Me._densities(index)
                Else
                    Dim tempValue As Double = Me._densities(index)
                    Return tempValue + tempValue
                End If

            Else

                Return Me._densities(index)

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)
            If index = 0 And halfSpectrum Then
                Return 0.5R * factor * Me._densities(index)
            Else
                Return factor * Me._densities(index)
            End If

        End If

    End Function

    Private _densities() As Double
    ''' <summary>Returns the unscaled (if not scaled) spectrum densities.
    ''' </summary>
    ''' <remarks>Unlike SRE5, the spectrum densities are scaled for full spectrum rather
    '''   than as a half spectrum.  This simplifies greatly the retrieval of 
    '''   spectrum magnitude or power for display.
    ''' </remarks>
    Public Overloads Function Densities() As Double()
        Return Me._densities
    End Function

    ''' <summary>Returns the scaled spectrum densities.
    ''' </summary>
    Public Overloads Function DensitiesDouble() As Double()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        ' allocate the outcome array
        Dim output(Me._densities.Length - 1) As Double

        If Me._scaled Then

            ReDim output(Me._densities.Length - 1)
            For i As Integer = 0 To Me._densities.Length - 1
                output(i) = Me._densities(i)
            Next

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To Me._densities.Length - 1
                output(i) = factor * Me._densities(i)
            Next

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled spectrum densities.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
    Public Overloads Function DensitiesDouble(ByVal halfSpectrum As Boolean) As Double()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        Dim output() As Double

        If Me._scaled Then

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Me._densities(0)
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = Me._densities(i)
                    output(i) = tempValue + tempValue
                Next

            Else

                ReDim output(Me._densities.Length - 1)
                For i As Integer = 0 To Me._densities.Length - 1
                    output(i) = Me._densities(i)
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = 0.5R * factor * Me._densities(0)
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = factor * Me._densities(i)
                    output(i) = tempValue + tempValue
                Next

            Else

                ReDim output(Me._densities.Length - 1)
                For i As Integer = 0 To Me._densities.Length - 1
                    output(i) = factor * Me._densities(i)
                Next

            End If

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled spectrum densities.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled
    '''   for displaying the half spectrum.</param>
    ''' <param name="fromIndex"></param>
    ''' <param name="toIndex"></param>
    Public Overloads Function DensitiesDouble(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        If fromIndex < Me._densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' allocate outcome space
        Dim output(toIndex - fromIndex) As Double

        If Me._scaled Then

            If halfSpectrum Then

                Dim j As Integer = 0
                If fromIndex = 0 Then
                    output(j) = Me._densities(fromIndex)
                    fromIndex += 1
                    j += 1
                End If
                For i As Integer = fromIndex To toIndex
                    Dim tempValue As Double = Me._densities(i)
                    output(j) = tempValue + tempValue
                    j += 1
                Next

            Else

                Dim j As Integer = 0
                ReDim output(toIndex - fromIndex)
                For i As Integer = fromIndex To toIndex
                    output(j) = Me._densities(i)
                    j += 1
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

            Dim j As Integer = 0
            If fromIndex = 0 And halfSpectrum Then
                output(j) = 0.5R * factor * Me._densities(fromIndex)
                fromIndex += 1
                j += 1
            End If

            ' Scale the rest of the spectrum
            For i As Integer = fromIndex To toIndex
                output(j) = factor * Me._densities(i)
                j += 1
            Next i

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled spectrum densities.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
    Public Overloads Function DensitiesSingle(ByVal halfSpectrum As Boolean) As Single()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        Dim output() As Single

        If Me._scaled Then

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = CSng(Me._densities(0))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = Me._densities(i)
                    output(i) = CSng(tempValue + tempValue)
                Next

            Else

                ReDim output(Me._densities.Length - 1)
                For i As Integer = 0 To Me._densities.Length - 1
                    output(i) = CSng(Me._densities(i))
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = CSng(0.5R * factor * Me._densities(0))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = factor * Me._densities(i)
                    output(i) = CSng(tempValue + tempValue)
                Next

            Else

                ReDim output(Me._densities.Length - 1)
                For i As Integer = 0 To Me._densities.Length - 1
                    output(i) = CSng(factor * Me._densities(i))
                Next

            End If

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled spectrum densities.
    ''' </summary>
    Public Overloads Function DensitiesSingle() As Single()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        ' allocate the outcome array
        Dim output(Me._densities.Length - 1) As Single

        If Me._scaled Then

            ReDim output(Me._densities.Length - 1)
            For i As Integer = 0 To Me._densities.Length - 1
                output(i) = CSng(Me._densities(i))
            Next

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To Me._densities.Length - 1
                output(i) = CSng(factor * Me._densities(i))
            Next

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled spectrum densities.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled
    '''   for displaying the half spectrum.</param>
    ''' <param name="fromIndex"></param>
    ''' <param name="toIndex"></param>
    Public Overloads Function DensitiesSingle(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Single()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        If fromIndex < Me._densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' allocate outcome space
        Dim output(toIndex - fromIndex) As Single

        If Me._scaled Then

            If halfSpectrum Then

                Dim j As Integer = 0
                If fromIndex = 0 Then
                    output(j) = CSng(Me._densities(fromIndex))
                    fromIndex += 1
                    j += 1
                End If
                For i As Integer = fromIndex To toIndex
                    Dim tempValue As Double = Me._densities(i)
                    output(j) = CSng(tempValue + tempValue)
                    j += 1
                Next

            Else

                Dim j As Integer = 0
                ReDim output(toIndex - fromIndex)
                For i As Integer = fromIndex To toIndex
                    output(j) = CSng(Me._densities(i))
                    j += 1
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

            Dim j As Integer = 0
            If fromIndex = 0 And halfSpectrum Then
                output(j) = CSng(0.5R * factor * Me._densities(fromIndex))
                fromIndex += 1
                j += 1
            End If

            ' Scale the rest of the spectrum
            For i As Integer = fromIndex To toIndex
                output(j) = CSng(factor * Me._densities(i))
                j += 1
            Next i

        End If

        Return output

    End Function

    ''' <summary>Returns the length of the densities array.</summary>
    Public ReadOnly Property DensitiesLength() As Integer
        Get
            If Me._densities Is Nothing Then
                Return 0
            Else
                Return Me._densities.Length
            End If
        End Get
    End Property

    ''' <summary>Returns the indexed magnitude.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
    Public Function Magnitude(ByVal halfSpectrum As Boolean, ByVal index As Integer) As Double

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        If index < Me._densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("index", index, "Index must exceed lower bound")
        End If
        If index > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("index", index, "Index must not exceed spectrum length")
        End If

        Return Math.Sqrt(Me.Density(halfSpectrum, index))

    End Function

    ''' <summary>Returns the scaled magnitude spectrum.
    ''' </summary>
    Public Function MagnitudesSingle() As Single()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        ' allocate the outcome array
        Dim output(Me._densities.Length - 1) As Single

        If Me._scaled Then

            ReDim output(Me._densities.Length - 1)
            For i As Integer = 0 To Me._densities.Length - 1
                output(i) = CSng(Math.Sqrt(Me._densities(i)))
            Next

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To Me._densities.Length - 1
                output(i) = CSng(Math.Sqrt(factor * Me._densities(i)))
            Next

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled magnitude spectrum.
    ''' </summary>
    Public Function MagnitudesDouble() As Double()
        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        ' allocate the outcome array
        Dim output(Me._densities.Length - 1) As Double

        If Me._scaled Then

            ReDim output(Me._densities.Length - 1)
            For i As Integer = 0 To Me._densities.Length - 1
                output(i) = Math.Sqrt(Me._densities(i))
            Next

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To Me._densities.Length - 1
                output(i) = Math.Sqrt(factor * Me._densities(i))
            Next

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled magnitude spectrum.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
    Public Function MagnitudesDouble(ByVal halfSpectrum As Boolean) As Double()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        Dim output() As Double

        If Me._scaled Then

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Math.Sqrt(Me._densities(0))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = Me._densities(i)
                    output(i) = Math.Sqrt(tempValue + tempValue)
                Next

            Else

                ReDim output(Me._densities.Length - 1)
                For i As Integer = 0 To Me._densities.Length - 1
                    output(i) = Math.Sqrt(Me._densities(i))
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = Math.Sqrt(0.5R * factor * Me._densities(0))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = factor * Me._densities(i)
                    output(i) = Math.Sqrt(tempValue + tempValue)
                Next

            Else

                ReDim output(Me._densities.Length - 1)
                For i As Integer = 0 To Me._densities.Length - 1
                    output(i) = Math.Sqrt(factor * Me._densities(i))
                Next

            End If

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled magnitude spectrum.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled
    '''   for displaying the half spectrum.</param>
    ''' <param name="fromIndex"></param>
    ''' <param name="toIndex"></param>
    Public Function MagnitudesDouble(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        If fromIndex < Me._densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' allocate outcome space
        Dim output(toIndex - fromIndex) As Double

        If Me._scaled Then

            If halfSpectrum Then

                Dim j As Integer = 0
                If fromIndex = 0 Then
                    output(j) = Math.Sqrt(Me._densities(fromIndex))
                    fromIndex += 1
                    j += 1
                End If
                For i As Integer = fromIndex To toIndex
                    Dim tempValue As Double = Me._densities(i)
                    output(j) = Math.Sqrt(tempValue + tempValue)
                    j += 1
                Next

            Else

                Dim j As Integer = 0
                ReDim output(toIndex - fromIndex)
                For i As Integer = fromIndex To toIndex
                    output(j) = Math.Sqrt(Me._densities(i))
                    j += 1
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

            Dim j As Integer = 0
            If fromIndex = 0 And halfSpectrum Then
                output(j) = Math.Sqrt(0.5R * factor * Me._densities(fromIndex))
                fromIndex += 1
                j += 1
            End If

            ' Scale the rest of the spectrum
            For i As Integer = fromIndex To toIndex
                output(j) = Math.Sqrt(factor * Me._densities(i))
                j += 1
            Next i

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled magnitude spectrum.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled</param>
    Public Function MagnitudesSingle(ByVal halfSpectrum As Boolean) As Single()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        Dim output() As Single

        If Me._scaled Then

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = CSng(Math.Sqrt(Me._densities(0)))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = Me._densities(i)
                    output(i) = CSng(Math.Sqrt(tempValue + tempValue))
                Next

            Else

                ReDim output(Me._densities.Length - 1)
                For i As Integer = 0 To Me._densities.Length - 1
                    output(i) = CSng(Math.Sqrt(Me._densities(i)))
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(False)

            If halfSpectrum Then

                ReDim output(MyBase.HalfSpectrumLength - 1)
                output(0) = CSng(Math.Sqrt(0.5R * factor * Me._densities(0)))
                For i As Integer = 1 To MyBase.HalfSpectrumLength - 1
                    Dim tempValue As Double = factor * Me._densities(i)
                    output(i) = CSng(Math.Sqrt(tempValue + tempValue))
                Next

            Else

                ReDim output(Me._densities.Length - 1)
                For i As Integer = 0 To Me._densities.Length - 1
                    output(i) = CSng(Math.Sqrt(factor * Me._densities(i)))
                Next

            End If

        End If

        Return output

    End Function

    ''' <summary>Returns the scaled magnitude spectrum.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled
    '''   for displaying the half spectrum.</param>
    ''' <param name="fromIndex"></param>
    ''' <param name="toIndex"></param>
    Public Function MagnitudesSingle(ByVal halfSpectrum As Boolean, ByVal fromIndex As Integer, ByVal toIndex As Integer) As Single()

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        If fromIndex < Me._densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' allocate outcome space
        Dim output(toIndex - fromIndex) As Single

        If Me._scaled Then

            If halfSpectrum Then

                Dim j As Integer = 0
                If fromIndex = 0 Then
                    output(j) = CSng(Math.Sqrt(Me._densities(fromIndex)))
                    fromIndex += 1
                    j += 1
                End If
                For i As Integer = fromIndex To toIndex
                    Dim tempValue As Double = Me._densities(i)
                    output(j) = CSng(Math.Sqrt(tempValue + tempValue))
                    j += 1
                Next

            Else

                Dim j As Integer = 0
                ReDim output(toIndex - fromIndex)
                For i As Integer = fromIndex To toIndex
                    output(j) = CSng(Math.Sqrt(Me._densities(i)))
                    j += 1
                Next

            End If

        Else

            Dim factor As Double = Me.SpectrumScaleFactor(halfSpectrum)

            Dim j As Integer = 0
            If fromIndex = 0 And halfSpectrum Then
                output(j) = CSng(Math.Sqrt(0.5R * factor * Me._densities(fromIndex)))
                fromIndex += 1
                j += 1
            End If

            ' Scale the rest of the spectrum
            For i As Integer = fromIndex To toIndex
                output(j) = CSng(Math.Sqrt(factor * Me._densities(i)))
                j += 1
            Next i

        End If

        Return output

    End Function

    ''' <summary>Returns the index of the spectrum peak.</summary>
    ''' <param name="fromIndex"></param>
    ''' <param name="toIndex"></param>
    Public Function PeakIndex(ByVal fromIndex As Integer, ByVal toIndex As Integer) As Integer

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        If fromIndex < Me._densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' Set Max to first value
        Dim maxDensityIndex As Integer = fromIndex
        Dim maxDensity As Single = CSng(Me._densities(maxDensityIndex))
        Dim maxCandidate As Single
        For i As Integer = fromIndex To Math.Min(toIndex, Me.HalfSpectrumLength - 1)

            ' Get candidate
            maxCandidate = CSng(Me._densities(i))

            ' Select a new maximum if lower than element.
            If maxCandidate > maxDensity Then
                maxDensity = maxCandidate
                maxDensityIndex = i
            End If

        Next i

        ' return the index
        Return maxDensityIndex

    End Function

    ''' <summary>Reads spectrum densities from the .</summary>
    ''' <param name="reader">Specifies reference to an open binary reader.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub ReadDensities(ByVal reader As IO.BinaryReader)

        If reader Is Nothing Then
            Throw New ArgumentNullException("reader")
        End If

        ' assume the densities were scaled.
        Me._scaled = True

        ' read the densities parameters
        Me._averageCount = reader.ReadInt32
        Dim elementCount As Integer = reader.ReadInt32

        ' allocate data array
        ReDim _densities(elementCount - 1)

        ' Read the densities from the file
        For i As Integer = 0 To elementCount - 1
            Me._densities(i) = reader.ReadDouble
        Next i

    End Sub

    ''' <summary>Restore the densities to their pre-scaled values so that additional averages
    '''   can be added.</summary>
    Public Sub Restore()

        Dim factor As Double = Me.SpectrumScaleFactor(False)
        If factor <= 0 Then
            Throw New isr.Numerics.Signals.BaseException("Restoration spectrum scale factor must be positive.")
        Else
            factor = 1.0R / factor
        End If

        ' un-scale the rest of the spectrum
        For i As Integer = 0 To Me._densities.GetUpperBound(0)

            Me._densities(i) *= factor

        Next i

        ' un-tag power spectrum as not scaled allowing adding spectrum averages.
        Me._scaled = False

    End Sub

    ''' <summary>Scales the spectrum densities based on the number of averages.</summary>
    Public Sub Scale()

        Dim factor As Double = Me.SpectrumScaleFactor(False)

        ' Scale the rest of the spectrum
        For i As Integer = 0 To Me._densities.GetUpperBound(0)

            Me._densities(i) *= factor

        Next i

        ' tag power spectrum as scaled.  This prevents adding new averages until
        ' the spectrum is cleared or restored.
        Me._scaled = True

    End Sub

    ''' <summary>Returns the spectrum scale factor including the effect of the 
    '''   taper window power.
    ''' </summary>
    ''' <param name="halfSpectrum">Specifies is the scaling is doubled
    '''   for displaying the half spectrum.</param>
    Public Function SpectrumScaleFactor(ByVal halfSpectrum As Boolean) As Double
        Dim factor As Double = 1 / (Convert.ToDouble(Me.AverageCount) * MyBase.TimeSeriesLength * MyBase.TimeSeriesLength)
        If halfSpectrum Then
            ' The factor of two ensure double the power for half spectrum.
            factor *= 2
        End If
        ' add taper window power.
        If Not MyBase.TaperWindow Is Nothing Then
            factor /= MyBase.TaperWindow.Power
        End If
        Return factor
    End Function

    ''' <summary>Returns the total power for the specified index range but using both
    '''   side of the spectrum and assuming the spectrum is for real valued time series, that
    '''   is, that the spectrum is symmetric.
    ''' </summary>
    ''' <param name="fromIndex"></param>
    ''' <param name="toIndex"></param>
    Public Function TotalPower(ByVal fromIndex As Integer, ByVal toIndex As Integer) As Double

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        If fromIndex < Me._densities.GetLowerBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must exceed lower bound")
        End If
        If fromIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("fromIndex", fromIndex, "Starting index must not exceed spectrum length")
        End If
        If toIndex < fromIndex Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must exceed starting index")
        End If
        If toIndex > Densities.GetUpperBound(0) Then
            Throw New ArgumentOutOfRangeException("toIndex", toIndex, "Ending index must not exceed spectrum length")
        End If

        ' the DC spectrum is scaled by half because the Spectrum Scale Factor
        ' is derived for the half spectrum (twice as large).
        Dim total As Double = 0
        If fromIndex = 0 Then
            total += 0.5 * Me._densities(0)
            fromIndex += 1
        End If
        ' go up to half spectrum
        For i As Integer = fromIndex To Math.Min(toIndex, Me.HalfSpectrumLength - 1)
            ' add the power
            total += Me._densities(i)
        Next

        If Not Me._scaled Then
            ' apply double the spectrum power factor to correct for the two-sided spectrum.
            total *= Me.SpectrumScaleFactor(True)
        Else
            ' apply double the spectrum power factor to correct for the two-sided spectrum.
            total *= 2
        End If

        Return total

    End Function

    ''' <summary>Returns the total power for the specified index range.
    ''' </summary>
    ''' <param name="fromIndex"></param>
    Public Function TotalPower(ByVal fromIndex As Integer) As Double
        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        Return TotalPower(fromIndex, Me._densities.GetUpperBound(0))
    End Function

    ''' <summary>Returns the total power.</summary>
    Public Function TotalPower() As Double
        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If
        Return TotalPower(Me._densities.GetLowerBound(0), Me._densities.GetUpperBound(0))
    End Function

    ''' <summary>Calculates the spectrum for real data and appends to the existing spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub TransformAppend(ByVal reals() As Double)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' Initialize imaginary array.
        Dim imaginaries() As Double = {}
        ReDim imaginaries(reals.Length - 1)

        ' calculate the spectrum
        MyBase.Calculate(reals, imaginaries)

        ' add the spectrum
        Me.Append(reals, imaginaries)

    End Sub

    ''' <summary>Calculates the spectrum for real data and appends to the existing spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub TransformAppend(ByVal reals() As Single)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' Initialize imaginary array.
        Dim imaginaries() As Single = {}
        ReDim imaginaries(reals.Length - 1)

        ' calculate the spectrum
        MyBase.Calculate(reals, imaginaries)

        ' add the spectrum
        Me.Append(reals, imaginaries)

    End Sub

    ''' <summary>Calculates the spectrum and appends to the existing spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub TransformAppend(ByVal reals() As Double, ByVal imaginaries() As Double)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' calculate the spectrum
        MyBase.Calculate(reals, imaginaries)

        ' add the spectrum
        Me.Append(reals, imaginaries)

    End Sub

    ''' <summary>Calculates the spectrum and appends to the existing spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="imaginaries">Holds the imaginary values.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub TransformAppend(ByVal reals() As Single, ByVal imaginaries() As Single)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        ' calculate the spectrum
        MyBase.Calculate(reals, imaginaries)

        ' add the spectrum
        Me.Append(reals, imaginaries)

    End Sub

    ''' <summary>
    ''' Calculates the spectrum for real data and appends to the existing spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="dataPoints">Specifies the number of data points to use.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub TransformAppend(ByVal reals() As Double, ByVal dataPoints As Integer)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        If dataPoints = reals.Length Then

            TransformAppend(reals)

        Else

            ' Get the sre data
            ' TO_DO: change to double once we fix mixed radix for double.
            Dim realPart() As Double = {}
            ReDim realPart(dataPoints - 1)
            isr.Numerics.Signals.Helper.Copy(reals, realPart)
            ' reals.CopyTo(realPart, 0)
            If reals.Length < realPart.Length Then
                For i As Integer = reals.Length To realPart.Length - 1
                    realPart(i) = realPart(i - 1)
                Next
            End If
            TransformAppend(realPart)

        End If

    End Sub

    ''' <summary>Calculates the spectrum for real data and appends to the existing spectrum.
    ''' </summary>
    ''' <param name="reals">Holds the real values.</param>
    ''' <param name="dataPoints">Specifies the number of data points to use.</param>
    ''' <example>
    '''   Sub Form_Click
    '''   End Sub
    ''' </example>
    Public Sub TransformAppend(ByVal reals() As Single, ByVal dataPoints As Integer)

        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If reals.Length < 2 Then
            Throw New System.ArgumentOutOfRangeException("reals", "Array must be longer than 1")
        End If

        If dataPoints = reals.Length Then

            TransformAppend(reals)

        Else

            ' Get the sre data
            Dim realPart() As Single = {}
            ReDim realPart(dataPoints - 1)
            If reals.Length < realPart.Length Then
                For i As Integer = reals.Length To realPart.Length - 1
                    realPart(i) = realPart(i - 1)
                Next
            End If
            TransformAppend(realPart)

        End If

    End Sub

    ''' <summary>Gets or sets the condition for the densities were scaled.  Once the densities
    '''   are scaled no more averages can be appended until the spectrum
    '''   is <see cref="Restore">restored</see></summary>
    Private _scaled As Boolean

    ''' <summary>Writes the densities to a data file.  The densities are 
    '''   written as scaled.</summary>
    ''' <param name="writer">Specifies reference to an open binary writer.</param>
    Public Overloads Sub WriteDensities(ByVal writer As System.IO.BinaryWriter)

        If writer Is Nothing Then
            Throw New ArgumentNullException("writer")
        End If

        If Me._densities Is Nothing Then
            Throw New isr.Numerics.Signals.BaseException("Spectrum densities do not exist.")
        End If

        ' write the spectrum parameters
        writer.Write(Me._averageCount)

        Dim elementCount As Integer = Me._densities.Length
        writer.Write(elementCount)

        If Me._scaled Then
            ' write values to the file
            For i As Integer = 0 To elementCount - 1
                writer.Write(Me._densities(i))
            Next i
        Else
            Dim factor As Double = Me.SpectrumScaleFactor(False)
            For i As Integer = 0 To elementCount - 1
                writer.Write(factor * Me._densities(i))
            Next i
        End If

    End Sub

#End Region

End Class

