Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Dsp Library Tester")> 
<Assembly: AssemblyDescription("Dsp Library Tester")> 
<Assembly: AssemblyProduct("Numerics.Signals.Library.Tester.2007")> 
<Assembly: CLSCompliant(True)> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: ComVisible(False)> 

' The following GUID is for the ID of the type library if this project is exposed to COM
<Assembly: Guid("0DD1D48A-DD62-4655-B9D9-417FC47A1964")> 
