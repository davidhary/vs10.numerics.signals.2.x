''' <summary>Includes test program for calculating the spectrum using DFT, Mixed Radix, 
''' and sliding Fourier transform algorithms.</summary>
''' <remarks>Launch this form by calling its Show or ShowDialog method from its default
'''   instance.</remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class SpectrumPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

#Region " UNUSED "

#If False Then
    ''' <summary>Cleans up managed components.</summary>
  ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
  Private Sub onDisposeManagedResources()

  End Sub

  ''' <summary>Cleans up unmanaged components.</summary>
  ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
  Private Sub onDisposeUnmanagedResources()

  End Sub

#End If

#End Region

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets the selected example.
    ''' </summary>
    Private ReadOnly Property selectedExample() As Example
        Get
            Return CType(CType(Me._ExampleComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, Example)
        End Get
    End Property

    ''' <summary>Returns true if an instance of the class was created and not disposed.</summary>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return My.Forms.SpectrumPanel IsNot Nothing AndAlso Not My.Forms.SpectrumPanel.IsDisposed
        End Get
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>This event is a notification that the form has already gone away before
    ''' control is returned to the calling method (in case of a modal form).  Use this
    ''' method to delete any temporary files that were created or dispose of any objects
    ''' not disposed with the closing event.
    ''' </remarks>
    Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs)
      Handles MyBase.Closed

    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
      Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        My.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
      Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": SPECTRUM PANEL"

            ' Initialize and set the user interface
            initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            ' loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    Private Sub showStatus(ByVal message As String)

        Me._MessagesList.PushMessage(message)
        Me._StatusStatusBarPanel.Text = message

    End Sub

    ''' <summary>Gets or sets the data format</summary>
    Const listFormat As String = "{0:0.000000000000000}{1}{2:0.000000000000000}"

    ''' <summary>Enumerates the action options.</summary>
    Private Enum Example
        <System.ComponentModel.Description("Discrete Fourier Transform")> DiscreteFourierTransform
        <System.ComponentModel.Description("Sliding FFT")> SlidingFFT
        <System.ComponentModel.Description("Mixed Radix FFT")> MixedRadixFFT
    End Enum

    ''' <summary>Initializes the user interface and tool tips.</summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
    Private Sub initializeUserInterface()

        ' tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
        ' tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")

        ' Set initial values defining the signal
        Me._PointsTextBox.Text = "16" ' "1000"
        Me._CyclesTextBox.Text = "1" ' "11.5"
        Me._PhaseTextBox.Text = "0" ' "45"
        Me._PointsToDisplayTextBox.Text = "100"
        Me._SignalDurationTextBox.Text = "1"

        ' set the default sample
        Me.populateExampleComboBox()
        Me._ExampleComboBox.SelectedIndex = 0

        ' create the two charts.
        createSpectrumChart()
        createSignalChart()

        ' plot the default sign wave
        Me.SineWaveDouble()

    End Sub

    ''' <summary>Calculates the sine wave.
    ''' </summary>
    ''' <param name="frequency"></param>
    ''' <param name="phase"></param>
    Private Overloads Function SineWave(ByVal frequency As Single, ByVal phase As Single, ByVal elements As Integer) As Single()

        ' get the signal
        Dim signal() As Single = isr.Numerics.Signals.Signal.Sine(frequency, phase, elements)

        ' Plot the Signal 
        Me.chartSignal(isr.Numerics.Signals.Signal.Ramp(Single.Parse(Me._SignalDurationTextBox.Text,
                                                                     Globalization.CultureInfo.CurrentCulture) / elements, elements), signal)

        Return signal

    End Function

    ''' <summary>Calculates the sine wave.
    ''' </summary>
    ''' <param name="frequency"></param>
    ''' <param name="phase"></param>
    Private Overloads Function SineWave(ByVal frequency As Double, ByVal phase As Double, ByVal elements As Integer) As Double()

        ' get the signal
        Dim signal() As Double = isr.Numerics.Signals.Signal.Sine(frequency, phase, elements)

        ' Plot the Signal 
        Me.chartSignal(isr.Numerics.Signals.Signal.Ramp(Double.Parse(Me._SignalDurationTextBox.Text,
                                                                     Globalization.CultureInfo.CurrentCulture) / elements, elements), signal)

        Return signal

    End Function

    ''' <summary>Calculates the sine wave.
    ''' </summary>
    Private Function SineWaveDouble() As Double()

        ' Read number of points from the text box.
        Dim signalPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signalCycles As Double = Double.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signalPhase As Double = isr.Numerics.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture))
        Return SineWave(signalCycles, signalPhase, signalPoints)

    End Function

    ''' <summary>Calculates the sine wave.
    ''' </summary>
    Private Function SineWaveSingle() As Single()

        ' Read number of points from the text box.
        Dim signalPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signalCycles As Single = Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        Dim signalPhase As Single = Convert.ToSingle(isr.Numerics.Signals.Signal.ToRadians(
                Single.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture)))
        Return SineWave(signalCycles, signalPhase, signalPoints)

    End Function

    Private Const copySigFormat As String = "    Copy Signal:  {0:0.###} ms "
    Private Const fftInitFormat As String = " FFT Initialize:  {0:0} ms "
    Private Const fftCalcFormat As String = "  FFT Calculate:  {0:0.###} ms "
    Private Const frqCalcFormat As String = "FFT Frequencies:  {0:0.###} ms "
    Private Const magCalcFormat As String = "  FFT Magnitude:  {0:0.###} ms "
    Private Const invCalcFormat As String = "    Inverse FFT:  {0:0.###} ms "
    Private Const chartinFormat As String = "  Charting time:  {0:0} ms "

    ''' <summary>Calculates the Spectrum based on the selected algorithm
    '''   and displays the results.
    ''' </summary>
    Private Sub calculateSpectrumDouble(ByVal algorithm As Example)

        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        Dim duration As TimeSpan
        Me._TimingTextBox.Text = isr.Core.EnumExtensions.Description(algorithm)

        showStatus("Calculating double-precision " & algorithm.ToString & " FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the sine wave.
        Dim signal() As Double = Me.SineWaveDouble()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Double
        ReDim real(fftPoints - 1)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Double
        ReDim imaginary(fftPoints - 1)

        ' ------------ SELECT PROPERTIES ---------------

        ' select the spectrum type
        Dim fft As isr.Numerics.Signals.FourierTransform = Nothing
        Try

            Select Case algorithm
                Case Example.DiscreteFourierTransform
                    fft = New isr.Numerics.Signals.DiscreteFourierTransform
                Case Example.MixedRadixFFT
                    fft = New isr.Numerics.Signals.MixedRadixFourierTransform
                Case Else
                    Return
            End Select
            Using spectrum As isr.Numerics.Signals.Spectrum = New isr.Numerics.Signals.Spectrum(fft)


                ' scale the FFT by the Window power and data points
                spectrum.IsScaleFft = True

                ' Remove mean before calculating the fft
                spectrum.IsRemoveMean = Me._RemoveMeanCheckBox.Checked

                ' Use Taper Window as selected
                If Me._TaperWindowCheckBox.Checked Then
                    spectrum.TaperWindow = New isr.Numerics.Signals.BlackmanTaperWindow
                Else
                    spectrum.TaperWindow = Nothing
                End If

                ' Initialize the FFT.
                timeKeeper.Start()
                spectrum.Initialize(real, imaginary)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, fftInitFormat, duration.TotalMilliseconds)

                ' ------------ CREATE TEH SIGNAL ---------------

                timeKeeper.Start()
                signal.CopyTo(real, 0)
                Array.Clear(imaginary, 0, imaginary.Length)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, copySigFormat, duration.TotalMilliseconds)

                ' ------------ Calculate Forward and Inverse FFT ---------------

                ' Compute the FFT.
                timeKeeper.Start()
                spectrum.Calculate(real, imaginary)
                duration = timeKeeper.Elapsed
                Me._TransformTimeStatusBarPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###} ms",
                                                              duration.TotalMilliseconds)
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, fftCalcFormat, duration.TotalMilliseconds)

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Numerics.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                Dim magnitudes() As Double
                magnitudes = isr.Numerics.Signals.Helper.Magnitudes(real, imaginary)

                timeKeeper.Start()
                magnitudes = isr.Numerics.Signals.Helper.Magnitudes(real, imaginary)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, magCalcFormat, duration.TotalMilliseconds)

                ' Get the Frequency
                Dim frequencies() As Double

                timeKeeper.Start()
                frequencies = isr.Numerics.Signals.Signal.Frequencies(1.0R, fftPoints)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, frqCalcFormat, duration.TotalMilliseconds)

                ' Compute the inverse transform.
                timeKeeper.Start()
                fft.Inverse(real, imaginary)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, invCalcFormat, duration.TotalMilliseconds)

                ' Display the forward and inverse data.
                DisplayCalculationAccuracy(signal, real)

                ' ------------ Plot FFT outcome ---------------
                timeKeeper.Start()
                chartAmplitudeSpectrum(frequencies, magnitudes, 1)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, chartinFormat, duration.TotalMilliseconds)
            End Using
        Catch
            Throw
        Finally
            If fft IsNot Nothing Then
                fft.Dispose()
                fft = Nothing
            End If
        End Try

    End Sub

    ''' <summary>Calculates the Spectrum based on the selected algorithm
    '''   and displays the results.
    ''' </summary>
    Private Sub calculateSpectrumSingle(ByVal algorithm As Example)

        Dim timeKeeper As Diagnostics.Stopwatch
        timeKeeper = New Diagnostics.Stopwatch
        Dim duration As TimeSpan

        showStatus("Calculating single-precision " & algorithm.ToString & " FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create and plot the sine wave.
        Dim signal() As Single = Me.SineWaveSingle()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Single
        ReDim real(fftPoints - 1)
        signal.CopyTo(real, 0)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Single
        ReDim imaginary(fftPoints - 1)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        Dim fft As isr.Numerics.Signals.FourierTransform = Nothing
        Try
            Select Case algorithm
                Case Example.DiscreteFourierTransform
                    fft = New isr.Numerics.Signals.DiscreteFourierTransform
                Case Example.MixedRadixFFT
                    fft = New isr.Numerics.Signals.MixedRadixFourierTransform
                Case Else
                    Return
            End Select
            Using spectrum As isr.Numerics.Signals.Spectrum = New isr.Numerics.Signals.Spectrum(fft)

                ' scale the FFT by the Window power and data points
                spectrum.IsScaleFft = True

                ' Remove mean before calculating the fft
                spectrum.IsRemoveMean = Me._RemoveMeanCheckBox.Checked

                ' Use Taper Window as selected
                If Me._TaperWindowCheckBox.Checked Then
                    spectrum.TaperWindow = New isr.Numerics.Signals.BlackmanTaperWindow
                Else
                    spectrum.TaperWindow = Nothing
                End If

                ' Initialize the FFT.
                timeKeeper.Start()
                spectrum.Initialize(real, imaginary)
                duration = timeKeeper.Elapsed
                Me._TimingTextBox.Text = Me._TimingTextBox.Text & Environment.NewLine &
                    String.Format(Globalization.CultureInfo.CurrentCulture, fftInitFormat, duration.TotalMilliseconds)

                ' Compute the FFT.
                timeKeeper.Start()
                spectrum.Calculate(real, imaginary)
                duration = timeKeeper.Elapsed

                ' Display time
                Me._TransformTimeStatusBarPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###} ms ",
                                                              duration.TotalMilliseconds)

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Numerics.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                Dim magnitudes() As Single
                magnitudes = isr.Numerics.Signals.Helper.Magnitudes(real, imaginary)

                ' Get the Frequency
                Dim frequencies() As Single
                frequencies = isr.Numerics.Signals.Signal.Frequencies(1.0F, fftPoints)

                ' Compute the inverse transform.
                fft.Inverse(real, imaginary)

                ' Display the forward and inverse data.
                DisplayCalculationAccuracy(signal, real)

                ' ------------ Plot FFT outcome ---------------
                chartAmplitudeSpectrum(frequencies, magnitudes, 1)
            End Using
        Catch
            Throw
        Finally
            If fft IsNot Nothing Then
                fft.Dispose()
                fft = Nothing
            End If
        End Try

    End Sub

    ''' <summary>Displays the test results.</summary>
    ''' <param name="signal"></param>
    ''' <param name="real"></param>
    Private Sub DisplayCalculationAccuracy(ByVal signal() As Double, ByVal real() As Double)

        If signal Is Nothing Then
            Exit Sub
        End If
        If real Is Nothing Then
            Exit Sub
        End If
        Dim elementCount As Integer = Math.Min(real.Length, Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        If elementCount <= 0 Then
            Exit Sub
        End If

        ' Display the forward and inverse data.
        Dim totalError As Double = 0
        Me._SignalListBox.Items.Clear()
        Dim value As Double
        For i As Integer = 0 To elementCount - 1
            value = signal(i) - real(i)
            totalError += value * value
            Me._SignalListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture,
                                               listFormat, signal(i), Microsoft.VisualBasic.ControlChars.Tab, real(i)))
        Next i
        totalError = System.Math.Sqrt(totalError / Convert.ToDouble(elementCount))
        Me._ErrorStatusBarPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###E+0}", totalError)
        Me._StatusBar.Invalidate()

    End Sub

    ''' <summary>Displays the test results.</summary>
    ''' <param name="signal"></param>
    ''' <param name="real"></param>
    Private Sub displayCalculationAccuracy(ByVal signal() As Single, ByVal real() As Single)

        If signal Is Nothing Then
            Exit Sub
        End If
        If real Is Nothing Then
            Exit Sub
        End If
        Dim elementCount As Integer = Math.Min(real.Length, Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        If elementCount <= 0 Then
            Exit Sub
        End If

        ' Display the forward and inverse data.
        Dim totalError As Double = 0
        Me._SignalListBox.Items.Clear()
        Dim value As Double
        For i As Integer = 0 To elementCount - 1
            value = signal(i) - real(i)
            totalError += value * value
            Me._SignalListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture,
                                               listFormat, signal(i), Microsoft.VisualBasic.ControlChars.Tab, real(i)))
        Next i
        totalError = System.Math.Sqrt(totalError / Convert.ToDouble(elementCount))
        Me._ErrorStatusBarPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###E+0}", totalError)
        Me._StatusBar.Invalidate()

    End Sub

    ''' <summary>Populates the list of options in the action combo box.</summary>
    ''' <remarks>It seems that out enumerated list does not work very well with
    '''   this list.</remarks>
    Private Sub populateExampleComboBox()

        ' set the action list
        Me._ExampleComboBox.Items.Clear()
        Me._ExampleComboBox.DataSource = isr.Core.EnumExtensions.ValueDescriptionPairs(GetType(Example))
        Me._ExampleComboBox.DisplayMember = "Value"
        Me._ExampleComboBox.ValueMember = "Key"

    End Sub

    ''' <summary>Evaluates sliding FFT until stopped.</summary>
    ''' <param name="noiseFigure">Specifies the relative noise level to add to the 
    '''   signal for observing the changes in the spectrum.</param>
    Private Sub slidingFft(ByVal noiseFigure As Double)

        showStatus("Calculating double-precision sliding FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signal() As Double = Me.SineWaveDouble()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Double
        ReDim real(fftPoints - 1)
        signal.CopyTo(real, 0)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Double
        ReDim imaginary(fftPoints - 1)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        Dim duration As TimeSpan = TimeSpan.Zero
        ' Create a new instance of the Mixed Radix class
        Using fft As isr.Numerics.Signals.MixedRadixFourierTransform = New isr.Numerics.Signals.MixedRadixFourierTransform
            ' Compute the FFT.
            Dim timeKeeper As Diagnostics.Stopwatch
            timeKeeper = New Diagnostics.Stopwatch
            timeKeeper.Start()
            fft.Forward(real, imaginary)
            duration = timeKeeper.Elapsed
        End Using

        ' Display time
        Me._TransformTimeStatusBarPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###} ms",
                                                      duration.TotalMilliseconds)

        ' Clear the data
        Me._SignalListBox.Items.Clear()

        ' Clear the error value
        Me._ErrorStatusBarPanel.Text = String.Empty

        ' Get the FFT magnitudes
        Dim magnitudes() As Double
        magnitudes = isr.Numerics.Signals.Helper.Magnitudes(real, imaginary)

        ' Get the Frequency
        Dim frequencies() As Double
        frequencies = isr.Numerics.Signals.Signal.Frequencies(1.0R, fftPoints)

        ' Initialize the last point of the signal to the last point.
        Dim lastSignalPoint As Integer = fftPoints - 1

        ' Initialize the first point of the signal to the last point.
        Dim firstSignalPoint As Integer = 0

        ' Calculate the phase of the last point of the sine wave
        Dim deltaPhase As Double = 2 * Math.PI * Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) / Convert.ToSingle(fftPoints)

        ' get the signal phase of the last point
        Dim signalPhase As Double = isr.Numerics.Signals.Signal.ToRadians(Double.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture))
        signalPhase += deltaPhase * lastSignalPoint

        ' Use a new frequency for the sine wave to see how
        ' the old is phased out and the new gets in
        deltaPhase *= 2

        ' First element in the previous Real part of the time series
        Dim oldReal As Double

        ' First element in the previous imaginary part of the time series.
        Dim oldImaginary As Double

        ' New Real part of Signal
        Dim newReal As Double

        ' New imaginary part of Signal
        Dim newImaginary As Double

        ' Create a new instance of the sliding fft class
        Using slidingFft As isr.Numerics.Signals.SlidingFourierTransform = New isr.Numerics.Signals.SlidingFourierTransform

            ' Initialize sliding FFT coefficients.
            slidingFft.Initialize(real, imaginary)

            ' Recalculate the sliding FFT
            Do While Me._StartStopCheckBox.Checked

                ' Allow other events to occur
                My.Application.DoEvents()

                ' ------- Calculate FFT outcomes ---------

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Numerics.Signals.Helper.Map(real, imaginary)

                ' Get the FFT magnitudes
                magnitudes = isr.Numerics.Signals.Helper.Magnitudes(real, imaginary)

                ' Get the Frequency
                frequencies = isr.Numerics.Signals.Signal.Frequencies(1.0R, fftPoints)

                ' ------------ Plot the Signal ---------------

                Me.chartSignal(isr.Numerics.Signals.Signal.Ramp(Double.Parse(Me._SignalDurationTextBox.Text,
                                                                             Globalization.CultureInfo.CurrentCulture) / fftPoints, fftPoints), signal)

                ' ------------ Plot FFT outcome ---------------

                chartAmplitudeSpectrum(frequencies, magnitudes, 1)

                ' Update the previous values of the signal
                oldReal = signal(firstSignalPoint)
                oldImaginary = 0

                ' Get new signal values.
                signalPhase += deltaPhase
                newReal = System.Math.Sin(signalPhase)
                newImaginary = 0

                ' Add some random noise to make it interesting.
                newReal += noiseFigure * 2 * (Microsoft.VisualBasic.Rnd() - 0.5)

                ' Update the signal itself.

                ' Update the last point.
                lastSignalPoint += 1
                If lastSignalPoint >= fftPoints Then
                    lastSignalPoint = 0
                End If

                ' Update the first point.
                firstSignalPoint += 1
                If firstSignalPoint >= fftPoints Then
                    firstSignalPoint = 0
                End If

                ' Place new data in the signal itself.
                signal(lastSignalPoint) = newReal

                ' You must re-map the FFT utilities if using more than one time series at a time
                ' isr.Numerics.Signals.Helper.Map(real, imaginary)

                ' Calculate the sliding FFT coefficients.
                slidingFft.Update(newReal, newImaginary, oldReal, oldImaginary)

            Loop
        End Using


    End Sub

    ''' <summary>Evaluates sliding FFT until stopped.</summary>
    ''' <param name="noiseFigure">Specifies the relative noise level to add to the 
    '''   signal for observing the changes in the spectrum.</param>
    Private Sub slidingFft(ByVal noiseFigure As Single)

        showStatus("Calculating single-precision sliding FFT")

        ' ------------ Create the Signal for FFT ---------------

        ' Set FFT points 
        Dim fftPoints As Integer = Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture)

        ' Create cycles of the sine wave.
        Dim signal() As Single = Me.SineWaveSingle()

        ' Allocate array for the Real part of the DFT.
        Dim real() As Single
        ReDim real(fftPoints - 1)
        signal.CopyTo(real, 0)

        ' Allocate array for the imaginary part of the DFT.
        Dim imaginary() As Single
        ReDim imaginary(fftPoints - 1)

        ' ------------ Calculate Forward and Inverse FFT ---------------

        ' Create a new instance of the Mixed Radix class
        Using fft As isr.Numerics.Signals.MixedRadixFourierTransform = New isr.Numerics.Signals.MixedRadixFourierTransform

            ' Compute the FFT.
            Dim timeKeeper As Diagnostics.Stopwatch
            timeKeeper = New Diagnostics.Stopwatch
            timeKeeper.Start()
            fft.Forward(real, imaginary)
            Dim duration As TimeSpan = timeKeeper.Elapsed

            ' Display time
            Me._TransformTimeStatusBarPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:0.###} ms",
                duration.TotalMilliseconds)

            ' Clear the data
            Me._SignalListBox.Items.Clear()
            Me._ErrorStatusBarPanel.Text = String.Empty

            ' Get the FFT magnitudes
            Dim magnitudes() As Single
            magnitudes = isr.Numerics.Signals.Helper.Magnitudes(real, imaginary)

            ' Get the Frequency
            Dim frequencies() As Single
            frequencies = isr.Numerics.Signals.Signal.Frequencies(1.0F, fftPoints)

            ' Initialize the last point of the signal to the last point.
            Dim lastSignalPoint As Integer = fftPoints - 1

            ' Initialize the first point of the signal to the last point.
            Dim firstSignalPoint As Integer = 0

            ' Calculate the phase of the last point of the sine wave
            Dim deltaPhase As Single = 2.0F * Convert.ToSingle(Math.PI)
                * Single.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) / fftPoints

            ' get the signal phase of the last point
            Dim signalPhase As Single = Convert.ToSingle(isr.Numerics.Signals.Signal.ToRadians(
                                Single.Parse(Me._PhaseTextBox.Text, Globalization.CultureInfo.CurrentCulture)))
            signalPhase += deltaPhase * lastSignalPoint

            ' Use a new frequency for the sine wave to see how
            ' the old is phased out and the new gets in
            deltaPhase *= 2

            Dim oldReal As Single ' First element in the previous
            ' real part of the time series.

            Dim oldImaginary As Single ' First element in the previous
            ' imaginary time series.

            Dim newReal As Single ' New Real part of Signal

            Dim newImaginary As Single ' New imaginary part of Signal

            ' Create a new instance of the sliding fft class
            Using slidingFft As isr.Numerics.Signals.SlidingFourierTransform = New isr.Numerics.Signals.SlidingFourierTransform

                ' Initialize sliding FFT coefficients.
                slidingFft.Initialize(real, imaginary)

                ' Recalculate the sliding FFT
                Do While Me._StartStopCheckBox.Checked

                    ' Allow other events to occur
                    My.Application.DoEvents()

                    ' ------- Calculate FFT outcomes ---------

                    ' You must re-map the FFT utilities if using more than one time series at a time
                    ' isr.Numerics.Signals.Helper.Map(real, imaginary)

                    ' Get the FFT magnitudes
                    magnitudes = isr.Numerics.Signals.Helper.Magnitudes(real, imaginary)

                    ' Get the Frequency
                    frequencies = isr.Numerics.Signals.Signal.Frequencies(1.0F, fftPoints)

                    ' ------------ Plot the Signal ---------------

                    Me.chartSignal(isr.Numerics.Signals.Signal.Ramp(Single.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture) / fftPoints, fftPoints), signal)

                    ' ------------ Plot FFT outcome ---------------

                    chartAmplitudeSpectrum(frequencies, magnitudes, 1)

                    ' Update the previous values of the signal
                    oldReal = signal(firstSignalPoint)
                    oldImaginary = 0

                    ' Get new signal values.
                    signalPhase += deltaPhase
                    newReal = Convert.ToSingle(System.Math.Sin(signalPhase))
                    newImaginary = 0

                    ' Add some random noise to make it interesting.
                    newReal += noiseFigure * 2.0F * (Microsoft.VisualBasic.Rnd() - 0.5F)

                    ' Update the last point.
                    lastSignalPoint += 1
                    If lastSignalPoint >= fftPoints Then
                        lastSignalPoint = 0
                    End If

                    ' Update the first point.
                    firstSignalPoint += 1
                    If firstSignalPoint >= fftPoints Then
                        firstSignalPoint = 0
                    End If

                    ' Place new data in the signal itself.
                    signal(lastSignalPoint) = newReal

                    ' You must re-map the FFT utilities if using more than one time series at a time
                    ' isr.Numerics.Signals.Helper.Map(real, imaginary)

                    ' Calculate the sliding FFT coefficients.
                    slidingFft.Update(newReal, newImaginary, oldReal, oldImaginary)
                Loop
            End Using
        End Using

    End Sub

#End Region

#Region " CHARTING "

#Region " AMPLITUDE SPECTRUM CHART "

    ''' <summary>Gets or sets reference to the amplitude spectrum curve.</summary>
    Private amplitudeSpectrumCurve As isr.Drawing.Curve

    ''' <summary>Gets or sets reference to the spectrum amplitude axis.</summary>
    Private amplitudeSpectrumAxis As isr.Drawing.Axis

    ''' <summary>Gets or sets reference to the spectrum frequency axis.</summary>
    Private frequencyAxis As isr.Drawing.Axis

    ''' <summary>Charts spectrum.</summary>
    Private _spectrumChartPan As isr.Drawing.ChartPane

    ''' <summary>Creates the the chart for displaying the amplitude spectrum.</summary>
    Private Sub createSpectrumChart()

        ' set chart area and titles.
        Me._spectrumChartPan = New isr.Drawing.ChartPane
        Me._spectrumChartPan.PaneArea = New RectangleF(10, 10, 10, 10)
        Me._spectrumChartPan.Title.Caption = "Spectrum"
        Me._spectrumChartPan.Legend.Visible = False

        frequencyAxis = Me._spectrumChartPan.AddAxis("Frequency, Hz", isr.Drawing.AxisType.X)
        frequencyAxis.Max = New isr.Drawing.AutoValueR(100, False)
        frequencyAxis.Min = New isr.Drawing.AutoValueR(1, False)

        frequencyAxis.Grid.Visible = True
        frequencyAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        frequencyAxis.TickLabels.Visible = True
        frequencyAxis.TickLabels.DecimalPlaces = New isr.Drawing.AutoValue(1, True)
        ' frequencyAxis.TickLabels.Appearance.Angle = 60.0F

        amplitudeSpectrumAxis = Me._spectrumChartPan.AddAxis("Amplitude, Volts", isr.Drawing.AxisType.Y)
        amplitudeSpectrumAxis.CoordinateScale = New isr.Drawing.CoordinateScale(isr.Drawing.CoordinateScaleType.Linear)

        amplitudeSpectrumAxis.Title.Visible = True

        amplitudeSpectrumAxis.Max = New isr.Drawing.AutoValueR(1, True)
        amplitudeSpectrumAxis.Min = New isr.Drawing.AutoValueR(0, True)

        amplitudeSpectrumAxis.Grid.Visible = True
        amplitudeSpectrumAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        amplitudeSpectrumAxis.TickLabels.Visible = True
        amplitudeSpectrumAxis.TickLabels.DecimalPlaces = New isr.Drawing.AutoValue(0, True)

        Me._spectrumChartPan.AxisFrame.FillColor = Color.WhiteSmoke 'color.FromArgb(232, 236, 245) '  Color.LightGoldenrodYellow

        Me._spectrumChartPan.SetSize(Me._SpectrumChartPanel.ClientRectangle)

        amplitudeSpectrumCurve = Me._spectrumChartPan.AddCurve(Drawing.CurveType.XY, "Amplitude Spectrum", frequencyAxis, amplitudeSpectrumAxis)
        amplitudeSpectrumCurve.Cord.LineColor = Color.Red
        amplitudeSpectrumCurve.Cord.CordType = Drawing.CordType.Linear
        amplitudeSpectrumCurve.Symbol.Visible = False
        amplitudeSpectrumCurve.Cord.LineWidth = 1.0F

        Me._spectrumChartPan.Rescale()

    End Sub

    ''' <summary>Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
    ''' </summary>
    ''' <param name="frequencies">Holds the spectrum frequencies.</param>
    ''' <param name="magnitudes">Holds the spectrum amplitudes.</param>
    ''' <param name="scaleFactor">Specifies the scale factor by which to scale the 
    '''   spectrum for plotting data that was not scaled</param>
    Private Sub chartAmplitudeSpectrum(ByVal frequencies() As Single, ByVal magnitudes() As Single,
                                       ByVal scaleFactor As Single)

        ' copy the amplitudes to a double array
        Dim amplitudes(frequencies.Length - 1) As Double
        magnitudes.CopyTo(amplitudes, 0)

        ' copy the frequencies to a double array.
        Dim freq(frequencies.Length - 1) As Double
        frequencies.CopyTo(freq, 0)
        chartAmplitudeSpectrum(freq, amplitudes, scaleFactor)

    End Sub

    ''' <summary>Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
    ''' </summary>
    ''' <param name="frequencies">Holds the spectrum frequencies.</param>
    ''' <param name="magnitudes">Holds the spectrum amplitudes.</param>
    ''' <param name="scaleFactor">Specifies the scale factor by which to scale the 
    '''   spectrum for plotting data that was not scaled</param>
    Private Sub chartAmplitudeSpectrum(ByVal frequencies() As Double, ByVal magnitudes() As Double,
                                       ByVal scaleFactor As Double)

        ' adjust abscissa scale.
        frequencyAxis.Min = New isr.Drawing.AutoValueR(frequencies(frequencies.GetLowerBound(0)), False)
        frequencyAxis.Max = New isr.Drawing.AutoValueR(frequencies(frequencies.GetUpperBound(0)), False)

        ' adjust ordinate scale.
        amplitudeSpectrumAxis.Max = New isr.Drawing.AutoValueR(1, True)
        amplitudeSpectrumAxis.Min = New isr.Drawing.AutoValueR(0, True)

        ' Set initial value for frequency
        '    Dim currentFrequency As Double = Gopher.AmplitudeSpectrumChart.AbscissaMin.Value
        '   Dim deltaFrequency As Double = 1.0# / Gopher.Test.EpochDuration

        ' plot all but last, which was plotted above

        ' scale the magnitudes
        Dim amplitudes(frequencies.Length - 1) As Double
        For i As Integer = frequencies.GetLowerBound(0) To frequencies.GetUpperBound(0)

            amplitudes(i) = scaleFactor * magnitudes(i)
            If (i Mod 50) = 0 Then
                Threading.Thread.Sleep(1)
            End If

        Next i

        amplitudeSpectrumCurve.UpdateData(frequencies, amplitudes)
        Me._spectrumChartPan.Rescale()
        Me._SpectrumChartPanel.Invalidate()

    End Sub

    ''' <summary>Redraws the amplitude spectrum Chart.</summary>
    Private Sub spectrumChartPanel_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles _SpectrumChartPanel.Paint
        If sender IsNot Nothing Then
            Using sb As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(sb, Me.ClientRectangle)
                Me._spectrumChartPan.Draw(e.Graphics)
            End Using
        End If
    End Sub

    ''' <summary>Redraws the amplitude spectrum chart when the form is resized</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of the 
    '''   chart panel <see cref="System.Windows.Forms.Panel"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    Private Sub spectrumChartPanel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SpectrumChartPanel.Resize
        If Not Me._spectrumChartPan Is Nothing Then
            Me._spectrumChartPan.SetSize(Me._SpectrumChartPanel.ClientRectangle)
        End If
    End Sub

#End Region

#Region " SIGNAL CHART "

    ''' <summary>Gets or sets reference to the instantaneous DP curve.</summary>
    Private signalCurve As isr.Drawing.Curve

    ''' <summary>Gets or sets reference to the instantaneous DP amplitude axis.</summary>
    Private signalAxis As isr.Drawing.Axis

    ''' <summary>Gets or sets reference to the instantaneous DP time axis.</summary>
    Private timeAxis As isr.Drawing.Axis

    ''' <summary>Charts spectrum.</summary>
    Private signalChartPane As isr.Drawing.ChartPane

    ''' <summary>Creates the the Scope for display of voltages during the epoch.</summary>
    Private Sub createSignalChart()

        signalChartPane = New isr.Drawing.ChartPane

        ' set chart area and titles.
        signalChartPane.PaneArea = New RectangleF(10, 10, 10, 10)
        signalChartPane.Title.Caption = "Voltage versus Time"
        signalChartPane.Legend.Visible = False

        timeAxis = signalChartPane.AddAxis("Time, Seconds", isr.Drawing.AxisType.X)
        timeAxis.Max = New isr.Drawing.AutoValueR(1, False)
        timeAxis.Min = New isr.Drawing.AutoValueR(0, False)

        timeAxis.Grid.Visible = True
        timeAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        timeAxis.TickLabels.Visible = True
        timeAxis.TickLabels.DecimalPlaces = New isr.Drawing.AutoValue(1, True)
        ' timeAxis.TickLabels.Appearance.Angle = 60.0F

        signalAxis = signalChartPane.AddAxis("Volts", isr.Drawing.AxisType.Y)
        signalAxis.CoordinateScale = New isr.Drawing.CoordinateScale(isr.Drawing.CoordinateScaleType.Linear)

        signalAxis.Title.Visible = True

        signalAxis.Max = New isr.Drawing.AutoValueR(1, True)
        signalAxis.Min = New isr.Drawing.AutoValueR(-1, True)

        signalAxis.Grid.Visible = True
        signalAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        signalAxis.TickLabels.Visible = True
        signalAxis.TickLabels.DecimalPlaces = New isr.Drawing.AutoValue(0, True)

        signalChartPane.AxisFrame.FillColor = Color.WhiteSmoke '  Color.FromArgb(232, 236, 245) ' Color.LightGoldenrodYellow
        signalChartPane.SetSize(Me._SignalChartPanel.ClientRectangle)

        signalCurve = signalChartPane.AddCurve(Drawing.CurveType.XY, "TimeSeries", timeAxis, signalAxis)
        signalCurve.Cord.LineColor = Color.Red
        signalCurve.Cord.CordType = Drawing.CordType.Linear
        signalCurve.Symbol.Visible = False
        signalCurve.Cord.LineWidth = 1.0F

        signalChartPane.Rescale()

    End Sub

    ''' <summary>Plot the DP signal from the entire epoch data simulating a 
    '''   strip chart effect.</summary>
    Private Sub chartSignal(ByVal times() As Single, ByVal amplitudes() As Single)

        ' plot all but last, which was plotted above
        Dim newApms(times.Length - 1) As Double
        Dim newTimes(times.Length - 1) As Double
        For i As Integer = times.GetLowerBound(0) To times.GetUpperBound(0)

            newApms(i) = amplitudes(i)
            newTimes(i) = times(i)
            If (i Mod 50) = 0 Then
                Threading.Thread.Sleep(1)
            End If

        Next i

        chartSignal(newTimes, newApms)

    End Sub

    ''' <summary>Plot the DP signal from the entire epoch data simulating a 
    '''   strip chart effect.</summary>
    Private Sub chartSignal(ByVal times() As Double, ByVal amplitudes() As Double)

        timeAxis.Max = New isr.Drawing.AutoValueR(Double.Parse(Me._SignalDurationTextBox.Text, Globalization.CultureInfo.CurrentCulture), False) ' times(UBound(times)), False)
        timeAxis.Min = New isr.Drawing.AutoValueR(0, False)  ' times(LBound(times)), False)

        signalAxis.Max = New isr.Drawing.AutoValueR(1, True)
        signalAxis.Min = New isr.Drawing.AutoValueR(-1, True)

        signalCurve.UpdateData(times, amplitudes)
        Me.signalChartPane.Rescale()

        Me._SignalChartPanel.Invalidate()

    End Sub

    ''' <summary>Redraws the Signal.</summary>
    Private Sub _SignalChartPanel_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles _SignalChartPanel.Paint
        If sender IsNot Nothing Then
            Using sb As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(sb, Me.ClientRectangle)
                signalChartPane.Draw(e.Graphics)
            End Using
        End If
    End Sub

    ''' <summary>Redraws the Signal chart when the form is resized</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of the 
    '''   chart panel <see cref="System.Windows.Forms.Panel"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    Private Sub _SignalChartPanel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SignalChartPanel.Resize
        If Not signalChartPane Is Nothing Then
            signalChartPane.SetSize(Me._SignalChartPanel.ClientRectangle)
        End If
    End Sub

#End Region

#End Region

#Region " CONTROL EVENT HANDLERS "

    Private Sub _cyclesTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _CyclesTextBox.Validating

        Me._ErrorProvider.SetError(Me._CyclesTextBox, String.Empty)
        If isr.Core.StringExtensions.IsNumber(Me._CyclesTextBox.Text) Then
            If Integer.Parse(Me._CyclesTextBox.Text, Globalization.CultureInfo.CurrentCulture) < 1 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._CyclesTextBox, "Must exceed 1")
            Else
                Me.SineWaveDouble()
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._CyclesTextBox, "Enter numeric value")
        End If

    End Sub

    Private Sub _PhaseTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PhaseTextBox.Validating

        Me._ErrorProvider.SetError(Me._PhaseTextBox, String.Empty)
        If isr.Core.StringExtensions.IsNumber(Me._PhaseTextBox.Text) Then
            Me.SineWaveDouble()
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._PhaseTextBox, "Enter numeric value")
        End If

    End Sub

    Private Sub _pointsTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PointsTextBox.Validating

        Me._ErrorProvider.SetError(Me._PointsTextBox, String.Empty)
        If isr.Core.StringExtensions.IsNumber(Me._PointsTextBox.Text) Then
            If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) < 2 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._PointsTextBox, "Must exceed 2")
            Else
                ' Set initial values defining the signal
                If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                  < Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                    Me._PointsToDisplayTextBox.Text = Me._PointsTextBox.Text
                End If
                Me.SineWaveDouble()
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._PointsTextBox, "Enter numeric value")
        End If

    End Sub

    Private Sub _pointsToDisplayTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _PointsToDisplayTextBox.Validating

        Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, String.Empty)
        If isr.Core.StringExtensions.IsNumber(Me._PointsToDisplayTextBox.Text) Then
            If Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) < 2 Then
                e.Cancel = True
                Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, "Must exceed 2")
            Else
                ' Set initial values defining the signal
                If Integer.Parse(Me._PointsTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                  < Integer.Parse(Me._PointsToDisplayTextBox.Text, Globalization.CultureInfo.CurrentCulture) Then
                    Me._PointsToDisplayTextBox.Text = Me._PointsTextBox.Text
                End If
            End If
        Else
            e.Cancel = True
            Me._ErrorProvider.SetError(Me._PointsToDisplayTextBox, "Enter numeric value")
        End If

    End Sub

    Private Sub _startStopCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _StartStopCheckBox.CheckedChanged

        If Me._StartStopCheckBox.Checked Then
            Me._StartStopCheckBox.Text = "&Stop"
        Else
            Me._StartStopCheckBox.Text = "&Start"
        End If

        If Me._StartStopCheckBox.Enabled AndAlso Me._StartStopCheckBox.Checked Then

            Dim algorithm As Example = Me.selectedExample

            Select Case algorithm

                Case Example.DiscreteFourierTransform, Example.MixedRadixFFT

                    Me._CountStatusBarPanel.Text = "0"
                    Do
                        If Me._DoubleRadioButton.Checked Then
                            Me.calculateSpectrumDouble(algorithm)
                        Else
                            Me.calculateSpectrumSingle(algorithm)
                        End If
                        My.Application.DoEvents()
                        Dim count As Integer = Integer.Parse(Me._CountStatusBarPanel.Text, Globalization.CultureInfo.CurrentCulture) + 1
                        Me._CountStatusBarPanel.Text = count.ToString(Globalization.CultureInfo.CurrentCulture)
                    Loop While Me._StartStopCheckBox.Checked And False

                Case Example.SlidingFFT

                    If Me._DoubleRadioButton.Checked Then
                        Me.slidingFft(0.5R)
                    Else
                        Me.slidingFft(0.5F)
                    End If

                Case Else

            End Select

            If Me._StartStopCheckBox.Checked Then
                Me._StartStopCheckBox.Enabled = False
                Me._StartStopCheckBox.Checked = False
                Me._StartStopCheckBox.Enabled = True
            End If

        Else

            Me._CountStatusBarPanel.Text = "0"

        End If

    End Sub

#End Region

End Class