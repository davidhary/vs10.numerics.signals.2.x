<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SpectrumPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            If disposing Then

                ' Free managed resources when explicitly called
                If signalChartPane IsNot Nothing Then
                    signalChartPane.Dispose()
                    signalChartPane = Nothing
                End If

                If Me._spectrumChartPan IsNot Nothing Then
                    Me._spectrumChartPan.Dispose()
                    Me._spectrumChartPan = Nothing
                End If

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="TransformTimeStatusBarPanel")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="StatusStatusBarPanel")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="ErrorStatusBarPanel")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="CountStatusBarPanel")>
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._PointsToDisplayTextBox = New System.Windows.Forms.TextBox()
        Me._SignalListBox = New System.Windows.Forms.ListBox()
        Me._DataTabPage = New System.Windows.Forms.TabPage()
        Me._TimingTextBox = New System.Windows.Forms.TextBox()
        Me._TimingListBoxLabel = New System.Windows.Forms.Label()
        Me._PointsToDisplayTextBoxLabel = New System.Windows.Forms.Label()
        Me._SignalListBoxLabel = New System.Windows.Forms.Label()
        Me._DoubleRadioButton = New System.Windows.Forms.RadioButton()
        Me._RemoveMeanCheckBox = New System.Windows.Forms.CheckBox()
        Me._ExampleComboBox = New System.Windows.Forms.ComboBox()
        Me._TaperWindowCheckBox = New System.Windows.Forms.CheckBox()
        Me._TransformTimeStatusBarPanel = New System.Windows.Forms.StatusBarPanel()
        Me._ErrorStatusBarPanel = New System.Windows.Forms.StatusBarPanel()
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._SignalTabPage = New System.Windows.Forms.TabPage()
        Me._SignalChartPanel = New System.Windows.Forms.Panel()
        Me._SignalOptionsPanel = New System.Windows.Forms.Panel()
        Me._SignalDurationTextBox = New System.Windows.Forms.TextBox()
        Me._SignalDurationTextBoxLabel = New System.Windows.Forms.Label()
        Me._PointsTextBox = New System.Windows.Forms.TextBox()
        Me._PhaseTextBox = New System.Windows.Forms.TextBox()
        Me._CyclesTextBox = New System.Windows.Forms.TextBox()
        Me._PointsTextBoxLabel = New System.Windows.Forms.Label()
        Me._PhaseTextBoxLabel = New System.Windows.Forms.Label()
        Me._CyclesTextBoxLabel = New System.Windows.Forms.Label()
        Me._SpectrumTabPage = New System.Windows.Forms.TabPage()
        Me._SpectrumChartPanel = New System.Windows.Forms.Panel()
        Me._SpectrumOptionsPanel = New System.Windows.Forms.Panel()
        Me._ExampleComboBoxLabel = New System.Windows.Forms.Label()
        Me._StartStopCheckBox = New System.Windows.Forms.CheckBox()
        Me._SingleRadioButton = New System.Windows.Forms.RadioButton()
        Me._MessagesTabPage = New System.Windows.Forms.TabPage()
        Me._MessagesList = New isr.Controls.MessagesBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._CountStatusBarPanel = New System.Windows.Forms.StatusBarPanel()
        Me._StatusStatusBarPanel = New System.Windows.Forms.StatusBarPanel()
        Me._StatusBar = New System.Windows.Forms.StatusBar()
        Me._DataTabPage.SuspendLayout()
        CType(Me._TransformTimeStatusBarPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ErrorStatusBarPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Tabs.SuspendLayout()
        Me._SignalTabPage.SuspendLayout()
        Me._SignalOptionsPanel.SuspendLayout()
        Me._SpectrumTabPage.SuspendLayout()
        Me._SpectrumOptionsPanel.SuspendLayout()
        Me._MessagesTabPage.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._CountStatusBarPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._StatusStatusBarPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_PointsToDisplayTextBox
        '
        Me._PointsToDisplayTextBox.AcceptsReturn = True
        Me._PointsToDisplayTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._PointsToDisplayTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._PointsToDisplayTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PointsToDisplayTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._PointsToDisplayTextBox.Location = New System.Drawing.Point(126, 16)
        Me._PointsToDisplayTextBox.MaxLength = 0
        Me._PointsToDisplayTextBox.Name = "_PointsToDisplayTextBox"
        Me._PointsToDisplayTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsToDisplayTextBox.Size = New System.Drawing.Size(65, 20)
        Me._PointsToDisplayTextBox.TabIndex = 17
        '
        '_SignalListBox
        '
        Me._SignalListBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SignalListBox.BackColor = System.Drawing.SystemColors.Window
        Me._SignalListBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SignalListBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._SignalListBox.Location = New System.Drawing.Point(272, 24)
        Me._SignalListBox.Name = "_SignalListBox"
        Me._SignalListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalListBox.Size = New System.Drawing.Size(269, 303)
        Me._SignalListBox.TabIndex = 15
        '
        '_DataTabPage
        '
        Me._DataTabPage.Controls.Add(Me._TimingTextBox)
        Me._DataTabPage.Controls.Add(Me._TimingListBoxLabel)
        Me._DataTabPage.Controls.Add(Me._PointsToDisplayTextBox)
        Me._DataTabPage.Controls.Add(Me._SignalListBox)
        Me._DataTabPage.Controls.Add(Me._PointsToDisplayTextBoxLabel)
        Me._DataTabPage.Controls.Add(Me._SignalListBoxLabel)
        Me._DataTabPage.Location = New System.Drawing.Point(4, 22)
        Me._DataTabPage.Name = "_DataTabPage"
        Me._DataTabPage.Size = New System.Drawing.Size(552, 342)
        Me._DataTabPage.TabIndex = 2
        Me._DataTabPage.Text = "Data"
        '
        '_TimingTextBox
        '
        Me._TimingTextBox.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TimingTextBox.Location = New System.Drawing.Point(20, 70)
        Me._TimingTextBox.Multiline = True
        Me._TimingTextBox.Name = "_TimingTextBox"
        Me._TimingTextBox.Size = New System.Drawing.Size(246, 257)
        Me._TimingTextBox.TabIndex = 21
        '
        '_TimingListBoxLabel
        '
        Me._TimingListBoxLabel.AutoSize = True
        Me._TimingListBoxLabel.Location = New System.Drawing.Point(16, 54)
        Me._TimingListBoxLabel.Name = "_TimingListBoxLabel"
        Me._TimingListBoxLabel.Size = New System.Drawing.Size(70, 13)
        Me._TimingListBoxLabel.TabIndex = 20
        Me._TimingListBoxLabel.Text = "Timing Data: "
        '
        '_PointsToDisplayTextBoxLabel
        '
        Me._PointsToDisplayTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._PointsToDisplayTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._PointsToDisplayTextBoxLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PointsToDisplayTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PointsToDisplayTextBoxLabel.Location = New System.Drawing.Point(16, 18)
        Me._PointsToDisplayTextBoxLabel.Name = "_PointsToDisplayTextBoxLabel"
        Me._PointsToDisplayTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsToDisplayTextBoxLabel.Size = New System.Drawing.Size(104, 16)
        Me._PointsToDisplayTextBoxLabel.TabIndex = 18
        Me._PointsToDisplayTextBoxLabel.Text = "Points to Display: "
        Me._PointsToDisplayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SignalListBoxLabel
        '
        Me._SignalListBoxLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SignalListBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._SignalListBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalListBoxLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SignalListBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SignalListBoxLabel.Location = New System.Drawing.Point(272, 8)
        Me._SignalListBoxLabel.Name = "_SignalListBoxLabel"
        Me._SignalListBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalListBoxLabel.Size = New System.Drawing.Size(269, 16)
        Me._SignalListBoxLabel.TabIndex = 16
        Me._SignalListBoxLabel.Text = "Signal                       IFFT{ FFT{ Signal}}"
        '
        '_DoubleRadioButton
        '
        Me._DoubleRadioButton.Checked = True
        Me._DoubleRadioButton.Location = New System.Drawing.Point(112, 5)
        Me._DoubleRadioButton.Name = "_DoubleRadioButton"
        Me._DoubleRadioButton.Size = New System.Drawing.Size(120, 17)
        Me._DoubleRadioButton.TabIndex = 27
        Me._DoubleRadioButton.TabStop = True
        Me._DoubleRadioButton.Text = "Double Precision"
        '
        '_RemoveMeanCheckBox
        '
        Me._RemoveMeanCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._RemoveMeanCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._RemoveMeanCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RemoveMeanCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RemoveMeanCheckBox.Location = New System.Drawing.Point(8, 4)
        Me._RemoveMeanCheckBox.Name = "_RemoveMeanCheckBox"
        Me._RemoveMeanCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RemoveMeanCheckBox.Size = New System.Drawing.Size(104, 18)
        Me._RemoveMeanCheckBox.TabIndex = 23
        Me._RemoveMeanCheckBox.Text = "Remove Mean"
        Me._RemoveMeanCheckBox.UseVisualStyleBackColor = False
        '
        '_ExampleComboBox
        '
        Me._ExampleComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._ExampleComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ExampleComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ExampleComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ExampleComboBox.Items.AddRange(New Object() {"Mixed Radix FFT", "Sliding FFT"})
        Me._ExampleComboBox.Location = New System.Drawing.Point(294, 27)
        Me._ExampleComboBox.Name = "_ExampleComboBox"
        Me._ExampleComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ExampleComboBox.Size = New System.Drawing.Size(178, 21)
        Me._ExampleComboBox.TabIndex = 25
        Me._ExampleComboBox.Text = "Combo1"
        '
        '_TaperWindowCheckBox
        '
        Me._TaperWindowCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._TaperWindowCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._TaperWindowCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TaperWindowCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._TaperWindowCheckBox.Location = New System.Drawing.Point(8, 24)
        Me._TaperWindowCheckBox.Name = "_TaperWindowCheckBox"
        Me._TaperWindowCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TaperWindowCheckBox.Size = New System.Drawing.Size(104, 18)
        Me._TaperWindowCheckBox.TabIndex = 24
        Me._TaperWindowCheckBox.Text = "Taper Window"
        Me._TaperWindowCheckBox.UseVisualStyleBackColor = False
        '
        '_TransformTimeStatusBarPanel
        '
        Me._TransformTimeStatusBarPanel.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me._TransformTimeStatusBarPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me._TransformTimeStatusBarPanel.MinWidth = 80
        Me._TransformTimeStatusBarPanel.Name = "_TransformTimeStatusBarPanel"
        Me._TransformTimeStatusBarPanel.Text = "0.000 ms"
        Me._TransformTimeStatusBarPanel.ToolTipText = "Time to calculate an FFT"
        Me._TransformTimeStatusBarPanel.Width = 80
        '
        '_ErrorStatusBarPanel
        '
        Me._ErrorStatusBarPanel.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me._ErrorStatusBarPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me._ErrorStatusBarPanel.MinWidth = 60
        Me._ErrorStatusBarPanel.Name = "_ErrorStatusBarPanel"
        Me._ErrorStatusBarPanel.Text = "0.000"
        Me._ErrorStatusBarPanel.ToolTipText = "RMS difference between signal and inverse FFT"
        Me._ErrorStatusBarPanel.Width = 60
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._SignalTabPage)
        Me._Tabs.Controls.Add(Me._SpectrumTabPage)
        Me._Tabs.Controls.Add(Me._DataTabPage)
        Me._Tabs.Controls.Add(Me._MessagesTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(560, 368)
        Me._Tabs.TabIndex = 2
        '
        '_SignalTabPage
        '
        Me._SignalTabPage.Controls.Add(Me._SignalChartPanel)
        Me._SignalTabPage.Controls.Add(Me._SignalOptionsPanel)
        Me._SignalTabPage.Location = New System.Drawing.Point(4, 22)
        Me._SignalTabPage.Name = "_SignalTabPage"
        Me._SignalTabPage.Size = New System.Drawing.Size(552, 342)
        Me._SignalTabPage.TabIndex = 0
        Me._SignalTabPage.Text = "Signal"
        '
        '_SignalChartPanel
        '
        Me._SignalChartPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SignalChartPanel.Location = New System.Drawing.Point(0, 56)
        Me._SignalChartPanel.Name = "_SignalChartPanel"
        Me._SignalChartPanel.Size = New System.Drawing.Size(552, 286)
        Me._SignalChartPanel.TabIndex = 30
        '
        '_SignalOptionsPanel
        '
        Me._SignalOptionsPanel.Controls.Add(Me._SignalDurationTextBox)
        Me._SignalOptionsPanel.Controls.Add(Me._SignalDurationTextBoxLabel)
        Me._SignalOptionsPanel.Controls.Add(Me._PointsTextBox)
        Me._SignalOptionsPanel.Controls.Add(Me._PhaseTextBox)
        Me._SignalOptionsPanel.Controls.Add(Me._CyclesTextBox)
        Me._SignalOptionsPanel.Controls.Add(Me._PointsTextBoxLabel)
        Me._SignalOptionsPanel.Controls.Add(Me._PhaseTextBoxLabel)
        Me._SignalOptionsPanel.Controls.Add(Me._CyclesTextBoxLabel)
        Me._SignalOptionsPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._SignalOptionsPanel.Location = New System.Drawing.Point(0, 0)
        Me._SignalOptionsPanel.Name = "_SignalOptionsPanel"
        Me._SignalOptionsPanel.Size = New System.Drawing.Size(552, 56)
        Me._SignalOptionsPanel.TabIndex = 29
        '
        '_SignalDurationTextBox
        '
        Me._SignalDurationTextBox.AcceptsReturn = True
        Me._SignalDurationTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._SignalDurationTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._SignalDurationTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SignalDurationTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._SignalDurationTextBox.Location = New System.Drawing.Point(90, 30)
        Me._SignalDurationTextBox.MaxLength = 0
        Me._SignalDurationTextBox.Name = "_SignalDurationTextBox"
        Me._SignalDurationTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalDurationTextBox.Size = New System.Drawing.Size(46, 20)
        Me._SignalDurationTextBox.TabIndex = 23
        '
        '_SignalDurationTextBoxLabel
        '
        Me._SignalDurationTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._SignalDurationTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SignalDurationTextBoxLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SignalDurationTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SignalDurationTextBoxLabel.Location = New System.Drawing.Point(8, 32)
        Me._SignalDurationTextBoxLabel.Name = "_SignalDurationTextBoxLabel"
        Me._SignalDurationTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SignalDurationTextBoxLabel.Size = New System.Drawing.Size(80, 16)
        Me._SignalDurationTextBoxLabel.TabIndex = 24
        Me._SignalDurationTextBoxLabel.Text = "Duration [Sec]: "
        Me._SignalDurationTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PointsTextBox
        '
        Me._PointsTextBox.AcceptsReturn = True
        Me._PointsTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._PointsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._PointsTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PointsTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._PointsTextBox.Location = New System.Drawing.Point(90, 4)
        Me._PointsTextBox.MaxLength = 0
        Me._PointsTextBox.Name = "_PointsTextBox"
        Me._PointsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsTextBox.Size = New System.Drawing.Size(46, 20)
        Me._PointsTextBox.TabIndex = 22
        Me._PointsTextBox.Text = "1000"
        '
        '_PhaseTextBox
        '
        Me._PhaseTextBox.AcceptsReturn = True
        Me._PhaseTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._PhaseTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._PhaseTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PhaseTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._PhaseTextBox.Location = New System.Drawing.Point(240, 30)
        Me._PhaseTextBox.MaxLength = 0
        Me._PhaseTextBox.Name = "_PhaseTextBox"
        Me._PhaseTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PhaseTextBox.Size = New System.Drawing.Size(48, 20)
        Me._PhaseTextBox.TabIndex = 19
        '
        '_CyclesTextBox
        '
        Me._CyclesTextBox.AcceptsReturn = True
        Me._CyclesTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._CyclesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._CyclesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CyclesTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._CyclesTextBox.Location = New System.Drawing.Point(240, 4)
        Me._CyclesTextBox.MaxLength = 0
        Me._CyclesTextBox.Name = "_CyclesTextBox"
        Me._CyclesTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CyclesTextBox.Size = New System.Drawing.Size(48, 20)
        Me._CyclesTextBox.TabIndex = 17
        '
        '_PointsTextBoxLabel
        '
        Me._PointsTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._PointsTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._PointsTextBoxLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PointsTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PointsTextBoxLabel.Location = New System.Drawing.Point(40, 6)
        Me._PointsTextBoxLabel.Name = "_PointsTextBoxLabel"
        Me._PointsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsTextBoxLabel.Size = New System.Drawing.Size(48, 16)
        Me._PointsTextBoxLabel.TabIndex = 21
        Me._PointsTextBoxLabel.Text = "Points: "
        Me._PointsTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PhaseTextBoxLabel
        '
        Me._PhaseTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._PhaseTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._PhaseTextBoxLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PhaseTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PhaseTextBoxLabel.Location = New System.Drawing.Point(160, 32)
        Me._PhaseTextBoxLabel.Name = "_PhaseTextBoxLabel"
        Me._PhaseTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PhaseTextBoxLabel.Size = New System.Drawing.Size(80, 16)
        Me._PhaseTextBoxLabel.TabIndex = 20
        Me._PhaseTextBoxLabel.Text = "Phase [Deg]: "
        Me._PhaseTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_CyclesTextBoxLabel
        '
        Me._CyclesTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._CyclesTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CyclesTextBoxLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CyclesTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CyclesTextBoxLabel.Location = New System.Drawing.Point(152, 6)
        Me._CyclesTextBoxLabel.Name = "_CyclesTextBoxLabel"
        Me._CyclesTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CyclesTextBoxLabel.Size = New System.Drawing.Size(87, 16)
        Me._CyclesTextBoxLabel.TabIndex = 18
        Me._CyclesTextBoxLabel.Text = "Frequency [Hz]: "
        Me._CyclesTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SpectrumTabPage
        '
        Me._SpectrumTabPage.Controls.Add(Me._SpectrumChartPanel)
        Me._SpectrumTabPage.Controls.Add(Me._SpectrumOptionsPanel)
        Me._SpectrumTabPage.Location = New System.Drawing.Point(4, 22)
        Me._SpectrumTabPage.Name = "_SpectrumTabPage"
        Me._SpectrumTabPage.Size = New System.Drawing.Size(552, 342)
        Me._SpectrumTabPage.TabIndex = 1
        Me._SpectrumTabPage.Text = "Spectrum"
        '
        '_SpectrumChartPanel
        '
        Me._SpectrumChartPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SpectrumChartPanel.Location = New System.Drawing.Point(0, 51)
        Me._SpectrumChartPanel.Name = "_SpectrumChartPanel"
        Me._SpectrumChartPanel.Size = New System.Drawing.Size(552, 291)
        Me._SpectrumChartPanel.TabIndex = 28
        '
        '_SpectrumOptionsPanel
        '
        Me._SpectrumOptionsPanel.Controls.Add(Me._ExampleComboBoxLabel)
        Me._SpectrumOptionsPanel.Controls.Add(Me._StartStopCheckBox)
        Me._SpectrumOptionsPanel.Controls.Add(Me._SingleRadioButton)
        Me._SpectrumOptionsPanel.Controls.Add(Me._DoubleRadioButton)
        Me._SpectrumOptionsPanel.Controls.Add(Me._RemoveMeanCheckBox)
        Me._SpectrumOptionsPanel.Controls.Add(Me._ExampleComboBox)
        Me._SpectrumOptionsPanel.Controls.Add(Me._TaperWindowCheckBox)
        Me._SpectrumOptionsPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._SpectrumOptionsPanel.Location = New System.Drawing.Point(0, 0)
        Me._SpectrumOptionsPanel.Name = "_SpectrumOptionsPanel"
        Me._SpectrumOptionsPanel.Size = New System.Drawing.Size(552, 51)
        Me._SpectrumOptionsPanel.TabIndex = 27
        '
        '_ExampleComboBoxLabel
        '
        Me._ExampleComboBoxLabel.Location = New System.Drawing.Point(232, 29)
        Me._ExampleComboBoxLabel.Name = "_ExampleComboBoxLabel"
        Me._ExampleComboBoxLabel.Size = New System.Drawing.Size(64, 16)
        Me._ExampleComboBoxLabel.TabIndex = 32
        Me._ExampleComboBoxLabel.Text = "Calculate: "
        Me._ExampleComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_StartStopCheckBox
        '
        Me._StartStopCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._StartStopCheckBox.Appearance = System.Windows.Forms.Appearance.Button
        Me._StartStopCheckBox.Location = New System.Drawing.Point(484, 25)
        Me._StartStopCheckBox.Name = "_StartStopCheckBox"
        Me._StartStopCheckBox.Size = New System.Drawing.Size(64, 24)
        Me._StartStopCheckBox.TabIndex = 31
        Me._StartStopCheckBox.Text = "&Start"
        Me._StartStopCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_SingleRadioButton
        '
        Me._SingleRadioButton.Location = New System.Drawing.Point(112, 25)
        Me._SingleRadioButton.Name = "_SingleRadioButton"
        Me._SingleRadioButton.Size = New System.Drawing.Size(104, 17)
        Me._SingleRadioButton.TabIndex = 28
        Me._SingleRadioButton.Text = "Single Precision"
        '
        '_MessagesTabPage
        '
        Me._MessagesTabPage.Controls.Add(Me._MessagesList)
        Me._MessagesTabPage.Location = New System.Drawing.Point(4, 22)
        Me._MessagesTabPage.Name = "_MessagesTabPage"
        Me._MessagesTabPage.Size = New System.Drawing.Size(552, 342)
        Me._MessagesTabPage.TabIndex = 3
        Me._MessagesTabPage.Text = "Messages"
        '
        '_MessagesList
        '
        Me._MessagesList.BackColor = System.Drawing.SystemColors.Info
        Me._MessagesList.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MessagesList.Location = New System.Drawing.Point(0, 0)
        Me._MessagesList.Multiline = True
        Me._MessagesList.Name = "_MessagesList"
        Me._MessagesList.PresetCount = 50
        Me._MessagesList.ReadOnly = True
        Me._MessagesList.ResetCount = 100
        Me._MessagesList.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._MessagesList.Size = New System.Drawing.Size(552, 342)
        Me._MessagesList.TabIndex = 4
        Me._MessagesList.TimeFormat = "HH:mm:ss. "
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_CountStatusBarPanel
        '
        Me._CountStatusBarPanel.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me._CountStatusBarPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me._CountStatusBarPanel.MinWidth = 20
        Me._CountStatusBarPanel.Name = "_CountStatusBarPanel"
        Me._CountStatusBarPanel.Text = "0"
        Me._CountStatusBarPanel.ToolTipText = "FFT counter"
        Me._CountStatusBarPanel.Width = 20
        '
        '_StatusStatusBarPanel
        '
        Me._StatusStatusBarPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me._StatusStatusBarPanel.Name = "_StatusStatusBarPanel"
        Me._StatusStatusBarPanel.Text = "ready"
        Me._StatusStatusBarPanel.ToolTipText = "Status"
        Me._StatusStatusBarPanel.Width = 383
        '
        '_StatusBar
        '
        Me._StatusBar.Location = New System.Drawing.Point(0, 368)
        Me._StatusBar.Name = "_StatusBar"
        Me._StatusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me._StatusStatusBarPanel, Me._CountStatusBarPanel, Me._ErrorStatusBarPanel, Me._TransformTimeStatusBarPanel})
        Me._StatusBar.ShowPanels = True
        Me._StatusBar.Size = New System.Drawing.Size(560, 22)
        Me._StatusBar.TabIndex = 3
        Me._StatusBar.Text = "ready"
        '
        'SpectrumPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(560, 390)
        Me.Controls.Add(Me._Tabs)
        Me.Controls.Add(Me._StatusBar)
        Me.Name = "SpectrumPanel"
        Me.Text = "Spectrum Panel"
        Me._DataTabPage.ResumeLayout(False)
        Me._DataTabPage.PerformLayout()
        CType(Me._TransformTimeStatusBarPanel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ErrorStatusBarPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me._Tabs.ResumeLayout(False)
        Me._SignalTabPage.ResumeLayout(False)
        Me._SignalOptionsPanel.ResumeLayout(False)
        Me._SignalOptionsPanel.PerformLayout()
        Me._SpectrumTabPage.ResumeLayout(False)
        Me._SpectrumOptionsPanel.ResumeLayout(False)
        Me._MessagesTabPage.ResumeLayout(False)
        Me._MessagesTabPage.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._CountStatusBarPanel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._StatusStatusBarPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _PointsToDisplayTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SignalListBox As System.Windows.Forms.ListBox
    Private WithEvents _DataTabPage As System.Windows.Forms.TabPage
    Private WithEvents _PointsToDisplayTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SignalListBoxLabel As System.Windows.Forms.Label
    Private WithEvents _DoubleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _RemoveMeanCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ExampleComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _TaperWindowCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _TransformTimeStatusBarPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents _ErrorStatusBarPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents _Tabs As System.Windows.Forms.TabControl
    Private WithEvents _SignalTabPage As System.Windows.Forms.TabPage
    Private WithEvents _SignalChartPanel As System.Windows.Forms.Panel
    Private WithEvents _SignalOptionsPanel As System.Windows.Forms.Panel
    Private WithEvents _SignalDurationTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SignalDurationTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PointsTextBox As System.Windows.Forms.TextBox
    Private WithEvents _PhaseTextBox As System.Windows.Forms.TextBox
    Private WithEvents _CyclesTextBox As System.Windows.Forms.TextBox
    Private WithEvents _PointsTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PhaseTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _CyclesTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SpectrumTabPage As System.Windows.Forms.TabPage
    Private WithEvents _SpectrumChartPanel As System.Windows.Forms.Panel
    Private WithEvents _SpectrumOptionsPanel As System.Windows.Forms.Panel
    Private WithEvents _ExampleComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _StartStopCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _SingleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _MessagesTabPage As System.Windows.Forms.TabPage
    Private WithEvents _MessagesList As isr.Controls.MessagesBox
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _StatusBar As System.Windows.Forms.StatusBar
    Private WithEvents _CountStatusBarPanel As System.Windows.Forms.StatusBarPanel
    Private WithEvents _TimingListBoxLabel As System.Windows.Forms.Label
    Private WithEvents _TimingTextBox As System.Windows.Forms.TextBox
    Private WithEvents _StatusStatusBarPanel As System.Windows.Forms.StatusBarPanel
End Class
